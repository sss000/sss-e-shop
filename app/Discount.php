<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
      protected $table="discount";
    
    protected $fillable=['top_caption','middle_caption','buttom_caption','image','url','status'];
}
