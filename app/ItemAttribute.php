<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemAttribute extends Model
{
    protected $table='item_attribute';
    
    protected $fillable=['attribute_id','option_id','fk_item_id'];

     public function optionAttribute(){
    	return $this->belongsTo(SubAttribute::class,'option_id','id');

    }
	public function attribute(){
    	return $this->belongsTo(Attribute::class,'attribute_id','id');

    }


}
