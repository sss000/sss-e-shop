<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
        protected $table='items';
    protected $fillable=['title','product_code','link','rating','description','meta_description','additional_info','fk_category_id','fk_sub_category_id','fk_subsubcategory_id','fk_brand_id','wholesale_price','price','discount','quantity','is_featured','status','is_approved','approved_by','visitor','created_by','updated_by','banner'];

        public function itemImage(){
    	return $this->hasMany(ItemPhoto::class,'fk_item_id','id');

    }

    public function itemAttribute(){
    	return $this->hasMany(ItemAttribute::class,'fk_item_id','id');

    }

  

           public function itemBrands()
    {
        return $this->belongsTo('App\Brand','fk_brand_id','id');
    }

       public function categories(){

        return $this->belongsTo(Category::class,'fk_category_id','id');

    }

 
    
    
}
