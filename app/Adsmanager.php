<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Adsmanager extends Model
{
       protected $table="ads_manager";
    protected $primarykey="id";
    protected $fillable=['name','image','status'];
}
