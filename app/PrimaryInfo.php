<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrimaryInfo extends Model
{
       protected $table='about_company';
    protected $fillable=['company_name','logo','small','image','keyword','address','mobile_no','emargency','outdoor','email','meta_description','description','map_embed','fb_link'];
}
