<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table='categories';
    protected $fillable=['category_name','serial_num','description','keyword','status','created_by','created_by','link','show_home','Show_top_menu'];

      public function subcategory(){

    	return $this->hasMany(SubCategory::class,'fk_category_id','id');

    } 

    public function items(){

    	return $this->hasMany(Items::class,'fk_category_id','id');

    }

      
}
 