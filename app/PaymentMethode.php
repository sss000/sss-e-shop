<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethode extends Model
{
   protected $table="payment_methode";
    protected $primarykey="id";
    protected $fillable=['name','account_number','details','description','status'];
}
