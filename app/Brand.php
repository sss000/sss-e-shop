<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
     protected $table="brand";
    protected $primarykey="id";
    protected $fillable=['serial','name','image','status'];
}
