<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\PrimaryInfo;
use App\SocialIcon;



use View;


class ShooperServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
      {
        View::composer( 

        [
            'frontend._partials.footer',
            'frontend._partials.app',
            'frontend.home',
            'frontend.contact.contact'

        ],function($view){

             $info = PrimaryInfo::first();

             $view->with('info',$info);
        });


          View::composer( // social icon show in footer
            [
                'frontend._partials.footer'
            ],function ($view){
                $icons=SocialIcon::orderBy('serial_num','ASC')->where('status',1)->get();
                $view->with('icons',$icons);
        });

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
