<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialIcon extends Model
{
    protected $table='social_icons';
    protected $fillable=['name','url','status','serial_num','icon_class'];
}
