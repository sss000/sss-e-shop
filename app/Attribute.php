<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
     protected $table='attribute';
    protected $fillable=['name','status','description','fk_attribute_id','created_by','type'];

    public function options(){
    	return $this->hasMany(SubAttribute::class,'fk_attribute_id','id');

    }


}
