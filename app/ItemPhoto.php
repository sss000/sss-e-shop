<?php

namespace App;
use Image;

use Illuminate\Database\Eloquent\Model;

class ItemPhoto extends Model
{
      protected $table='item_photo';
    
    protected $fillable=['photo','fk_item_id'];

            static public function imageUpdate($request,$id){

        $photos = ItemPhoto::where('fk_item_id',$id)->get();
        if ($request->hasFile('image')) {
            $photo=$request->file('image');
            $fileType=$photo->getClientOriginalExtension();
            $fileName=rand(1,1000).date('dmyhis').".".$fileType;
            $path2=base_path().'/images/items/small/'.date('Y/m/d');
            if (!is_dir($path2)) {
                mkdir("$path2",0777,true);
            }
            $path3=base_path().'/images/items/big/'.date('Y/m/d');
            if (!is_dir($path3)) {
                mkdir("$path3",0777,true);
            }
            $img = Image::make($photo);
            $img->resize(600,600);
            $img->save('images/items/big/'.date('Y/m/d/').$fileName);
            $img->resize(253, 300);
            $img->save('images/items/small/'.date('Y/m/d/').$fileName);
            $image=date('Y/m/d/').$fileName;
            if(isset($photos[0])){
                $photoId = $photos[0]->id;
                $oldPath1 = base_path().'/images/items/small/'.$photos[0]->photo;
                if(file_exists($oldPath1)){
                    unlink($oldPath1);
                }
                $oldPath2 = base_path().'/images/items/big/'.$photos[0]->photo;
                if(file_exists($oldPath2)){
                    unlink($oldPath2);
                }
                ItemPhoto::where('id',$photoId)->delete();
            }
            ItemPhoto::create(['photo'=>$image,'fk_item_id'=>$id]);
        }
        if ($request->hasFile('image-one')) {
            $photo=$request->file('image-one');
            $fileType=$photo->getClientOriginalExtension();
            $fileName=rand(1,1000).date('dmyhis').".".$fileType;
            $path2=base_path().'/images/items/small/'.date('Y/m/d');
            if (!is_dir($path2)) {
                mkdir("$path2",0777,true);
            }
            $path3=base_path().'/images/items/big/'.date('Y/m/d');
            if (!is_dir($path3)) {
                mkdir("$path3",0777,true);
            }
            $img = Image::make($photo);
            $img->resize(600,600);
            $img->save('images/items/big/'.date('Y/m/d/').$fileName);
            $img->resize(253, 300);
            $img->save('images/items/small/'.date('Y/m/d/').$fileName);
            $image_one=date('Y/m/d/').$fileName;
            if(isset($photos[1])){
                $photoId = $photos[1]->id;
                $oldPath1 = base_path().'/images/items/small/'.$photos[1]->photo;
                if(file_exists($oldPath1)){
                    unlink($oldPath1);
                }
                $oldPath2 = base_path().'/images/items/big/'.$photos[1]->photo;
                if(file_exists($oldPath2)){
                    unlink($oldPath2);
                }
                ItemPhoto::where('id',$photoId)->delete();
            }
            ItemPhoto::create(['photo'=>$image_one,'fk_item_id'=>$id]);
        }
        if ($request->hasFile('image_three')) {
            $photo=$request->file('image_three');
            $fileType=$photo->getClientOriginalExtension();
            $fileName=rand(1,1000).date('dmyhis').".".$fileType;
            $path2=base_path().'/images/items/small/'.date('Y/m/d');
            if (!is_dir($path2)) {
                mkdir("$path2",0777,true);
            }
            $path3=base_path().'/images/items/big/'.date('Y/m/d');
            if (!is_dir($path3)) {
                mkdir("$path3",0777,true);
            }
            $img = Image::make($photo);
            $img->resize(600,600);
            $img->save('images/items/big/'.date('Y/m/d/').$fileName);
            $img->resize(253, 300);
            $img->save('images/items/small/'.date('Y/m/d/').$fileName);
            $image_three=date('Y/m/d/').$fileName;
            if(isset($photos[2])){
                $photoId = $photos[2]->id;
                $oldPath1 = base_path().'/images/items/small/'.$photos[2]->photo;
                if(file_exists($oldPath1)){
                    unlink($oldPath1);
                }
                $oldPath2 = base_path().'/images/items/big/'.$photos[2]->photo;
                if(file_exists($oldPath2)){
                    unlink($oldPath2);
                }
                ItemPhoto::where('id',$photoId)->delete();
            }
            ItemPhoto::create(['photo'=>$image_three,'fk_item_id'=>$id]);
        }
        if ($request->hasFile('image_four')) {
            $photo=$request->file('image_four');
            $fileType=$photo->getClientOriginalExtension();
            $fileName=rand(1,1000).date('dmyhis').".".$fileType;
            $path2=base_path().'/images/items/small/'.date('Y/m/d');
            if (!is_dir($path2)) {
                mkdir("$path2",0777,true);
            }
            $path3=base_path().'/images/items/big/'.date('Y/m/d');
            if (!is_dir($path3)) {
                mkdir("$path3",0777,true);
            }
            $img = Image::make($photo);
            $img->resize(600,600);
            $img->save('images/items/big/'.date('Y/m/d/').$fileName);
            $img->resize(253, 300);
            $img->save('images/items/small/'.date('Y/m/d/').$fileName);
            $image_four=date('Y/m/d/').$fileName;
            if(isset($photos[3])){
                $photoId = $photos[3]->id;
                $oldPath1 = base_path().'/images/items/small/'.$photos[3]->photo;
                if(file_exists($oldPath1)){
                    unlink($oldPath1);
                }
                $oldPath2 = base_path().'/images/items/big/'.$photos[3]->photo;
                if(file_exists($oldPath2)){
                    unlink($oldPath2);
                }
                ItemPhoto::where('id',$photoId)->delete();
            }
            ItemPhoto::create(['photo'=>$image_four,'fk_item_id'=>$id]);
        }

    }


    

}
