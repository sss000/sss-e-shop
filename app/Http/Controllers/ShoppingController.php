<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Attribute;
use App\ItemPhoto;
use App\ItemAttribute;
use App\Brand;
use App\Items;
use Image;
use Validator;
use Session;
use Cart;
use DB;

class ShoppingController extends Controller
{
   
   public function addToCart(Request $request){


 
   	$product=Items::findOrFail($request->id);
$photo= $product->itemImage[0]->photo;
       
   		 
     

   		    $price = $product->discount;

   		       $attrDetails = [];
        if(isset($request->attribute) and $request->attribute!=null){
            foreach($request->attribute as $key => $value){
              foreach($value as $k => $val){
                $attr = ItemAttribute::findOrFail($val);
                $attribute[$attr->attribute_id] = $attr->option_id;
                $attrDetails[$attr->attribute->name] = $attr->optionAttribute->option_name;
                
              }



            }
        }

        $attribute  = json_encode($request->attribute);
      

        

        Cart::add(array(
            'id' => $product->id,
             'name' => $product->title, 
             'qty' => $request->quantity, 
             'price' => $product->discount,
             'options'=>['attributes'=>$attribute,
                        'attr_details'=>$attrDetails,
                        'link'=>$product->link,
                        'photo'=>$photo]));

       
 

        return redirect()->back();



   }

       public function cartDetails(){
        $total = floatval(str_replace(',','', Cart::total()));
        $cart = [
            'totalAmount'=>$total,
            'total'=>Cart::total(),
            'tax'=>Cart::tax(),
            'subtotal'=>Cart::subtotal(),
            'count'=>Cart::count(),
            'content'=>Cart::content(),
        ];


        return view('frontend.cart.cart',compact('cart'));
    }



        public function remove($rowId){

        Cart::remove($rowId);

         return redirect()->back()->with('success','Successfully Remove');

    }

       public function increment($rowId){
        $item = Cart::get($rowId );
        Cart::update($rowId , $item->qty + 1);
        return redirect()->back();
    }

    public function decrement($rowId){

        $item = Cart::get($rowId );
        Cart::update($rowId , $item->qty - 1);
        

    }

        public function cartClear() {
          
        Cart::destroy();

        return redirect()->back();
    }




}
