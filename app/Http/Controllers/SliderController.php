<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\Category;
use Auth;
use Image; 

class SliderController extends Controller
{
    /**
     * Display a listing of the resource. 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $alldata=Slider::all();
       return view('backend.slider.index',compact('alldata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function create()
    {
        $max_serial=Slider::max('serial');

        $categories=Category::where('status',1)->orderBy('serial_num','ASC')->pluck('category_name','id');
        
       return view('backend.slider.create',compact('max_serial','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $input=$request->all();
    

        try{
            if ($request->hasFile('image')) {
                $photo=$request->file('image');
                $fileType=$photo->getClientOriginalExtension();
                $fileName=rand(1,1000).date('dmyhis').".".$fileType;
                $path2=base_path().'/images/slider/'.date('Y/m/d');
                if (!is_dir($path2)) {
                    mkdir("$path2",0777,true);
                    }
            
                $img = Image::make($photo);
                $img->resize(1280, 380);
                $img->save('images/slider/'.date('Y/m/d/').$fileName);
                
                $input['image']='images/slider/'.date('Y/m/d/').$fileName;
            }

       
        Slider::create($input);
        $bug=0;
        }
        catch(\Exception $e){
            $bug=$e->errorInfo[1];
            $bug1=$e->errorInfo[2];
        }
         if($bug==0){
        return redirect('Slider-info')->with('success','Slider Successfully Inserted');

        }elseif($bug==1062){
            return redirect()->back()->with('error','The link has already been taken.');
        }else{
            return redirect()->back()->with('error','Error: '.$bug1);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories=Category::where('status',1)->orderBy('serial_num','ASC')->pluck('category_name','id');
        $data=Slider::findOrFail($id);
         $max_serial=Slider::max('serial');
        return view('backend.slider.edit',compact('data','max_serial','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=Slider::findOrFail($id);
        $input = $request->except('_token');
        
        

      
            if ($request->hasFile('image')) {
                $photo=$request->file('image');
                $fileType=$photo->getClientOriginalExtension();
                $fileName=rand(1,1000).date('dmyhis').".".$fileType;
                $path2=base_path().'/images/slider/'.date('Y/m/d');
                if (!is_dir($path2)) {
                    mkdir("$path2",0777,true);
                    }
            
                $img = Image::make($photo);
                $img->resize(1350, 570);
                $img->save('images/slider/'.date('Y/m/d/').$fileName);
                
                $input['image']='images/slider/'.date('Y/m/d/').$fileName;

                if($data->image!=null and file_exists($data->image)){
                    unlink($data->image);
                }
            }

       
        $data->update($input);
        try{
        $bug=0;
        }
        catch(\Exception $e){
            $bug=$e->errorInfo[1];
            $bug1=$e->errorInfo[2];
        }
         if($bug==0){
        return redirect('Slider-info')->with('success','Post Successfully Updated');

        }elseif($bug==1062){
            return redirect()->back()->with('error','The link has already been taken.');
        }else{
            return redirect()->back()->with('error','Error: '.$bug1);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         try{
         $data=Slider::where('id',$id)->first();
         /*Delete old photo*/
        $img_path1=base_path().'/'.$data->image;
          

            if($data->image!=null){
                if(file_exists($img_path1)){
                    unlink($img_path1);
                }
                
            }
            $data->delete();
            $bug=0;
            $error=0;
        }catch(\Exception $e){
            $bug=$e->errorInfo[1];
            $error=$e->errorInfo[2];
        }
        if($bug==0){
       return redirect()->back()->with('success','Ad Successfully Deleted!');
        }elseif($bug==1451){
            return redirect()->back()->with('error','This ad is Used anywhere ! ');
        }
        elseif($bug>0){
       return redirect()->back()->with('error','Some thing error found !');

        }
    }
}

