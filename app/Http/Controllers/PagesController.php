<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;


class PagesController extends Controller
{
    
    public function show($link){
    	$value=array('link'=>$link);
        $validator = \Validator::make($value,[
                'link' => 'required|exists:page,link',
        ]);

        if($validator->fails()){
            return redirect()->back();
        } 
    	$page=Page::where('link',$link)->first();
        $pages=Page::where('status',1)->pluck('name','link');
    	
    	\Session::put('title_msg',$page->name);
        \Session::put('metaDescription',$page->title);
    	\Session::put('keywords',$page->title);
    	return view('frontend.page.pagesingle',compact('page','pagePhoto','pages'));
    }
}
