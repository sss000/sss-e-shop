<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Items;

class SpecialsShowController extends Controller
{
    public function specialProduct(){

    	$categories=Category::where('status',1)->get();
    	 $specials=Items::where('is_featured',1)->get();

        return view('frontend.special.special',compact('categories','specials'));
    }
} 
