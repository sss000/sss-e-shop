<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;
use App\SubSubCategory;
use App\SubAttribute;
use App\Attribute;
use App\ItemPhoto;
use App\ItemAttribute;
use App\Brand;
use App\Items;
use App\Model\OrderItems;
use Image;
use Auth;
use Validator;
use DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



        $alldata=Items::leftjoin('categories','items.fk_category_id','categories.id')

                    ->select('categories.category_name','items.*')->paginate(2);

      


     

        return view('backend.product_add.index',compact('alldata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


            $attrubute=Attribute::where('status',1)->get();

       
          $categories=Category::where('status',1)->orderBy('category_name','ASC')->pluck('category_name','id');

          $Brands=Brand::where('status',1)->orderBy('name','ASC')->pluck('name','id');

         return view('backend.product_add.create',compact('categories','Brands','attrubute'));
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



    $input = $request->all();
     
        $validator = Validator::make($input, [
            
                    'link'  => 'required|max:50|unique:items,link', 
                    'product_code'  => 'required', 
                    'title' => 'required',
                    'image' => 'required',
                    
                   
                ]);
                if ($validator->fails()) {
                    return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
                }
               
      $input2=array();

                  if ($request->hasFile('image')) {
                    $photo=$request->file('image');
                    $fileType=$photo->getClientOriginalExtension();
                     $fileName=rand(1,1000).date('dmyhis').".".$fileType;

                      $path2=base_path().'/images/items/small/'.date('Y/m/d');
                                if (!is_dir($path2)) {
                                    mkdir("$path2",0777,true);
                                     }

            
                   $path3=base_path().'/images/items/big/'.date('Y/m/d');
            if (!is_dir($path3)) {
                mkdir("$path3",0777,true);
            }
            $img = Image::make($photo);
            $img->resize(600,600);
            $img->save('images/items/big/'.date('Y/m/d/').$fileName);
            $img->resize(253, 300);
            $img->save('images/items/small/'.date('Y/m/d/').$fileName);



                    $input2[]=date('Y/m/d/').$fileName;
                }


                     if ($request->hasFile('image-one')) {
                    $photo=$request->file('image-one');
                    $fileType=$photo->getClientOriginalExtension();
                     $fileName=rand(1,1000).date('dmyhis').".".$fileType;
                      $path2=base_path().'/images/items/small/'.date('Y/m/d');
                            if (!is_dir($path2)) {
                                mkdir("$path2",0777,true);
                                }


            $path3=base_path().'/images/items/big/'.date('Y/m/d');
            if (!is_dir($path3)) {
                mkdir("$path3",0777,true);
            }
            $img = Image::make($photo);
            $img->resize(600,600);
            $img->save('images/items/big/'.date('Y/m/d/').$fileName);
            $img->resize(253, 300);
            $img->save('images/items/small/'.date('Y/m/d/').$fileName);
                    $input2[]=date('Y/m/d/').$fileName;
                }



                if ($request->hasFile('image_three')) {
                    $photo=$request->file('image_three');
                    $fileType=$photo->getClientOriginalExtension();
                     $fileName=rand(1,1000).date('dmyhis').".".$fileType;
                      $path2=base_path().'/images/items/small/'.date('Y/m/d');
                            if (!is_dir($path2)) {
                                mkdir("$path2",0777,true);
                                }
                    $path3=base_path().'/images/items/big/'.date('Y/m/d');
                    if (!is_dir($path3)) {
                        mkdir("$path3",0777,true);
                    }
                    $img = Image::make($photo);
                    $img->resize(600,600);
                    $img->save('images/items/big/'.date('Y/m/d/').$fileName);
                    $img->resize(253, 300);
                    $img->save('images/items/small/'.date('Y/m/d/').$fileName);
                    $input2[]=date('Y/m/d/').$fileName;
                }



                if ($request->hasFile('image_four')) {
                    $photo=$request->file('image_four');
                    $fileType=$photo->getClientOriginalExtension();
                     $fileName=rand(1,1000).date('dmyhis').".".$fileType;
                      $path2=base_path().'/images/items/small/'.date('Y/m/d');
                if (!is_dir($path2)) {
                    mkdir("$path2",0777,true);
                    }
                    $path3=base_path().'/images/items/big/'.date('Y/m/d');
                    if (!is_dir($path3)) {
                        mkdir("$path3",0777,true);
                    }
                    $img = Image::make($photo);
                    $img->resize(600,600);
                    $img->save('images/items/big/'.date('Y/m/d/').$fileName);
                    $img->resize(253, 300);
                    $img->save('images/items/small/'.date('Y/m/d/').$fileName);


                    $input2[]=date('Y/m/d/').$fileName;
                }


                 

                 $itemId=Items::create($input)->id;

                    
                   if(isset($input2)){
                   
                     for ($i=0; $i <sizeof($input2) ; $i++) { 
                         ItemPhoto::create([
                            'photo'=>$input2[$i],
                            'fk_item_id'=>$itemId
                         ]);
                     }
                      
                   }
                   
                   if($request->option_id>0){

                    
                  foreach ($request->option_id as $key => $value) {
                    
                    for ($i=0; $i < sizeof($request->option_id[$key]); $i++) { 
                        ItemAttribute::create([
                        'attribute_id'=>$key,
                        'option_id'=>$request->option_id[$key][$i],
                        'fk_item_id'=>$itemId
                       ]);
                    }
                   
                } 
                   }


      
            try{
                
            $bug=0;
            }catch(\Exception $e){
                $bug=$e->errorInfo[1];
            }
             if($bug==0){
            return redirect('product-create')->with('success','Successfully Inserted');
            }else{
                return redirect()->back()->with('error','Something Error Found ! ');
            }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       

       $data=Items::findOrFail($id);

     

        $attributeOption = ItemAttribute::where('fk_item_id',$id)->get()->keyBy('option_id');



        $attrubute=Attribute::where('status',1)->get();

       
        $categories=Category::where('status',1)->orderBy('category_name','ASC')->pluck('category_name','id');

        $Brands=Brand::where('status',1)->orderBy('name','ASC')->pluck('name','id');

        $subCategory = SubCategory::where('fk_category_id',$data->fk_category_id)->pluck('sub_category_name','id');

         $subsubCategory = SubSubCategory::where('fk_subsubcategory_id',$data->fk_subsubcategory_id)->pluck('subsub_category_name','id');

        
     
        return view('backend.product_add.edit',compact('data','attrubute','categories','Brands','subCategory','subsubCategory','attributeOption'));


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
               $input = $request->except('_token','_method');
        $input['updated_by']=Auth::user()->id;
        $input['link']=CommonController::slugify($input['link']);
        $validator = Validator::make($input, [
            'title' => 'required|max:255',
            'product_code' => "required|unique:items,product_code,$id|max:10",
            'link' => "required|unique:items,link,$id|max:50",
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()], 422);
        }
        DB::beginTransaction();
        $data = Items::findOrFail($id);
        $data->update($input);

        ItemPhoto::imageUpdate($request,$id);
      if(isset($input['option_id'])){
            ItemAttribute::where('fk_item_id',$id)->delete();
            foreach($input['option_id'] as $key => $value){
                $attrId = SubAttribute::where('id',$key)->value('fk_attribute_id');
                $details = '';
           
                ItemAttribute::create([
                    'attribute_id'=>$attrId,
                    'option_id'=>$key,
                    'fk_item_id'=>$id,
                    'details'=>$details,

                ]);
            }

        }

      


        try{


            DB::commit();
            $bug=0;
        }catch(\Exception $e){
            DB::rollback();
            $bug=$e->errorInfo[1];
            $bug1=$e->errorInfo[2];
        }
        if($bug==0){
            return redirect()->back()->with('success','Data Successfully Updated!');
        }elseif($bug==1451){
            return redirect()->back()->with('error','Data is used anywhere!');
        }else{
            return redirect()->back()->with('error','Some thing error found !');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      

       $order=OrderItems::where('fk_item_id',$id)->count();

     if($order > 0){
          return redirect()->back()->with('error','This Data is Used anywhere ! ');
     }

     else{

  
          
           
        $itemPhoto=ItemPhoto::where('fk_item_id',$id)->get();
            foreach ($itemPhoto as $row){
                $img_path1='images/items/big/'.$row['photo'];
                if($row['photo']!=null and file_exists($img_path1)){
                    unlink($img_path1);
                }
                $img_path2='images/items/small/'.$row['photo'];
                if($row['photo']!=null and file_exists($img_path2)){
                    unlink($img_path2);
                }
                ItemPhoto::where('fk_item_id',$id)->delete();

            }
            
            ItemAttribute::where('fk_item_id',$id)->delete();

            $data=Items::findOrFail($id);

        try{
            $data->delete();
            $bug=0;
            $error=0;
        }catch(\Exception $e){
            $bug=$e->errorInfo[1];
            $error=$e->errorInfo[2];
        }
        if($bug==0){
            return redirect()->back()->with('success','Data has been Successfully Deleted!');
        }elseif($bug==1451){
            return redirect()->back()->with('error','This Data is Used anywhere ! ');

        }
        elseif($bug>0){
            return redirect()->back()->with('error','Some thing error found !');

        }
      }
    }



        public function loadSubCat($id)
    {
       

        $subCategory=SubCategory::where(['status'=>1,'fk_category_id'=>$id])->orderBy('sub_category_name','ASC')->pluck('sub_category_name','id');

       

         

        return view('backend.product_add.loadSubCategory',compact('subCategory'));
    }

    public function loadSubSubCat($id)
    {
        $subsubCategory=SubSubCategory::where(['status'=>1,'fk_subsubcategory_id'=>$id])->orderBy('subsub_category_name','ASC')->pluck('subsub_category_name','id');
        return view('backend.product_add.loadSubsubCategory',compact('subsubCategory'));
    }

}
