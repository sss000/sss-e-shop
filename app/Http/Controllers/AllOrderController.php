<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Model\Order;
use App\Attributes;
use App\SubAttribute;
use App\PrimaryInfo;
use App\Model\OrderItems; 
use App\Items;
use Auth;


class AllOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $allData=Order::leftJoin('users','orders.fk_user_id','users.id')->select('orders.id','orders.invoice_id','orders.total_amount','orders.status','name','phone_number','email')->orderBy('orders.id','DESC')
        ->where('orders.status',1)->paginate(20);

        //return $allData;
        return view('backend.order.orders',compact('allData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         $info=PrimaryInfo::first();
        $data=Order::leftJoin('users','orders.fk_user_id','users.id')->select('orders.*','name','phone_number','email')->where('orders.id',$id)->first();
    



        return view('backend.order.details',compact('data','cart','allAttribute','info'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
         $data=Order::findOrFail($id);
        $input['status']=$request->action;
       $data->update($input);

        $order = Order::where(['id'=>$id])->first();
       $info = PrimaryInfo::first();
        \Mail::send('backend.order.email', ['order' => $order,'info'=>$info], function ($m) use ($order,$info) {
            $m->from($info->email, $info->company_name);

            $m->to($order->user->email, $order->user->name)->subject('Your Order List at ',$info->company_name);
        });


         return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input=$request->except('_token');
        $data=Order::findOrFail($id);
        $data->update([
            'delivered_by'=>$input['delivered_by'],
            'date_time'=>date('Y-m-d H:i:s',strtotime($input['date_time'])),
            'status'=>4,
        ]);

          $info = PrimaryInfo::first();
        \Mail::send('backend.order.email', ['order' => $data,'info'=>$info], function ($m) use ($data,$info) {
            $m->from($info->email, $info->company_name);

            $m->to($data->user->email, $data->user->name)->subject('Your Order List at ',$info->company_name);
        });
    
        return redirect()->back()->with('success','Delivered Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Order::findOrFail($id);
        $items=OrderItems::where('fk_order_id','=',$id)->get();
        foreach ($items as $value) {
            $value->delete();
        }
        try{
            $data->delete();
            $bug=0;
            $error=0;
        }catch(\Exception $e){
            $bug=$e->errorInfo[1];
            $error=$e->errorInfo[2];
        }
        if($bug==0){
            return redirect()->back()->with('success','Data has been Successfully Deleted!');
        }elseif($bug==1451){
            return redirect()->back()->with('error','This Data is Used anywhere ! ');

        }
        elseif($bug>0){
            return redirect()->back()->with('error','Some thing error found !');

        }
    }



        public function received()
    {
        $allData=Order::leftJoin('users','orders.fk_user_id','users.id')->orderBy('id','desc')
        ->select('orders.id','orders.invoice_id','phone_number','orders.total_amount','email','name','orders.status')->where('orders.status',2)
        ->paginate(20);
        return view('backend.order.orders',compact('allData'));
    }

    public function delivered()
    {
        $allData=Order::leftJoin('users','orders.fk_user_id','users.id')->orderBy('id','desc')
        ->select('orders.id','orders.invoice_id','phone_number','orders.total_amount','email','name','orders.status')
        ->where('orders.status',4)
        ->paginate(20);



        return view('backend.order.orders',compact('allData'));
    }
 public function cancelOrder()
    {
        $allData=Order::leftJoin('users','orders.fk_user_id','users.id')->orderBy('id','desc')
        ->select('orders.id','orders.invoice_id','phone_number','orders.total_amount','email','name','orders.status')
        ->where('orders.status',0)
        ->paginate(20);
        return view('backend.order.orders',compact('allData'));
    }


}
