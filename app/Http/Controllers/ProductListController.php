<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubCategory; 
use App\SubSubCategory;
use App\Items;

class ProductListController extends Controller 
{ 
    
    public function productList($id){ 
    	

    	$items=Items::where('status',1)->where('fk_category_id','=',$id)->get();

        $categories=Category::where('status',1)->get();

    	$cate=Category::findOrFail($id);

    	return view('frontend.list-product.listproduct',compact('items','categories','cate'));

 
    }


     

    	 public function subproductList($id){



		     $items=Items::where('status',1)->where('fk_sub_category_id','=',$id)->get();

		    	$categories=Category::where('status',1)->get();

                $subcate=SubCategory::findOrFail($id);

		    	return view('frontend.list-product.subcategoryproduct',compact('items','categories','subcate'));
 

       }   

        public function ssproductList($id){

                $sscate=SubSubCategory::findOrFail($id);
		     $items=Items::where('status',1)->where('fk_subsubcategory_id','=',$id)->get();

		    	$categories=Category::where('status',1)->get();

		    	return view('frontend.list-product.subsubcategoryproduct',compact('items','categories','sscate'));


		       }


}
