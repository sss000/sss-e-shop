<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Order;
use App\Model\OrderItems;
use Auth;
use Cart;
use DB;

use Session;
use Validator;



class OrderViewController extends Controller
{
    public function orderForm(Request $request){

        $input = $request->all();
 
      $validator = Validator::make($request->all(),[
           
           
            'shipping_amount' => 'required',
            'total_amount' => 'required',
            'payment_method_id' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }


       

   
        $invoice = Order::max('id')+1;
        $order = Order::create([
            'shipping_address'=>$request->shipping_address,
            'billing_address'=>Auth::user()->address,
            'fk_user_id'=>Auth::user()->id,
            'invoice_id'=>$invoice,
            'total_amount'=>$request->total_amount,
            'payment_method_id'=>$request->payment_method_id,
            'delivery_id'=>$request->delivery_id,
            'total'=>$input['total'],
            'status'=>1,
            'delivered_by'=>'',
            'date_time'=>'',
            'shipping_amount'=>$request->shipping_amount,
            'transaction_no'=>$request->transaction_no,

        ]);

       
        foreach(\Cart::content() as $cart){
            OrderItems::create([
                'fk_order_id'=> $order->id,
                'fk_item_id'=>$cart->id,
                'quantity'=>$cart->qty,
                'price'=>$cart->price,
                'amount'=>$cart->subtotal,
                'attributes'=>json_encode($cart->options['attr_details'],true),

            ]);
        }
        try{

            $bug=0;
        }catch(\Exception $e){
            $bug=$e->errorInfo[1];
        }
        if($bug==0){
            Cart::destroy();
            return redirect('order/'.$order->id)->with('success','Successfully Ordered');
        }else{
            return redirect()->back()->with('error','Something Error Found ! ');
        }

    }


       public function order($id){



        $order = Order::where(['id'=>$id,'fk_user_id'=>Auth::user()->id])->first();

         /*$$order=Order::leftjoin('order_item','orders.fk_order_id','orders.id')->select('order_item.*','orders.*')->where(['id'=>$id,'fk_user_id'=>Auth::user()->id])->first();
*/
        if($order==null){
            return redirect()->back();
        }

        

       

        return view('frontend.order.order',compact('order'));

        
    }


   
}
