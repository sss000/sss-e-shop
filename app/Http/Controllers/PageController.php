<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Page;


class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        
           $allData=Page::orderBy('id','desc')
            ->paginate(15);
        return view('backend.page.index',compact('allData'));
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('backend.page.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
   {

        $validator = Validator::make($request->all(), [  
                    'link'  => 'required|max:50|unique:page,link', 
                    'name'  => 'required', 
                    'title' => 'required',
                    'file' => 'mimes:pdf',
                ]);
                if ($validator->fails()) {
                    return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
                }
            $input = $request->all();
            $link=str_replace(' , ', '-', $input['link']);
            $link=str_replace(', ', '-', $link);
            $link=str_replace(' ,', '-', $link);
            $link=str_replace(',', '-', $link);
            $link=str_replace('/', '-', $link);
            $link=rtrim($link,' ');
            $link=str_replace(' ', '-', $link);
            $link=strtolower($link);
            $input['link']=$link;
            /*Upload PDF File*/
            if ($request->hasFile('file')) {
                $file=$request->file('file');
                $fileType=$file->getClientOriginalExtension();
                $fileName=rand(1,1000).date('dmyhis').".".$fileType;
                /*$fileName=$file->getClientOriginalName();*/
                $file->move('public/files/page',$fileName);
                $input['file']=$fileName;
            }
    
            $currentId=Page::create($input)->id;
            $input2=array();
      
            

            try{
                
            $bug=0;
            }catch(\Exception $e){
                $bug=$e->errorInfo[1];
            }
             if($bug==0){
            return redirect("pages")->with('success','Data Successfully Inserted');
            }else{
                return redirect()->back()->with('error','Something Error Found ! ');
             }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Page::findOrFail($id);
       
        return view('backend.page.packgedetails',compact('data'));
    }
 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Page::findOrFail($id);
     
        return view('backend.page.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $input = $request->all();
        $data=Page::findOrFail($request->id);
        $link=str_replace(' , ', '-', $input['link']);
        $link=str_replace(', ', '-', $link);
        $link=str_replace(' ,', '-', $link);
        $link=str_replace(',', '-', $link);
        $link=str_replace('/', '-', $link);
        $link=rtrim($link,' ');
        $link=str_replace(' ', '-', $link);
        $link=strtolower($link);
        $input['link']=$link;
        $validator = Validator::make($request->all(), [
                    'name'      => 'required', 
                    'title'  => 'required',
                    'link'  => "required|max:50|unique:page,link,$id", 
                ]);
        
                if ($validator->fails()) {
                    return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
                }
            /*Upload PDF File*/
            if ($request->hasFile('file')) {
                $file=$request->file('file');
                $fileType=$file->getClientOriginalExtension();
                $fileName=rand(1,1000).date('dmyhis').".".$fileType;
                /*$fileName=$file->getClientOriginalName();*/
                $file->move('public/files/page',$fileName);
                $input['file']=$fileName;
                $file_path='public/files/page/'.$data['file'];

                if($data['file']!=null and file_exists($file_path)){
                unlink($file_path);
                }
            }
            /*-- Photo Upload --*/
    
            /*--Delete Photo--*/
        
        try{
            $data->update($input);
            

            $bug=0;
            }catch(\Exception $e){
                $bug=$e->errorInfo[1];
            }
             if($bug==0){
            return redirect()->back()->with('success','Data Successfully Updated');
            }else{
                return redirect()->back()->with('error','Something Error Found ! ');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Page::findOrFail($id);
      
    
        $file_path='public/files/page/'.$data['file'];
        if($data['file']!=null and file_exists($file_path)){
           unlink($file_path);
        }
       
       try{
        
        
            $data->delete();
            $bug=0;
            $error=0;
        }catch(\Exception $e){
            $bug=$e->errorInfo[1];
            $error=$e->errorInfo[2];
        }
        if($bug==0){
       return redirect('pages')->with('success','Data has been Successfully Deleted!');
        }elseif($bug==1451){
       return redirect('pages')->with('error','This Data is Used anywhere ! ');

        }
        elseif($bug>0){
       return redirect('pages')->with('error','Some thing error found !');

        }
    }
}
