<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\OrderMail;
use Illuminate\Mail\Mailer;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests;
use App\Model\Order;
use App\Attributes;
use App\SubAttribute;
use App\PrimaryInfo;
use App\Items;


class OrdersController extends Controller
{
   public function allOrder()
    {


    	
        $allData=Order::leftJoin('users','orders.fk_user_id','users.id')
        ->select('orders.id','orders.invoice_id','phone_number','orders.total_amount','email','name','orders.status')
        ->orderBy('orders.id','DESC')->latest('orders.created_at')->paginate(20);
        return view('backend.order.orders',compact('allData'));
    }


    



}
