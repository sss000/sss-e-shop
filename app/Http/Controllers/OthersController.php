<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\PrimaryInfo;
use Auth;
use Image;


class OthersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response



     */


        public function about()
    {
        $data=PrimaryInfo::first();
        
        
        return view('backend.otherInfo.about',compact('data'));

          

    }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)

    {   $data=PrimaryInfo::first();

        return view('backend.otherInfo.primaryInfo',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          $input = $request->all();
 
       if(isset($input['map_embed'])){
            /*Embed youtube video link */
            $video=explode('src="', $input['map_embed']);
            if(isset($video[1])){
                $video=explode('"',$video[1] );
            }
            $input['map_embed'] = $video[0];
        }
        
        $data=PrimaryInfo::findOrFail($request->id);
        
        $validator = Validator::make($input, [
                    'map_embed'          => 'url',
                ]);
        
                if ($validator->fails()) {
                    return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
                }
                if ($request->hasFile('image')) {
                    $photo=$request->file('image');
                    
                    $img=\Image::make($photo)->resize(570,480);
                    $img->save('images/about/image.jpg');
                    $input['image']='image.jpg';
                }



 
        if ($request->hasFile('logo')) {
                    $photo=$request->file('logo');
                    $img=\Image::make($photo)->resize(186,80);
                    $img->save('images/logo/logo.png');
                    $input['logo']='logo.png';
                }

    
              
        try{
            $data->update($input);
                
            $bug=0;
            }catch(\Exception $e){
                $bug=$e->errorInfo[1];
            }
             if($bug==0){
            return redirect()->back()->with('success','Successfully Update');
            }else{
                return redirect()->back()->with('error','Something Error Found ! ');
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
