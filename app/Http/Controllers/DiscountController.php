<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Discount;
use Image;

class DiscountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alldata=Discount::all();
       return view('backend.discount_offer.index',compact('alldata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.discount_offer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
    

        try{
            if ($request->hasFile('image')) {
                $photo=$request->file('image');
                $fileType=$photo->getClientOriginalExtension();
                $fileName=rand(1,1000).date('dmyhis').".".$fileType;
                $path2=base_path().'/images/discount/'.date('Y/m/d');
                if (!is_dir($path2)) {
                    mkdir("$path2",0777,true);
                    }
            
                $img = Image::make($photo);
                $img->resize(568, 340);
                $img->save('images/discount/'.date('Y/m/d/').$fileName);
                
                $input['image']='images/discount/'.date('Y/m/d/').$fileName;
            }

       
        Discount::create($input);
        $bug=0;
        }
        catch(\Exception $e){
            $bug=$e->errorInfo[1];
            $bug1=$e->errorInfo[2];
        }
         if($bug==0){
        return redirect('admin-discount')->with('success','discount offer Successfully Inserted');

        }elseif($bug==1062){
            return redirect()->back()->with('error','The link has already been taken.');
        }else{
            return redirect()->back()->with('error','Error: '.$bug1);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
