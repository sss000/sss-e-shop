<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\PrimaryInfo;
use App\SubCategory;
use App\SubSubCategory;
use App\Items;
use App\Brand;
use App\ItemPhoto;
use App\Adsmanager;
use App\Slider;
use App\Discount;
use DB;
use Validator;
use Auth;

class HomeController extends Controller
{ 
    /**
     * Create a new controller instance.
     *
     * @return void 
     */
   
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    /*    $sliders=Items::leftjoin('categories','items.fk_category_id','categories.id')

                    ->select('categories.category_name','items.*')

                    ->where('banner',1)->get();*/

        $brand=Brand::orderBy('id','desc')->where('status',1)->get();
         $Discount=Discount::orderBy('id','desc')->where('status',1)->first();
         $sliders=Slider::where('status',1)->get();
          $product=Items::where('status',1)->take(16)->get();



          
          $categories=Category::where('status',1)->where('id','desc')->get();
            





          $adsmanager=Adsmanager::where('status',1)->get();

          $specials=Items::where('is_featured',1)->take(10)->get();
          
        return view('frontend.home',compact('sliders','categories','adsmanager','specials','Discount','product','brand'));
      
    }


       public function userLogin(){
        
        return view('auth.login');
    }


     public function search(Request $request){
     
        $datasearch= $request->all();

        $all_category =Category::where('status',1)->orderBy('serial_num','asc')->get();
        foreach ($all_category as  $value) {
              $all_sub_category[$value->id]=SubCategory::where('status',1)->where('fk_category_id',$value->id)->orderBy('serial_num','asc')->get();
        }




           
           $allData=Items::orderBy('items.id','desc')
                    ->where('items.status',1)
                    ->where('title','like','%'.$request->datasearch.'%')
                   
                    ->orWhere('additional_info','like','%'.$request->datasearch.'%')
                    ->orWhere('meta_description','like','%'.$request->datasearch.'%')
                    ->simplePaginate(12);



        return view('frontend.search-result',compact('allData','cat_name','datasearch','all_category','all_sub_category'));

     
         
    }


 
}
