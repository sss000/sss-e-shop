<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Model\Order;
use Validator;
use Auth;
use DB;

class ProfileController extends Controller
{
    
	public function userprofile(){





		return view('frontend.profile.profile');
	}

 public function changeUserProfile(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
                'name'     => 'required',
                'email'         => 'email|required'
            ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
   
        $input = $request->all();
        $id = $request['id'];
        $data = User::findOrFail($request->id);

        //print_r($data);exit;
        try{
            $data->update($input);
            $result=0;
        }catch(\Exception $e){
            $result = $e->errorInfo[1];
        }

        if($result==0){
        return redirect()->back()->with('success','Profile Successfully Updated');
        }elseif($result==1062){
            return redirect()->back()->with('error','The Email has already been taken.');
        }else{
        return redirect()->back()->with('error','Something Error Found ! ');
        }
    }


       public function orderList()
    {
        $userId = Auth::user()->id;
        
        $allData=Order::orderBy('id','desc')
        ->leftJoin('users','orders.fk_user_id','users.id')
        ->select('orders.id','orders.invoice_id','orders.total_amount','orders.status','name','email')
        ->where('orders.fk_user_id','=',$userId)
        ->paginate(20);
        
        return view('frontend.userOrderlist.userorderlist',compact('allData'));
    }



    }
