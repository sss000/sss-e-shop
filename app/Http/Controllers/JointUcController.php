<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\JoinUS;

class JointUcController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $alldata=JoinUS::paginate(10);



       return view('backend.jointus.index',compact('alldata'));
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
 

             $validator = Validator::make($input, [
            
                    'email'  => 'required|max:50|unique:join_us,email', 
                 
                   
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->with('error','There was a problem with the join us: This email address is already assigned to another user. ! ');
                }
    
   // $date = $request->input('date')->format('Y/m/d');
   
 

       JoinUS::create($input);
       
         try{
                
            $bug=0;
            }catch(\Exception $e){
                $bug=$e->errorInfo[1];
            }
             if($bug==0){
            return redirect()->back()->with('success','Thank you for your join us .');
            }else{
                return redirect()->back()->with('error','There was a problem with the join us: This email address is already assigned to another user. ! ');
             }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $data=JoinUS::findOrFail($id);

        try{
            $data->delete();
            $bug=0;
            $error=0;
        }catch(\Exception $e){
            $bug=$e->errorInfo[1];
            $error=$e->errorInfo[2];
        }
        if($bug==0){
       return redirect('join-us')->with('success','Data has been Successfully Deleted!');
        }elseif($bug==1451){
       return redirect('join-us')->with('error','This Data is Used anywhere ! ');

        }
        elseif($bug>0){
       return redirect('join-us')->with('error','Some thing error found !');

        }
    }
}
