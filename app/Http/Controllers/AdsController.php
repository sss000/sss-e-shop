<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Adsmanager;
use Auth;
use Image; 

class AdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $alldata=Adsmanager::all();
       return view('backend.ads.index',compact('alldata'));
    }

    /** 
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
         return view('backend.ads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
         $input = $request->except('_token');

       
       

        try{
            if ($request->hasFile('image')) {
                $photo=$request->file('image');
                $fileType=$photo->getClientOriginalExtension();
                $fileName=rand(1,1000).date('dmyhis').".".$fileType;
                $path2=base_path().'/images/ads/'.date('Y/m/d');
                if (!is_dir($path2)) {
                    mkdir("$path2",0777,true);
                    }
                
                /*$fileName=$feature_photo->getClientOriginalName();*/
                $img = Image::make($photo);
                $img->resize(350, 177);
                $img->save('images/ads/'.date('Y/m/d/').$fileName);
                
                $input['image']=date('Y/m/d/').$fileName;
            }

       
     Adsmanager::create($input);
        $bug=0;
        }catch(\Exception $e){
            $bug=$e->errorInfo[1];
            $bug1=$e->errorInfo[2];
        }
         if($bug==0){
        return redirect('add-manager')->with('success','Post Successfully Inserted');
        }elseif($bug==1062){
            return redirect()->back()->with('error','The link has already been taken.');
        }else{
            return redirect()->back()->with('error','Error: '.$bug1);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data=Adsmanager::findOrFail($id);
      return view('backend.ads.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $data=Adsmanager::findOrFail($id);
        $input = $request->except('_token');
        
       
       

        try{
            if ($request->hasFile('image')) {
                $photo=$request->file('image');
                $fileType=$photo->getClientOriginalExtension();
                $fileName=rand(1,1000).date('dmyhis').".".$fileType;
                $path=base_path().'/images/ads/'.date('Y/m/d');
                if (!is_dir($path)) {
                    mkdir("$path",0777,true);
                    }
                
                /*$fileName=$feature_photo->getClientOriginalName();*/
                $img = Image::make($photo);
                $img->resize(350, 177);
                $img->save('images/ads/'.date('Y/m/d/').$fileName);
               
                $input['image']=date('Y/m/d/').$fileName;

                /*Delete old photo*/
                $img_path1=base_path().'/images/ads/'.$data->image;
                

                if($data->image!=null){
                    if(file_exists($img_path1)){
                        unlink($img_path1);
                    }
                    
                }
            }

       $data->update($input);

        $bug=0;
        }catch(\Exception $e){
            $bug=$e->errorInfo[1];
            $bug1=$e->errorInfo[2];
        }
         if($bug==0){
        return redirect('add-manager')->with('success','Post Successfully Updated');
        }elseif($bug==1062){
            return redirect()->back()->with('error','The link has already been taken.');
        }else{
            return redirect()->back()->with('error','Error: '.$bug1);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
         $data=Adsmanager::where('id',$id)->first();
         /*Delete old photo*/
            $img_path1=base_path().'/images/ads/'.$data->images;
           

            if($data->images!=null){
                if(file_exists($img_path1)){
                    unlink($img_path1);
                }
                if(file_exists($img_path2)){
                    unlink($img_path2);
                }
            }
            $data->delete();
            $bug=0;
            $error=0;
        }catch(\Exception $e){
            $bug=$e->errorInfo[1];
            $error=$e->errorInfo[2];
        }
        if($bug==0){
       return redirect()->back()->with('success','Ad Successfully Deleted!');
        }elseif($bug==1451){
            return redirect()->back()->with('error','This ad is Used anywhere ! ');
        }
        elseif($bug>0){
       return redirect()->back()->with('error','Some thing error found !');

        } 
    }
}
