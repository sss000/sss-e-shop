<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Validator;
use Auth;
use DB;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
       

            $allUsers=User::all();

        return view('backend.users.index',compact('allUsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
        return view('backend.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validator = Validator::make($request->all(), [
                    'name' => 'required|max:20',
                    'email' => 'email|required|unique:users',
                    'password' => 'required|min:6|confirmed',
                    /*enable   extension=php_fileinfo*/ 
                ]);
                if ($validator->fails()) {
                    return redirect('users/create')
                        ->withErrors($validator)
                        ->withInput();
                }
                
            $input = $request->all();


            $input['password']=bcrypt($input['password']);
            $input['created_by']=Auth::user()->id;
           $insertId= User::create($input)->id;
        
            try{

            $bug=0;
            }catch(\Exception $e){
                $bug=$e->errorInfo[1];
            }
             if($bug==0){
            return redirect('users')->with('success','Data Successfully Inserted');
            }elseif($bug==1062){
                return redirect('users')->with('error','The Email has already been taken.');
            }else{
                return redirect('users')->with('error','Something Error Found ! ');
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $data=User::findOrFail($id);
        return view('backend.users.password',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data= User::findOrFail($request->id);

        $validator= validator::make($request->all(),
            [
                'name'  =>'required|Max:50',
                'email'  =>'email|required'

            ]);

        $input=$request->all();
        $data->update($input);

        try{
            $result=0;
        }
        catch(\Exception $e)

        {
            $result=$e->errorInfo[1];
        }

        if($result==0){

            return redirect()->back()->with('success','Profile Successfully updated');

        }elseif ($result==1062) {

            return redirect()->back()->with('error','the name has already been taken');
        }else{
            return redirect()->back()->with('error','Something error taken !');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $data=User::findOrFail($id);
       
       try{
            $data->delete();
            $bug=0;
            $error=0;
        }catch(\Exception $e){
            $bug=$e->errorInfo[1];
            $error=$e->errorInfo[2];
        }
        if($bug==0){
       return redirect('users')->with('success','Data has been Successfully Deleted!');
        }elseif($bug==1451){
       return redirect('users')->with('error','This Data is Used anywhere ! ');

        }
        elseif($bug>0){
       return redirect('users')->with('error','Some thing error found !');

        }
    }
}
 