<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\SubCategory;
use App\SubSubCategory;
use App\SubAttribute;
use App\Attribute;
use App\ItemPhoto;
use App\ItemAttribute;
use App\Brand;
use App\Items; 
use Image;
use Validator;
use DB;

class ShoppingviewController extends Controller
{
	public function productDetails($id){
		$item = Items::findOrFail($id);
		return redirect("product-view/$item->link");
	}

   public function viewProduct($link){
      
   	$products=Items::where('link',$link)->first();
   	if($products==null){
   		return redirect()->back();
   	}
   	
   

   	 $related_product=Items::where('status','1')->where('fk_category_id','=',$products->fk_category_id)->where('id','NOT LIKE',$products->id)->orderby('id','DSC')->take(8)->get();

   	

   	  return view('frontend.singleProduct',compact('products','related_product'));
   }

   

   
}  

 	/*$products=Items::leftjoin('categories','items.fk_category_id','categories.id')
   					->select('categories.category_name','items.*')	
   					 ->where('link',$link)->first();*/