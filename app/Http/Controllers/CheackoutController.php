<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\PaymentMethode;
use Cart;
use DB;
use Auth;
use Session;

class CheackoutController extends Controller
{
    public function checkoutView(){

    $paymentmethode=PaymentMethode::where('status',1)->orderBy('id','ASC')->pluck('name','id');

     
                $data=User::where('id',Auth::user()->id)->first();
            
        

    	return view('frontend.checkout.checkout',compact('data','paymentmethode'));
 
    }


   public function loadInstruction($id)
    {
        $paymentmethode=PaymentMethode::findOrFail($id);
        return view('frontend.checkout.showInstruction',compact('paymentmethode'));
    } 






 public function cartDetail(){
        
        $total = floatval(str_replace(',','', Cart::total()));
        $cart = [
            'totalAmount'=>$total,
            'total'=>Cart::total(),
            'tax'=>Cart::tax(),
            'subtotal'=>Cart::subtotal(),
            'count'=>Cart::count(),
            'content'=>Cart::content(),
        ];


        return view('frontend.checkout.checkout',compact('cart'));
    }



}
