<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JoinUS extends Model
{
    protected $table='join_us';
    
    protected $fillable=['email'];
}
