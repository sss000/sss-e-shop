<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
     protected $table="slider";
    protected $primarykey="id";
    protected $fillable=['serial','top_caption','middle_caption','buttom_caption','image','categories_id','status'];
}
