<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubAttribute extends Model
{
    protected $table='sub_attribute';
    protected $fillable=['option_name','status','description','fk_attribute_id'];

       public function attribute(){
        return $this->belongsTo(Attribute::class,'fk_attribute_id','id');
    }
}
