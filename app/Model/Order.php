<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
     protected $table="orders";
  
    protected $fillable=['shipping_address','billing_address','fk_user_id','invoice_id','total_amount','payment_method_id','total','delivery_id','shipping_amount','transaction_no','delivered_by','date_time','status'];


  

    public function hasproduct(){
        return $this->hasMany('App\Model\OrderItems','fk_order_id','id');
    }

          public function hasMethode()
    {
        return $this->belongsTo('App\PaymentMethode','payment_method_id','id');
    }


      public function user(){
        return $this->belongsTo('App\User','fk_user_id','id');
    }

         public function item(){
        return $this->belongsTo('App\Items','fk_user_id','id');
    }
}
