<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
     protected $table="order_item";
  
    protected $fillable=['fk_order_id','fk_item_id','quantity','price','attributes','amount'];


    public function hasItems()
    {
        return $this->belongsTo('App\Model\Order','id','fk_order_id');
    }


        public function hasItem()
    {
        return $this->belongsTo('App\Items','fk_item_id','id');
    }

    

 }