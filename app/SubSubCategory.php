<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubSubCategory extends Model
{
   protected $table='subsub_categories';
    protected $fillable=['subsub_category_name','status','serial_num','fk_subsubcategory_id','created_by','sub_link','subsub_description','subsub_keywords'];


         public function subcategori(){
         	
        return $this->belongsTo(SubCategory::class,'fk_subsubcategory_id','id');
    }

}
