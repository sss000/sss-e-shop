<?php

/* 
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/   

/*Route::get('/', function () {
    return view('frontend.home');
});*/ 


 
  

 
Auth::routes();

Route::get('contact-us', function(){

	return view('frontend.contact.contact');
});



Route::get('/home',function(){
	return redirect('dashboard');
});

Route::resource('join-us','JointUcController');
Route::get('search', 'HomeController@search');

Route::get('/','HomeController@index');

Route::get('product-details/{id}','ShoppingviewController@productDetails');
Route::get('product-view/{link}','ShoppingviewController@viewProduct');



Route::post('add-to-cart','ShoppingController@addToCart');

Route::get('cart','ShoppingController@cartDetails');
Route::get('Special-product','SpecialsShowController@specialProduct');

 
Route::get('category-product/{link}','ProductListController@productList');
Route::get('sub-category-product/{id}','ProductListController@subproductList');
Route::get('sub-sub-category-product/{id}','ProductListController@ssproductList');

Route::get('page/{link}', 'PagesController@show');


 


Route::middleware(['admin'])->group(function () {

Route::get('checkout','CheackoutController@checkoutView');
//Route::get('cart-show','CheackoutController@cartDetail');
Route::get('load-payment-methode/{id}','CheackoutController@loadInstruction');
Route::post('order-form','OrderViewController@orderForm');
Route::get('order/{id}','OrderViewController@order');
Route::get('/user-profile','ProfileController@userprofile');
Route::post('/userProfileUpdate','ProfileController@changeUserProfile');
Route::get('/myOrderList','ProfileController@orderList');
Route::get('wishlist','WishlistController@index');
Route::get('wishlist-store','WishlistController@store');
Route::get('wishlist-delete/{id}','WishlistController@delete');

 
	Route::group(['middleware'=>['auth']],function(){



		Route::resource('/users', 'UsersController');
		Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

		Route::resource('pages','PageController');
		Route::resource('menu','MenuController');
		Route::resource('sub-menu','SubmenuController');
		Route::resource('sub-sub-menu','SubsubmenuController');
		Route::resource('others-info','OthersController');
		Route::resource('Slider-info','SliderController');
		Route::resource('Social-links','SociallinkController');
		Route::resource('payment-methode','PaymentController');

		Route::resource('admin-discount','DiscountController');


		Route::get('other/about','OthersController@about');
		
		Route::get('page-menu','MenuController@page');

		Route::resource('category','CategoryController');
		Route::resource('sub-category','SubcategoryController');
		Route::resource('subsub-category','SubSubCategoryController');
		Route::resource('attribute','AttributeController');

		Route::resource('sub-attribute','SubAttributeController');
		Route::resource('all-brand','BrandController');
		Route::resource('add-manager','AdsController');

		Route::resource('product-create','ProductController');
		Route::get('load-sub-cat/{id}','ProductController@loadSubCat');
		Route::get('load-subsub-cat/{id}','ProductController@loadSubSubCat');
		Route::resource('orders','AllOrderController');

		Route::get('delivered','AllOrderController@delivered');
        Route::get('cancel-order','AllOrderController@cancelOrder');

        Route::get('received','AllOrderController@received');

		

    });

	Route::get('/cart-remove-item/{rowId}', 'ShoppingController@remove');
	Route::get('/increment/{rowId}', 'ShoppingController@increment');
	Route::get('/decrement/{rowId}', 'ShoppingController@decrement');
	Route::get('/clear-cart', 'ShoppingController@cartClear');


 






});

