-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2018 at 03:10 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `database`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_company`
--

CREATE TABLE `about_company` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keyword` text COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fb_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `map_embed` varchar(455) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `about_company`
--

INSERT INTO `about_company` (`id`, `company_name`, `logo`, `image`, `keyword`, `address`, `mobile_no`, `email`, `fb_link`, `meta_description`, `description`, `map_embed`, `created_at`, `updated_at`) VALUES
(1, 'Daily Shoppers', 'logo.png', 'image.jpg', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humourThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humourThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour', '152/2/N, Panthapath, Dhaka 1209', '1215345646', 'admin1@gmail.com', 'http://facebook.com/ComputersSchool/', 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of us', '<p class=\"MsoNormal\" style=\"margin-bottom: .0001pt; text-align: justify; text-justify: inter-ideograph;\"><span style=\"font-family: \'Open Sans\', Arial, sans-serif;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</span><span style=\"font-family: \'Open Sans\', Arial, sans-serif;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</span><span style=\"font-family: \'Open Sans\', Arial, sans-serif;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</span><span style=\"font-family: \'Open Sans\', Arial, sans-serif;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</span><span style=\"font-family: \'Open Sans\', Arial, sans-serif;\">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour</span></p>', 'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d116818.32584103779!2d90.41549295859971!3d23.798226433079737!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c766dae163a9%3A0x6f0612fa8107b57c!2sSmart+Software+Inc!5e0!3m2!1sen!2sbd!4v1531990860748', NULL, '2018-08-06 05:53:19');

-- --------------------------------------------------------

--
-- Table structure for table `ads_manager`
--

CREATE TABLE `ads_manager` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(2) NOT NULL,
  `image` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ads_manager`
--

INSERT INTO `ads_manager` (`id`, `name`, `status`, `image`, `updated_at`, `created_at`) VALUES
(1, 'Digital camera', 0, '2018/08/12/517120818064442.jpg', '2018-08-18 04:18:28', '2018-08-06 23:25:43'),
(3, 'new style', 0, '2018/08/07/138070818092429.png', '2018-08-18 04:18:35', '2018-08-07 03:24:29');

-- --------------------------------------------------------

--
-- Table structure for table `attribute`
--

CREATE TABLE `attribute` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(2) NOT NULL,
  `type` tinyint(2) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `attribute`
--

INSERT INTO `attribute` (`id`, `name`, `description`, `status`, `type`, `updated_at`, `created_at`) VALUES
(3, 'Color', 'Color', 1, 1, '2018-07-22 02:49:42', '2018-07-22 02:49:42'),
(4, 'size', 'size', 1, 0, '2018-07-23 05:23:54', '2018-07-23 05:23:54');

-- --------------------------------------------------------

--
-- Table structure for table `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial` tinyint(2) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `image` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brand`
--

INSERT INTO `brand` (`id`, `name`, `serial`, `status`, `image`, `created_at`, `updated_at`) VALUES
(1, 'amazon', 1, 1, '2018/08/19/788190818061132.png', '2018-08-19 06:11:33', '2018-08-19 00:11:33'),
(2, 'IPM', 2, 1, '2018/08/19/623190818061148.png', '2018-08-19 00:11:48', '2018-08-19 00:11:48'),
(3, 'Upwork', 3, 1, '2018/08/19/138190818061202.png', '2018-08-19 00:12:02', '2018-08-19 00:12:02'),
(4, 'ENvato', 4, 1, '2018/08/19/53190818061217.png', '2018-08-19 00:12:17', '2018-08-19 00:12:17'),
(5, 'Ama', 5, 1, '2018/08/19/330190818061256.png', '2018-08-19 00:12:56', '2018-08-19 00:12:56'),
(6, 'ENvato', 6, 1, '2018/08/19/898190818061520.png', '2018-08-19 00:15:20', '2018-08-19 00:15:20');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_num` tinyint(4) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `keyword` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `show_home` tinyint(4) NOT NULL DEFAULT '0',
  `Show_top_menu` tinyint(4) NOT NULL DEFAULT '0',
  `link` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_name`, `serial_num`, `description`, `keyword`, `status`, `show_home`, `Show_top_menu`, `link`, `created_by`, `created_at`, `updated_at`) VALUES
(5, 'Men’s Fashion', 1, 'Men\'s Fashion Week is an event usually held twice a year in major cities around the globe, typically in January (Autumn/Winter) and June (Spring/Summer). The fashion industry event is held for fashion professionals and fashion occult. It is an event for designers and brands to showcase upcoming collections to press, media, stylists, buyers, fashion trailblazers and, in some cases, the public.', 'Men\'s Fashion Week is an event usually held twice a year in major cities around the globe, typically in January (Autumn/Winter) and June (Spring/Summe', 1, 1, 1, 'men-s-fashion', '1', '2018-08-19 00:41:56', '2018-08-19 00:41:56'),
(6, 'Women’s Fashion', 2, 'Saree is very common dress in this sub continental area like Bangladesh. Women look very gorgeous in colorful sarees, and so they love to wear and have luxurious and colorful sarees for different social and cultural functions. Young lady to elderly person - every woman is fascinated about sarees just because of our culture.', 'Saree is very common dress in this sub continental area like Bangladesh. Women look very gorgeous in colorful sarees, and so they love to wear and hav', 1, 1, 1, 'women-s-fashion', '1', '2018-08-19 00:43:35', '2018-08-19 00:43:35'),
(7, 'Baby, Kids & Toys', 3, 'baby-kids Baby, Kids & Toys Baby, Kids & Toys Baby, Kids & Toys', 'Baby, Kids & Toys Baby, Kids & Toys Baby, Kids & Toys Baby, Kids & Toys Baby, Kids & Toys', 1, 1, 1, 'baby-kids', '1', '2018-08-19 00:45:06', '2018-08-19 00:45:06'),
(8, 'Books & Stationary', 4, 'Books & Stationary', 'Books & Stationary,', 1, 1, 1, 'books-stationay', '1', '2018-08-19 00:45:41', '2018-08-19 00:45:41'),
(9, 'Home & Living', 5, 'Home & Living', 'Home & Living', 1, 1, 1, 'home-living', '1', '2018-08-19 00:46:08', '2018-08-19 00:46:08'),
(10, 'Mobiles & Computer', 6, 'Mobiles & Computer', 'Mobiles & Computer', 1, 1, 1, 'mobile-computer', '1', '2018-08-19 00:46:39', '2018-08-19 00:46:39');

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE `discount` (
  `id` int(11) NOT NULL,
  `top_caption` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_caption` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `buttom_caption` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(5) NOT NULL,
  `image` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `discount`
--

INSERT INTO `discount` (`id`, `top_caption`, `middle_caption`, `buttom_caption`, `url`, `status`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Child dress', 'White Cotton Sports Round dress', '15% OFF', 'child-111', 1, 'images/discount/2018/08/13/693130818115949.png', '2018-08-13 05:59:50', '2018-08-13 05:59:50');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `rating` float DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `meta_description` text COLLATE utf8_unicode_ci,
  `additional_info` text COLLATE utf8_unicode_ci,
  `fk_category_id` int(10) NOT NULL,
  `fk_sub_category_id` int(10) DEFAULT NULL,
  `fk_subsubcategory_id` int(10) DEFAULT NULL,
  `fk_brand_id` int(10) DEFAULT NULL,
  `wholesale_price` float DEFAULT NULL,
  `price` float DEFAULT '0',
  `discount` float DEFAULT NULL,
  `quantity` int(11) DEFAULT '0',
  `is_featured` int(4) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `is_approved` tinyint(3) DEFAULT '0' COMMENT '1=Yes, 2=Pending,3=No',
  `approved_by` int(11) UNSIGNED DEFAULT NULL,
  `visitor` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `updated_by` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `title`, `product_code`, `link`, `rating`, `description`, `meta_description`, `additional_info`, `fk_category_id`, `fk_sub_category_id`, `fk_subsubcategory_id`, `fk_brand_id`, `wholesale_price`, `price`, `discount`, `quantity`, `is_featured`, `status`, `is_approved`, `approved_by`, `visitor`, `created_at`, `updated_at`, `updated_by`) VALUES
(1, 'baby dress', 'baby-001', 'baby-001', NULL, '<p><strong style=\"margin: 0px; padding: 0px; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</span></p>', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', NULL, 7, 26, 51, 6, NULL, 1450, 1400, 15, 1, 1, 0, NULL, 0, '2018-08-26 23:51:53', '2018-08-27 04:25:56', '1'),
(2, 'Beautiful Pant', 'shirt-115', 'kids-product', NULL, '<p><strong style=\"margin: 0px; padding: 0px; font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">Lorem Ipsum</strong><span style=\"font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,</span></p>\r\n<p><span style=\"font-family: \'Open Sans\', Arial, sans-serif; text-align: justify;\"><strong style=\"margin: 0px; padding: 0px;\">Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</span></p>', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', NULL, 5, 6, 6, 5, NULL, 600, 400, 15, 1, 1, 0, NULL, 0, '2018-09-01 02:46:51', '2018-09-11 06:03:39', '1'),
(4, 'Beautiful Pant', 'shirt-113', 'shirt-four', NULL, '<p>hgjf</p>', 'hgjf', NULL, 5, 13, NULL, NULL, NULL, 908, 787, 9, 0, 1, 0, NULL, 0, '2018-09-03 04:26:33', '2018-09-03 04:26:33', NULL),
(5, 'Children\'s clothing and gender', 'shirt-112', 'Kids-products', NULL, '<p>ererwetetret</p>', 'hmmmm', NULL, 8, 30, 65, NULL, NULL, 1400, 1200, 15, 0, 1, 0, NULL, 0, '2018-09-03 04:45:49', '2018-09-03 04:45:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_attribute`
--

CREATE TABLE `item_attribute` (
  `id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) NOT NULL,
  `option_id` int(10) NOT NULL,
  `fk_item_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_attribute`
--

INSERT INTO `item_attribute` (`id`, `attribute_id`, `option_id`, `fk_item_id`, `created_at`, `updated_at`) VALUES
(25, 3, 1, 1, '2018-08-29 05:31:05', '2018-08-29 05:31:05'),
(26, 3, 2, 1, '2018-08-29 05:31:06', '2018-08-29 05:31:06'),
(27, 3, 6, 1, '2018-08-29 05:31:06', '2018-08-29 05:31:06'),
(28, 3, 7, 1, '2018-08-29 05:31:06', '2018-08-29 05:31:06'),
(29, 4, 3, 1, '2018-08-29 05:31:06', '2018-08-29 05:31:06'),
(30, 4, 4, 1, '2018-08-29 05:31:06', '2018-08-29 05:31:06'),
(31, 4, 8, 1, '2018-08-29 05:31:06', '2018-08-29 05:31:06'),
(32, 4, 9, 1, '2018-08-29 05:31:06', '2018-08-29 05:31:06'),
(33, 3, 2, 5, '2018-09-03 04:45:49', '2018-09-03 04:45:49'),
(34, 4, 4, 5, '2018-09-03 04:45:49', '2018-09-03 04:45:49'),
(37, 3, 2, 2, '2018-09-11 06:03:39', '2018-09-11 06:03:39'),
(38, 4, 4, 2, '2018-09-11 06:03:39', '2018-09-11 06:03:39');

-- --------------------------------------------------------

--
-- Table structure for table `item_photo`
--

CREATE TABLE `item_photo` (
  `id` int(10) UNSIGNED NOT NULL,
  `photo` longtext COLLATE utf8_unicode_ci NOT NULL,
  `fk_item_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `item_photo`
--

INSERT INTO `item_photo` (`id`, `photo`, `fk_item_id`, `created_at`, `updated_at`) VALUES
(13, '2018/08/29/987290818113105.jpg', 1, '2018-08-29 05:31:05', '2018-08-29 05:31:05'),
(14, '2018/09/01/158010918084650.jpg', 2, '2018-09-01 02:46:52', '2018-09-01 02:46:52'),
(15, '2018/09/01/516010918084651.jpg', 2, '2018-09-01 02:46:52', '2018-09-01 02:46:52'),
(18, '2018/09/03/31030918102632.jpg', 4, '2018-09-03 04:26:33', '2018-09-03 04:26:33'),
(19, '2018/09/03/422030918102633.jpg', 4, '2018-09-03 04:26:33', '2018-09-03 04:26:33'),
(20, '2018/09/03/234030918104549.jpg', 5, '2018-09-03 04:45:49', '2018-09-03 04:45:49');

-- --------------------------------------------------------

--
-- Table structure for table `join_us`
--

CREATE TABLE `join_us` (
  `id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `join_us`
--

INSERT INTO `join_us` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'admin@gmail.com', '2018-08-29 01:38:16', '2018-08-29 01:38:16'),
(3, 'jorna@gmail.com', '2018-08-29 01:42:14', '2018-08-29 01:42:14'),
(4, 'saiful@gmail.com', '2018-08-29 02:12:44', '2018-08-29 02:12:44'),
(5, 'mskamrul1@gmail.com', '2018-08-29 02:17:07', '2018-08-29 02:17:07');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(4) NOT NULL,
  `serial_num` int(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `shipping_address` text COLLATE utf8_unicode_ci,
  `billing_address` text COLLATE utf8_unicode_ci,
  `fk_user_id` int(10) UNSIGNED NOT NULL,
  `invoice_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total_amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `delivery_id` int(11) DEFAULT NULL,
  `shipping_amount` float NOT NULL,
  `transaction_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivered_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_time` datetime NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=Processing,2=Received, 3=Pending,0=Cancel, 4=Dalivered',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `shipping_address`, `billing_address`, `fk_user_id`, `invoice_id`, `total_amount`, `total`, `payment_method_id`, `delivery_id`, `shipping_amount`, `transaction_no`, `delivered_by`, `date_time`, `status`, `created_at`, `updated_at`) VALUES
(1, 'mirpur 10', 'Dhaka', 1, '1', '1450', '1', 3, NULL, 50, '4556465465', 'admin', '2018-08-27 07:21:33', 4, '2018-08-27 00:19:55', '2018-08-27 01:21:53'),
(2, 'mirpur 10', 'Dhaka', 1, '2', '1450', '1', 2, NULL, 50, NULL, '', '0000-00-00 00:00:00', 2, '2018-08-27 00:57:35', '2018-08-27 03:15:27'),
(6, 'comilla', 'Dhaka', 1, '6', '1450', '1,400.00', 3, NULL, 50, '4556465465', '', '0000-00-00 00:00:00', 1, '2018-08-27 01:05:08', '2018-08-27 01:05:08'),
(7, 'comilla', 'Dhaka', 1, '7', '1450', '1,400.00', 2, NULL, 50, NULL, '', '0000-00-00 00:00:00', 2, '2018-08-27 01:09:00', '2018-08-29 04:09:39'),
(8, 'comilla', 'Dhaka', 1, '8', '1450', '1,400.00', 2, NULL, 50, NULL, '', '2018-08-27 09:41:38', 2, '2018-08-27 01:09:37', '2018-08-27 03:42:22');

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `id` int(11) NOT NULL,
  `fk_order_id` int(11) UNSIGNED NOT NULL,
  `fk_item_id` int(11) UNSIGNED NOT NULL,
  `quantity` float NOT NULL,
  `price` float NOT NULL,
  `amount` float NOT NULL,
  `attributes` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`id`, `fk_order_id`, `fk_item_id`, `quantity`, `price`, `amount`, `attributes`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1400, 1400, '{\"Color\":\"Red\",\"size\":\"L\"}', '2018-08-27 00:19:55', '2018-08-27 00:19:55'),
(2, 2, 1, 1, 1400, 1400, '{\"Color\":\"yellow\",\"size\":\"M\"}', '2018-08-27 00:57:36', '2018-08-27 00:57:36'),
(7, 6, 1, 1, 1400, 1400, '{\"Color\":\"White\",\"size\":\"S\"}', '2018-08-27 01:05:08', '2018-08-27 01:05:08'),
(8, 7, 1, 1, 1400, 1400, '{\"Color\":\"White\",\"size\":\"XL\"}', '2018-08-27 01:09:00', '2018-08-27 01:09:00'),
(9, 8, 1, 1, 1400, 1400, '{\"Color\":\"White\",\"size\":\"L\"}', '2018-08-27 01:09:37', '2018-08-27 01:09:37');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(4) NOT NULL,
  `link` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `name`, `title`, `description`, `status`, `link`, `file`, `created_at`, `updated_at`) VALUES
(1, 'product view', 'Our all Clients', 'A migration class contains two methods: up and down. The up method is used to add new tables, columns, or indexes to your database, while the down method should reverse the operations performed by the up method.', 1, 'client-list', NULL, '2018-07-17 06:54:25', '2018-07-17 06:54:25');

-- --------------------------------------------------------

--
-- Table structure for table `payment_methode`
--

CREATE TABLE `payment_methode` (
  `id` int(11) NOT NULL,
  `name` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_number` int(20) DEFAULT NULL,
  `details` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(2) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_methode`
--

INSERT INTO `payment_methode` (`id`, `name`, `account_number`, `details`, `status`, `description`, `created_at`, `updated_at`) VALUES
(2, 'cash in dalivery', NULL, NULL, 1, NULL, '2018-07-30 06:50:57', '2018-07-30 06:50:57'),
(3, 'Rocket', 1845791926, 'this is personal number', 1, '<p>whats\'up</p>', '2018-07-30 07:47:58', '2018-07-30 07:47:58'),
(4, 'Bkash', 1845791927, 'this is personal number', 1, '<p>all</p>', '2018-07-30 07:49:55', '2018-07-30 07:49:55');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `serial_num` tinyint(4) NOT NULL,
  `link` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Direction with map, 2 =Only two location, 3 = One Location, 4 = No Location',
  `is_air_ticket` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shipment`
--

CREATE TABLE `shipment` (
  `id` int(11) NOT NULL,
  `details` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tk` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipment`
--

INSERT INTO `shipment` (`id`, `details`, `tk`, `created_at`, `updated_at`) VALUES
(1, ' Delivered in 2 - 5 days at your doorstep(Around Dhaka) for TK:50.', 50, NULL, NULL),
(2, ' Delivered in 3 - 10 days at nearby pick-up station for TK: 100.', 100, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `serial` tinyint(2) NOT NULL,
  `image` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `top_caption` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_caption` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `buttom_caption` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categories_id` tinyint(3) NOT NULL,
  `status` tinyint(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `serial`, `image`, `top_caption`, `middle_caption`, `buttom_caption`, `categories_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'images/slider/2018/07/19/502190718101619.jpg', 'welcome', 'smart Software Inc', 'there are many people....', 1, 1, '2018-07-19 04:16:20', '2018-07-19 04:16:20');

-- --------------------------------------------------------

--
-- Table structure for table `social_icons`
--

CREATE TABLE `social_icons` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(2) NOT NULL,
  `icon_class` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serial_num` tinyint(2) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `social_icons`
--

INSERT INTO `social_icons` (`id`, `name`, `url`, `status`, `icon_class`, `serial_num`, `updated_at`, `created_at`) VALUES
(4, 'facebook', 'https://www.facebook.com/dailyshoppersbd', 1, 'images/social/2018/08/29/742290818070556.jpg', 0, '2018-08-29 01:05:56', '2018-08-29 01:05:56'),
(5, 'Twitter', 'https://www.twitter.com/dailyshoppersbd', 1, 'images/social/2018/08/29/560290818071430.png', 0, '2018-08-29 01:14:30', '2018-08-29 01:14:30'),
(6, 'Google plus', 'https://www.googleplus.com/dailyshoppersbd', 1, 'images/social/2018/08/29/610290818071508.png', 0, '2018-08-29 01:15:08', '2018-08-29 01:15:08'),
(7, 'Linkdin', 'https://www.linkdin.com/dailyshoppersbd', 1, 'images/social/2018/08/29/170290818071532.png', 0, '2018-08-29 01:15:32', '2018-08-29 01:15:32');

-- --------------------------------------------------------

--
-- Table structure for table `subsub_categories`
--

CREATE TABLE `subsub_categories` (
  `id` int(11) NOT NULL,
  `subsub_category_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(2) NOT NULL,
  `serial_num` tinyint(2) NOT NULL,
  `subsub_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subsub_keywords` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fk_subsubcategory_id` int(2) NOT NULL,
  `sub_link` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subsub_categories`
--

INSERT INTO `subsub_categories` (`id`, `subsub_category_name`, `status`, `serial_num`, `subsub_description`, `subsub_keywords`, `fk_subsubcategory_id`, `sub_link`, `updated_at`, `created_at`) VALUES
(6, 'Full sleeve', 1, 1, 'Full sleeve', 'Full sleeve', 6, 'full-sleeve', '2018-08-19 00:48:36', '2018-08-19 00:48:36'),
(7, 'Half sleeve', 1, 2, 'Half sleeve', 'Half sleeve', 6, 'half-sleeve', '2018-08-19 00:48:59', '2018-08-19 00:48:59'),
(8, 'Perfumes', 1, 1, 'Perfumes', 'Perfumes', 17, 'perfumes', '2018-08-19 00:53:00', '2018-08-19 00:53:00'),
(9, 'Deodorants', 1, 2, 'Deodorants', 'Deodorants', 17, 'deodorants', '2018-08-19 00:53:13', '2018-08-19 00:53:13'),
(10, 'Skins & Hair', 1, 3, 'Skins & Hair', 'Skins & Hair', 17, 'skins-hair', '2018-08-19 00:53:30', '2018-08-19 00:53:30'),
(11, 'Toiletries', 1, 4, 'Toiletries', 'Toiletries', 17, 'toiletries', '2018-08-19 00:53:43', '2018-08-19 00:53:43'),
(12, 'Salwar & Kameez', 1, 1, 'Salwar & Kameez', 'Salwar & Kameez', 19, 'salwar-kameez', '2018-08-19 01:02:13', '2018-08-19 01:02:13'),
(13, 'Saree', 1, 2, 'Saree', 'Saree', 19, 'saree', '2018-08-19 01:02:29', '2018-08-19 01:02:29'),
(14, 'Kurties & Tunics', 1, 3, 'Kurties & Tunics', 'Kurties & Tunics', 19, 'kurties-tunics', '2018-08-19 01:02:47', '2018-08-19 01:02:47'),
(15, 'Burkha & Hijab', 1, 4, 'Burkha & Hijab', 'Burkha & Hijab', 19, 'burkha-hijab', '2018-08-19 01:03:05', '2018-08-19 01:03:05'),
(16, 'Palazzo & Leggins', 1, 5, 'Palazzo & Leggins', 'Palazzo & Leggins', 19, 'palazzo-leggins', '2018-08-19 01:03:23', '2018-08-19 01:03:23'),
(17, 'T-Shirts', 1, 6, 'T-Shirts-womes', 'T-Shirts-womes', 19, 't-shirts-womes', '2018-08-19 01:03:42', '2018-08-19 01:03:42'),
(18, 'Shirts', 1, 7, 'Shirts-womens', 'Shirts-womens', 19, 'shirts-womens', '2018-08-19 01:04:02', '2018-08-19 01:04:02'),
(19, 'Pant', 1, 8, 'Pant women', 'Pant women', 19, 'pant-women', '2018-08-19 01:04:28', '2018-08-19 01:04:28'),
(20, 'Inner wear', 1, 9, 'Inner-wear-women', 'Inner wear women', 19, 'inner-wear-women', '2018-08-19 01:04:58', '2018-08-19 01:04:58'),
(21, 'Winter wear', 1, 10, 'Winter wear', 'Winter wear women', 19, 'winter-wear-women', '2018-08-19 01:05:29', '2018-08-19 01:05:29'),
(22, 'Un Stitched Than', 1, 11, 'Un Stitched Than', 'Un Stitched Than', 19, 'un-stitched-than', '2018-08-19 01:05:52', '2018-08-19 01:05:52'),
(23, 'Wedding Collection', 1, 12, 'Wedding Collection', 'Wedding Collection', 19, 'wedding-collection', '2018-08-19 01:06:10', '2018-08-19 01:06:10'),
(24, 'Tops', 1, 13, 'Tops', 'Tops', 19, 'tops', '2018-08-19 01:06:22', '2018-08-19 01:06:22'),
(25, 'Party wear', 1, 14, 'Party wear', 'Party wear', 19, 'party-wear', '2018-08-19 01:06:38', '2018-08-19 01:06:38'),
(26, 'Make up', 1, 1, 'Make up', 'Make up', 20, 'make-up', '2018-08-19 01:07:21', '2018-08-19 01:07:21'),
(27, 'Skin care', 1, 2, 'Skin care', 'Women Skin care', 20, 'women-skin-care', '2018-08-19 01:07:56', '2018-08-19 01:07:56'),
(28, 'Hair care', 1, 3, 'Hair care', 'Hair care', 20, 'women-hair-care', '2018-08-19 01:08:22', '2018-08-19 01:08:22'),
(29, 'Health', 1, 4, 'Health', 'Health women', 20, 'health-women', '2018-08-19 01:08:43', '2018-08-19 01:08:43'),
(30, 'Toiletries', 1, 5, 'Toiletries', 'Toiletries women', 20, 'toiletries-women', '2018-08-19 01:09:02', '2018-08-19 01:09:02'),
(31, 'Eye wear', 1, 6, 'Eye wear', 'Eye wear', 20, 'eye-wear', '2018-08-19 01:09:17', '2018-08-19 01:09:17'),
(32, 'Perfumes', 1, 7, 'Women-Perfumes', 'Women-Perfumes', 20, 'women-perfumes', '2018-08-19 01:09:38', '2018-08-19 01:09:38'),
(35, 'Bags', 1, 1, 'Women-Bags', 'Women-Bags', 21, 'women-bags', '2018-08-19 01:10:53', '2018-08-19 01:10:53'),
(36, 'Hand bags', 1, 2, 'Hand bags', 'Hand bags', 21, 'hand-bags', '2018-08-19 01:11:09', '2018-08-19 01:11:09'),
(37, 'Wattles & clutches', 1, 3, 'Wattles & clutches', 'Wattles & clutches', 21, 'wattles-clutches', '2018-08-19 01:11:39', '2018-08-19 01:11:39'),
(38, 'Backpacks', 1, 4, 'Backpacks', 'Backpacks', 21, 'backpacks', '2018-08-19 01:11:56', '2018-08-19 01:11:56'),
(39, 'Earring', 1, 1, 'Earring', 'Earring', 23, 'earring', '2018-08-19 01:12:26', '2018-08-19 01:12:26'),
(40, 'Necklace', 1, 2, 'Necklace', 'Necklace', 23, 'necklace', '2018-08-19 01:12:37', '2018-08-19 01:12:37'),
(41, 'Pendants', 1, 3, 'Pendants', 'Pendants', 23, 'pendants', '2018-08-19 01:12:50', '2018-08-19 01:12:50'),
(42, 'Rings', 1, 4, 'Rings', 'Rings', 23, 'rings', '2018-08-19 01:13:03', '2018-08-19 01:13:03'),
(43, 'Bungles & Bracelets', 1, 5, 'Bungles & Bracelets', 'Bungles & Bracelets', 23, 'bungles-bracelets', '2018-08-19 01:13:21', '2018-08-19 01:13:21'),
(44, 'Anklet', 1, 6, 'Anklet', 'Anklet', 23, 'anklet', '2018-08-19 01:13:35', '2018-08-19 01:13:35'),
(45, 'Heals', 1, 1, 'Heals', 'Heals', 24, 'heals', '2018-08-19 01:14:26', '2018-08-19 01:14:26'),
(46, 'Semi Heals', 1, 2, 'Semi Heals', 'Semi Heals', 24, 'semi-heals', '2018-08-19 01:14:49', '2018-08-19 01:14:49'),
(47, 'Flat Shoes', 1, 3, 'Flat Shoes', 'Flat Shoes', 24, 'flat-shoes', '2018-08-19 01:15:05', '2018-08-19 01:15:05'),
(48, 'Sneakers', 1, 4, 'Sneakers', 'Sneakers', 24, 'sneakers', '2018-08-19 01:15:16', '2018-08-19 01:15:16'),
(49, 'Sandals', 1, 5, 'Sandals', 'Sandals', 24, 'sandals', '2018-08-19 01:15:28', '2018-08-19 01:15:28'),
(50, 'Socks', 1, 6, 'Socks', 'Socks', 24, 'socks', '2018-08-19 01:15:40', '2018-08-19 01:15:40'),
(51, 'Babies Clothing', 1, 1, 'Babies Clothing', 'Babies Clothing', 26, 'babies-clothing', '2018-08-19 01:17:50', '2018-08-19 01:17:50'),
(52, 'Babies-Shoes', 1, 2, 'Babies Shoes', 'Babies Shoes', 26, 'babies-shoes', '2018-08-19 01:18:10', '2018-08-19 01:18:10'),
(53, 'Baby care', 1, 3, 'Baby care', 'Baby care', 26, 'baby-care', '2018-08-19 01:18:26', '2018-08-19 01:18:26'),
(54, 'Diaper', 1, 4, 'Diaper', 'Diaper', 26, 'diaper', '2018-08-19 01:18:37', '2018-08-19 01:18:37'),
(55, 'Bath Items', 1, 5, 'Bath Items', 'Bath Items', 26, 'bath-items', '2018-08-19 01:18:52', '2018-08-19 01:18:52'),
(56, 'Shirts & T-shirts', 1, 1, 'Shirts & T-shirts', 'Shirts & T-shirts', 27, 'shirts-t-shirts', '2018-08-19 01:19:52', '2018-08-19 01:19:52'),
(57, 'Pants', 1, 2, 'Pants', 'Pants baby', 27, 'pants-baby', '2018-08-19 01:20:19', '2018-08-19 01:20:19'),
(58, 'Punjabis', 1, 3, 'Punjabis', 'baby-Punjabis', 27, 'baby-punjabis', '2018-08-19 01:20:52', '2018-08-19 01:20:52'),
(59, 'Shoes', 1, 4, 'Shoes', 'Baby-Shoes', 27, 'baby-shoes', '2018-08-19 01:21:13', '2018-08-19 01:21:13'),
(60, 'Boys Accessories', 1, 5, 'Boys Accessories', 'Boys Accessories', 27, 'boys-accessories', '2018-08-19 01:21:29', '2018-08-19 01:21:29'),
(61, 'Dress', 1, 1, 'Dress-girls', 'Dress-girls', 28, 'dress-girls', '2018-08-19 01:22:08', '2018-08-19 01:22:08'),
(62, 'Frocks', 1, 2, 'Frocks', 'Frocks', 28, 'frocks', '2018-08-19 01:22:21', '2018-08-19 01:22:21'),
(63, 'Shoes', 1, 3, 'Shoes', 'Shoes', 28, 'girls-shoes', '2018-08-19 01:22:40', '2018-08-19 01:22:40'),
(64, 'Girls Accessories', 1, 4, 'Girls Accessories', 'Girls Accessories', 28, 'girls-accessories', '2018-08-19 01:22:56', '2018-08-19 01:22:56'),
(65, 'Children’s Books', 1, 1, 'Children’s Books', 'Children’s Books', 30, 'children-books', '2018-08-19 01:25:39', '2018-08-19 01:25:39'),
(66, 'School Books', 1, 2, 'School Books', 'School Books', 30, 'school-books', '2018-08-19 01:25:53', '2018-08-19 01:25:53'),
(67, 'College Books', 1, 3, 'College Books', 'College Books', 30, 'college-books', '2018-08-19 01:26:09', '2018-08-19 01:26:09'),
(68, 'University Books', 1, 4, 'University Books', 'University Books', 30, 'university-books', '2018-08-19 01:26:24', '2018-08-19 01:26:24'),
(69, 'Medical Books', 1, 5, 'Medical Books', 'Medical Books', 30, 'medical-books', '2018-08-19 01:26:36', '2018-08-19 01:26:36'),
(70, 'Pen Drives', 1, 1, 'Pen Drives', 'Pen Drives', 57, 'pen-drives', '2018-08-19 01:55:43', '2018-08-19 01:55:43'),
(71, 'Hard Drives', 1, 2, 'Hard Drives', 'Hard Drives', 57, 'hard-drives', '2018-08-19 01:55:57', '2018-08-19 01:55:57'),
(72, 'Printer', 1, 3, 'Printer', 'Printer', 57, 'printer', '2018-08-19 01:56:05', '2018-08-19 01:56:05'),
(73, 'Keyboard', 1, 4, 'Keyboard', 'Keyboard', 57, 'keyboard', '2018-08-19 01:56:20', '2018-08-19 01:56:20'),
(74, 'Mouse', 1, 5, 'Mouse', 'Mouse', 57, 'mouse', '2018-08-19 01:56:30', '2018-08-19 01:56:30'),
(75, 'Head phone', 1, 6, 'Head phone', 'Head phone', 57, 'head-phone', '2018-08-19 01:56:46', '2018-08-19 01:56:46'),
(76, 'Speakers', 1, 7, 'Speakers', 'Speakers', 57, 'speakers', '2018-08-19 01:56:56', '2018-08-19 01:56:56'),
(77, 'Games pads', 1, 8, 'Games pads', 'Games pads', 57, 'games-pads', '2018-08-19 01:57:11', '2018-08-19 01:57:11'),
(78, 'Graphics card', 1, 9, 'Graphics card', 'Graphics card', 57, 'graphics-card', '2018-08-19 01:57:27', '2018-08-19 01:57:27'),
(79, 'Router', 1, 10, 'Router', 'Router', 57, 'router', '2018-08-19 01:57:36', '2018-08-19 01:57:36'),
(80, 'Ear Phone', 1, 11, 'Ear Phone', 'Ear Phone', 57, 'ear-phone', '2018-08-19 01:57:53', '2018-08-19 01:57:53'),
(81, 'Cases & Covers', 1, 1, 'Cases & Covers', 'Cases & Covers', 58, 'cases-covers', '2018-08-19 01:58:22', '2018-08-19 01:58:22'),
(82, 'Power Banks', 1, 2, 'Power Banks', 'Power Banks', 58, 'power-banks', '2018-08-19 01:58:57', '2018-08-19 01:58:57'),
(83, 'MicroSD Cards', 1, 3, 'MicroSD Cards', 'MicroSD Cards', 58, 'microsd-cards', '2018-08-19 01:59:25', '2018-08-19 01:59:25'),
(84, 'Head sets', 1, 4, 'Head-sets', 'Head sets', 58, 'head-sets', '2018-08-19 01:59:49', '2018-08-19 01:59:49'),
(85, 'Screen Protector', 1, 5, 'Screen Protector', 'Screen Protector', 58, 'screen-protector', '2018-08-19 02:00:15', '2018-08-19 02:00:15'),
(86, 'Selfie Sticks', 1, 6, 'Selfie Sticks', 'Selfie Sticks', 58, 'selfie-sticks', '2018-08-19 02:00:33', '2018-08-19 02:00:33'),
(88, 'Speakers', 1, 7, 'Speakers', 'Speakers', 58, 'mobile-speakers', '2018-08-19 02:01:16', '2018-08-19 02:01:16'),
(89, 'Smart witch', 1, 8, 'Smart witch', 'Smart witch', 58, 'smart-witch', '2018-08-19 02:01:31', '2018-08-19 02:01:31');

-- --------------------------------------------------------

--
-- Table structure for table `sub_attribute`
--

CREATE TABLE `sub_attribute` (
  `id` int(11) NOT NULL,
  `option_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `description` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fk_attribute_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_attribute`
--

INSERT INTO `sub_attribute` (`id`, `option_name`, `status`, `description`, `fk_attribute_id`, `created_at`, `updated_at`) VALUES
(1, 'Red', 1, 'Red', 3, '2018-07-28 05:40:27', '2018-07-27 23:40:27'),
(2, 'yellow', 1, 'Yellow', 3, '2018-07-28 05:50:12', '2018-07-27 23:50:12'),
(3, 'S', 1, 'small', 4, '2018-07-23 05:35:35', '2018-07-23 05:35:35'),
(4, 'M', 1, 'midium', 4, '2018-07-23 06:02:21', '2018-07-23 06:02:21'),
(6, 'blue', 1, 'Blue', 3, '2018-07-27 23:41:19', '2018-07-27 23:41:19'),
(7, 'White', 1, '#fff', 3, '2018-07-28 07:22:04', '2018-07-28 01:22:04'),
(8, 'L', 1, 'Large', 4, '2018-07-27 23:57:42', '2018-07-27 23:57:42'),
(9, 'XL', 1, 'Extra Large', 4, '2018-07-27 23:57:55', '2018-07-27 23:57:55');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(11) NOT NULL,
  `sub_category_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(2) NOT NULL,
  `serial_num` tinyint(2) NOT NULL,
  `sub_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fk_category_id` int(2) NOT NULL,
  `sub_link` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `sub_category_name`, `status`, `serial_num`, `sub_description`, `sub_keywords`, `fk_category_id`, `sub_link`, `updated_at`, `created_at`) VALUES
(5, 'T-Shirts', 1, 1, 'Exclosive all T-Shirts', 'Exclosive all T-Shirts', 5, 't-shirts', '2018-08-19 00:47:37', '2018-08-19 00:47:37'),
(6, 'Shirts', 1, 2, 'wonderful all Shirts', 'wonderful all Shirts', 5, 'shirts', '2018-08-19 00:48:15', '2018-08-19 00:48:15'),
(7, 'Pant & Trousers', 1, 3, 'Pant & Trousers', 'Pant & Trousers', 5, 'pant-trousers', '2018-08-19 00:49:35', '2018-08-19 00:49:35'),
(8, 'Jeans', 1, 4, 'Jeans', 'Jeans', 5, 'jeans', '2018-08-19 00:49:51', '2018-08-19 00:49:51'),
(9, 'Shorts', 1, 5, 'Shorts', 'Shorts', 5, 'shorts', '2018-08-19 00:50:07', '2018-08-19 00:50:07'),
(10, 'Punjabi & Pajama', 1, 6, 'Punjabi & Pajama', 'Punjabi & Pajama', 5, 'punjabi-pajama', '2018-08-19 00:50:27', '2018-08-19 00:50:27'),
(11, 'Sports wear', 1, 7, 'Sports wear', 'Sports wear', 5, 'sports-wear', '2018-08-19 00:50:58', '2018-08-19 00:50:58'),
(12, 'Accessories', 1, 8, 'Accessories', 'Accessories', 5, 'accessories', '2018-08-19 00:51:11', '2018-08-19 00:51:11'),
(13, 'Inner wear', 1, 9, 'Inner wear', 'Inner wear', 5, 'inner-wear', '2018-08-19 00:51:26', '2018-08-19 00:51:26'),
(14, 'Shoes & Sneakers', 1, 10, 'Shoes & Sneakers', 'Shoes & Sneakers', 5, 'shoes-sneakers', '2018-08-19 00:51:48', '2018-08-19 00:51:48'),
(15, 'Watches', 1, 11, 'Watches', 'Watches', 5, 'watches', '2018-08-19 00:52:01', '2018-08-19 00:52:01'),
(16, 'Wallets & Belts', 1, 12, 'Wallets & Belts', 'Wallets & Belts', 5, 'wallets-belts', '2018-08-19 00:52:20', '2018-08-19 00:52:20'),
(17, 'Beauty & Health', 1, 13, 'Beauty & Health', 'Beauty & Health', 5, 'beauty-health', '2018-08-19 00:52:42', '2018-08-19 00:52:42'),
(18, 'Winter wear', 1, 14, 'Winter wear', 'Winter wear', 5, 'winter-wear', '2018-08-19 00:54:03', '2018-08-19 00:54:03'),
(19, 'Clothing', 1, 1, 'Clothing', 'Clothing', 6, 'clothing', '2018-08-19 00:59:02', '2018-08-19 00:59:02'),
(20, 'Beauty & Health', 1, 2, 'Beauty & Health women', 'Beauty & Health women', 6, 'beauty-health-women', '2018-08-19 00:59:37', '2018-08-19 00:59:37'),
(21, 'Accessories', 1, 3, 'Accessories-women', 'Accessories women', 6, 'accessories-women', '2018-08-19 01:00:04', '2018-08-19 01:00:04'),
(22, 'Watches & Sunglasses', 1, 4, 'Watches & Sunglasses', 'Watches & Sunglasses', 6, 'watches-sunglasses', '2018-08-19 01:00:25', '2018-08-19 01:00:25'),
(23, 'Jewellery', 1, 5, 'Jewellery', 'Jewellery', 6, 'jewellery', '2018-08-19 01:00:40', '2018-08-19 01:00:40'),
(24, 'Shoes', 1, 6, 'Shoes-womens', 'Shoes-womens', 6, 'shoes-womens', '2018-08-19 01:00:59', '2018-08-19 01:00:59'),
(26, 'Baby Fashion', 1, 1, 'Baby Fashion', 'Baby Fashion', 7, 'baby-fashion', '2018-08-19 01:16:37', '2018-08-19 01:16:37'),
(27, 'Boys-Fashion', 1, 2, 'Boys Fashion', 'Boys Fashion', 7, 'boys-fashion', '2018-08-19 01:16:54', '2018-08-19 01:16:54'),
(28, 'Girls Fashion', 1, 3, 'Girls Fashion', 'Girls Fashion', 7, 'girls-fashion', '2018-08-19 01:17:12', '2018-08-19 01:17:12'),
(29, 'Toys & games', 1, 4, 'Toys & games', 'Toys & games', 7, 'toys-games', '2018-08-19 01:17:29', '2018-08-19 01:17:29'),
(30, 'Academic Books', 1, 1, 'Academic Books', 'Academic Books', 8, 'academic-books', '2018-08-19 01:23:44', '2018-08-19 01:23:44'),
(31, 'Novel & Literature', 1, 2, 'Novel & Literature', 'Novel & Literature', 8, 'novel-literature', '2018-08-19 01:24:02', '2018-08-19 01:24:02'),
(32, 'Islamic Books', 1, 3, 'Islamic Books', 'Islamic Books', 8, 'islamic-books', '2018-08-19 01:24:16', '2018-08-19 01:24:16'),
(33, 'Books on Music', 1, 4, 'Books on Music', 'Books on Music', 8, 'books-music', '2018-08-19 01:24:34', '2018-08-19 01:24:34'),
(34, 'Fiction', 1, 5, 'Fiction', 'Fiction', 8, 'fiction', '2018-08-19 01:25:01', '2018-08-19 01:25:01'),
(35, 'Stationary', 1, 6, 'Stationary', 'Stationary', 8, 'stationary', '2018-08-19 01:25:13', '2018-08-19 01:25:13'),
(36, 'Home Decor', 1, 1, 'Home Decor', 'Home Decor', 9, 'home-decor', '2018-08-19 01:49:59', '2018-08-19 01:49:59'),
(37, 'Kitchen & Dining', 1, 2, 'Kitchen & Dining', 'Kitchen & Dining', 9, 'kitchen-dining', '2018-08-19 01:50:16', '2018-08-19 01:50:16'),
(38, 'Home Furnishing', 1, 3, 'Home Furnishing', 'Home Furnishing', 9, 'home-furnishing', '2018-08-19 01:50:31', '2018-08-19 01:50:31'),
(39, 'Indoor Lighting', 1, 4, 'Indoor Lighting', 'Indoor Lighting', 9, 'indoor-lighting', '2018-08-19 01:50:44', '2018-08-19 01:50:44'),
(40, 'Home Storage', 1, 5, 'Home Storage', 'Home Storage', 9, 'home-storage', '2018-08-19 01:50:57', '2018-08-19 01:50:57'),
(41, 'Home Tools', 1, 6, 'Home Tools', 'Home Tools', 9, 'home-tools', '2018-08-19 01:51:13', '2018-08-19 01:51:13'),
(42, 'Furniture', 1, 7, 'Furniture', 'Furniture', 9, 'furniture', '2018-08-19 01:51:28', '2018-08-19 01:51:28'),
(43, 'Motor Bike', 1, 8, 'Motor Bike', 'Motor Bike', 9, 'motor-bike', '2018-08-19 01:51:43', '2018-08-19 01:51:43'),
(44, 'Car Accessories', 1, 9, 'Car Accessories', 'Car Accessories', 9, 'car-accessories', '2018-08-19 01:51:57', '2018-08-19 01:51:57'),
(45, 'Television', 1, 10, 'Television', 'Television', 9, 'television', '2018-08-19 01:52:07', '2018-08-19 01:52:07'),
(46, 'Wishing Machine', 1, 11, 'Wishing Machine', 'Wishing Machine', 9, 'wishing-machine', '2018-08-19 01:52:21', '2018-08-19 01:52:21'),
(47, 'Refrigerator', 1, 12, 'Refrigerator', 'Refrigerator', 9, 'refrigerator', '2018-08-19 01:52:36', '2018-08-19 01:52:36'),
(48, 'Air Conditioner', 1, 13, 'Air Conditioner', 'Air Conditioner', 9, 'air-conditioner', '2018-08-19 01:52:50', '2018-08-19 01:52:50'),
(49, 'Fens', 1, 14, 'Fens', 'Fens', 9, 'fens', '2018-08-19 01:53:00', '2018-08-19 01:53:00'),
(50, 'Lights', 1, 15, 'Lights', 'Lights', 9, 'lights', '2018-08-19 01:53:11', '2018-08-19 01:53:11'),
(51, 'Air Coolers', 1, 16, 'Air Coolers', 'Air Coolers', 9, 'air-coolers', '2018-08-19 01:53:27', '2018-08-19 01:53:27'),
(52, 'Camera', 1, 17, 'Camera', 'Camera', 9, 'camera', '2018-08-19 01:53:40', '2018-08-19 01:53:40'),
(53, 'Laptops', 1, 1, 'Laptops', 'Laptops', 10, 'laptops', '2018-08-19 01:54:09', '2018-08-19 01:54:09'),
(54, 'Desktop', 1, 2, 'Desktop', 'Desktop', 10, 'desktop', '2018-08-19 01:54:20', '2018-08-19 01:54:20'),
(55, 'Mobiles', 1, 3, 'Mobiles', 'Mobiles', 10, 'mobiles', '2018-08-19 01:54:34', '2018-08-19 01:54:34'),
(56, 'Tablets', 1, 4, 'Tablets', 'Tablets', 10, 'tablets', '2018-08-19 01:54:47', '2018-08-19 01:54:47'),
(57, 'Computer Accessories', 1, 5, 'Computer Accessories', 'Computer Accessories', 10, 'computer-accessories', '2018-08-19 01:55:01', '2018-08-19 01:55:01'),
(58, 'Mobile Accessories', 1, 6, 'Mobile Accessories', 'Mobile Accessories', 10, 'mobile-accessories', '2018-08-19 01:55:24', '2018-08-19 01:55:24');

-- --------------------------------------------------------

--
-- Table structure for table `sub_menu`
--

CREATE TABLE `sub_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(4) NOT NULL,
  `serial_num` int(4) NOT NULL,
  `fk_menu_id` int(3) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_sub_menu`
--

CREATE TABLE `sub_sub_menu` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(2) NOT NULL,
  `serial_num` int(4) NOT NULL,
  `fk_sub_menu_id` int(2) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) DEFAULT '1',
  `type` tinyint(4) DEFAULT '3' COMMENT '1=superadmin,2=admin,3=user',
  `photo` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` int(20) NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `type`, `photo`, `phone_number`, `address`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$iWmpDAXGI/ZRclOliSPF2.CFTV.cePdz7aHPxwzZWzAAtjSpp9hSW', NULL, 1, NULL, 1845791926, 'Dhaka', 'GpFZplXtHIuD5roRNQGCvrIdYasgxzsv9og6wTX1CtYvP6gZr83Gp738JUe7', '2018-07-16 23:49:28', '2018-08-13 01:18:55'),
(2, 'Myadmin', 'admin1@gmail.com', '$2y$10$er56PnwB1m1DOUedVKSiVecA3MWXBiPG281v4NhE5nVc/aCB5ojHS', NULL, 2, NULL, 0, 'Comilla', 'nLsLrJfQQgo7Fs4IkV3I81bcw4TIHlkljz91I3yvXUseHfazvVW7WbwT2Lmh', '2018-07-17 00:38:42', '2018-07-30 00:20:36'),
(3, 'kamrul', 'mskamrul1@gmail.com', '$2y$10$umST2Zn3aU1ZmSNMfE7nIuWML.4mxlSXDSvUq4kWlI3jHIe0EkcbW', NULL, 3, NULL, 1845791926, 'Dhaka', 'STVFOXXjtlfsmWpCbpUscvbJE7UmjEocqcdv06yGppBLnymrQfAZ0gEyeA4M', '2018-07-17 04:11:44', '2018-07-17 04:11:44'),
(6, 'bappy', 'admin3@gmail.com', '$2y$10$6WG5aZP6Wnfh7W.CIq46w.PZ5s9kxI7oBgf/9UyQ1vGTF/hbXOwKy', 1, 3, NULL, 1845784554, 'Dhaka', NULL, '2018-07-29 23:37:35', '2018-07-29 23:37:35'),
(7, 'new Shoop', 'admin4@gmail.com', '$2y$10$TjiMzCG6bQzrK9WMjjtXc.XPkGi22dXKPr9l4cDOixXuRiTRaFDei', 1, 2, NULL, 1845784556, 'Dhaka', 'Za2IEmBQcG2EThHd9LJrjjiulUhHDDZUHZkaa5G6GP7V5QK3TUxYXTihP2vq', '2018-07-30 00:03:06', '2018-07-30 00:15:02');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(11) NOT NULL,
  `fk_item_id` int(4) NOT NULL,
  `fk_user_id` int(4) NOT NULL,
  `status` int(4) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`id`, `fk_item_id`, `fk_user_id`, `status`, `updated_at`, `created_at`) VALUES
(1, 24, 8, 0, '2018-08-13 04:52:01', '2018-08-13 04:52:01'),
(2, 25, 8, 0, '2018-08-13 05:08:50', '2018-08-13 05:08:50'),
(3, 22, 8, 0, '2018-08-13 05:09:58', '2018-08-13 05:09:58'),
(4, 23, 8, 0, '2018-08-13 05:10:06', '2018-08-13 05:10:06'),
(16, 1, 1, 0, '2018-08-29 06:33:21', '2018-08-29 06:33:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_company`
--
ALTER TABLE `about_company`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ads_manager`
--
ALTER TABLE `ads_manager`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attribute`
--
ALTER TABLE `attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `link` (`link`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `link` (`link`),
  ADD KEY `FK_items_categories` (`fk_category_id`),
  ADD KEY `FK_items_sub_categories` (`fk_sub_category_id`),
  ADD KEY `FK_items_subsub_categories` (`fk_subsubcategory_id`),
  ADD KEY `FK_items_brand` (`fk_brand_id`);

--
-- Indexes for table `item_attribute`
--
ALTER TABLE `item_attribute`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_item_attribute_items` (`fk_item_id`),
  ADD KEY `FK_item_attribute_attribute` (`attribute_id`),
  ADD KEY `FK_item_attribute_sub_attribute` (`option_id`);

--
-- Indexes for table `item_photo`
--
ALTER TABLE `item_photo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_item_photo_items` (`fk_item_id`);

--
-- Indexes for table `join_us`
--
ALTER TABLE `join_us`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_id` (`invoice_id`),
  ADD KEY `FK_order_user` (`fk_user_id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_order_item_order` (`fk_order_id`),
  ADD KEY `FK_order_item_items` (`fk_item_id`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_methode`
--
ALTER TABLE `payment_methode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_role_user_roles` (`role_id`),
  ADD KEY `FK_role_user_users` (`user_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `services_name_unique` (`name`),
  ADD UNIQUE KEY `services_link_unique` (`link`),
  ADD KEY `services_created_by_foreign` (`created_by`);

--
-- Indexes for table `shipment`
--
ALTER TABLE `shipment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `social_icons`
--
ALTER TABLE `social_icons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subsub_categories`
--
ALTER TABLE `subsub_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sub_link` (`sub_link`),
  ADD KEY `fk_subsubcategory_id` (`fk_subsubcategory_id`);

--
-- Indexes for table `sub_attribute`
--
ALTER TABLE `sub_attribute`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_sub_attribute_attribute` (`fk_attribute_id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `sub_link` (`sub_link`);

--
-- Indexes for table `sub_menu`
--
ALTER TABLE `sub_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_menu_menu_id` (`fk_menu_id`);

--
-- Indexes for table `sub_sub_menu`
--
ALTER TABLE `sub_sub_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_sub_menu_submenu_id` (`fk_sub_menu_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_company`
--
ALTER TABLE `about_company`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ads_manager`
--
ALTER TABLE `ads_manager`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `attribute`
--
ALTER TABLE `attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `discount`
--
ALTER TABLE `discount`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `item_attribute`
--
ALTER TABLE `item_attribute`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `item_photo`
--
ALTER TABLE `item_photo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `join_us`
--
ALTER TABLE `join_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payment_methode`
--
ALTER TABLE `payment_methode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shipment`
--
ALTER TABLE `shipment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `social_icons`
--
ALTER TABLE `social_icons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `subsub_categories`
--
ALTER TABLE `subsub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `sub_attribute`
--
ALTER TABLE `sub_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `sub_menu`
--
ALTER TABLE `sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_sub_menu`
--
ALTER TABLE `sub_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `FK_items_brand` FOREIGN KEY (`fk_brand_id`) REFERENCES `brand` (`id`),
  ADD CONSTRAINT `FK_items_categories` FOREIGN KEY (`fk_category_id`) REFERENCES `categories` (`id`),
  ADD CONSTRAINT `FK_items_sub_categories` FOREIGN KEY (`fk_sub_category_id`) REFERENCES `sub_categories` (`id`),
  ADD CONSTRAINT `FK_items_subsub_categories` FOREIGN KEY (`fk_subsubcategory_id`) REFERENCES `subsub_categories` (`id`);

--
-- Constraints for table `item_attribute`
--
ALTER TABLE `item_attribute`
  ADD CONSTRAINT `FK_item_attribute_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `attribute` (`id`),
  ADD CONSTRAINT `FK_item_attribute_items` FOREIGN KEY (`fk_item_id`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `FK_item_attribute_sub_attribute` FOREIGN KEY (`option_id`) REFERENCES `sub_attribute` (`id`);

--
-- Constraints for table `item_photo`
--
ALTER TABLE `item_photo`
  ADD CONSTRAINT `FK_item_photo_items` FOREIGN KEY (`fk_item_id`) REFERENCES `items` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FK_orders_users` FOREIGN KEY (`fk_user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `order_item`
--
ALTER TABLE `order_item`
  ADD CONSTRAINT `FK_order_item_items` FOREIGN KEY (`fk_item_id`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `FK_order_item_orders` FOREIGN KEY (`fk_order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `subsub_categories`
--
ALTER TABLE `subsub_categories`
  ADD CONSTRAINT `subsub_categories_ibfk_1` FOREIGN KEY (`fk_subsubcategory_id`) REFERENCES `sub_categories` (`id`);

--
-- Constraints for table `sub_attribute`
--
ALTER TABLE `sub_attribute`
  ADD CONSTRAINT `FK_sub_attribute_attribute` FOREIGN KEY (`fk_attribute_id`) REFERENCES `attribute` (`id`);

--
-- Constraints for table `sub_menu`
--
ALTER TABLE `sub_menu`
  ADD CONSTRAINT `sub_menu_menu_id` FOREIGN KEY (`fk_menu_id`) REFERENCES `menu` (`id`);

--
-- Constraints for table `sub_sub_menu`
--
ALTER TABLE `sub_sub_menu`
  ADD CONSTRAINT `sub_sub_menu_submenu_id` FOREIGN KEY (`fk_sub_menu_id`) REFERENCES `sub_menu` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
