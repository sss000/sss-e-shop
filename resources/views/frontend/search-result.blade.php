@extends('frontend._partials.app')
@section('title')
   Search  Details

        @endsection

@section('content')
<div class='container'>
  <div class="col-12">
         <div class="row">
                <ol class="breadcrumb" style="background:  #fff;">
                      <li class="breadcrumb-item"><a href="{{URL::to('/')}}">Home</a></li>
                      <li class="breadcrumb-item active">Search</li>
                  </ol>
          </div>
   </div>
  <!-- /.breadcrumb -->
  <div class="body-content outer-top-xs">
    
      <div class='row'>
        <div class='col-md-3'> 
          <!-- ================================== TOP NAVIGATION ================================== -->
                        <nav id="sidebar">
                            <div class="sidebar-header">
                                <h3>Categories</h3>
                            </div>

                            <ul class="list-unstyled components">
                                

                                @foreach($all_category as $cat)
                                @php  
                                    $subcats = App\SubCategory::where('status',1)->where('fk_category_id',$cat->id)->count();
                                @endphp 

                                <li class="categories"><i class="fa fa-chevron-right" aria-hidden="true" style="    color: #000;font-size: 12px;"></i>
                                    <a href="{{URL::to('category-product/'.$cat->id)}}"> {{$cat->category_name}}</a><span><a href="#homeSubmenu{{$cat->id}}" @if($subcats>0) data-toggle="collapse" aria-expanded="false" class="dropdown-toggle" @endif > </a></span>

                                    <ul class="collapse list-unstyled" id="homeSubmenu{{$cat->id}}">

                                        @foreach($cat->subcategory as $subcat)

                                             @php
                                                   $count = App\SubSubCategory::where('fk_subsubcategory_id','=',$subcat->id)->count();
                                                                        
                                                 @endphp
                                        <li>

                                            <i class="fa fa-chevron-right" aria-hidden="true" style="    color: #ab9292;font-size: 11px;"></i>

                                            <a href="{{URL::to('sub-category-product/'.$subcat->id)}}" style="background: transparent;font-weight: 600;"> {{$subcat->sub_category_name}}</a>
                                            <span><a href="#homeSubSubmenu{{$subcat->id}}"  @if($count>0) data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"  @endif ></a></span>
                                            <ul class="collapse list-unstyled" id="homeSubSubmenu{{$subcat->id}}">
                                                    
                                             @php
                                                   $subsubcategory = App\SubSubCategory::where('fk_subsubcategory_id','=',$subcat->id)->get();
                                                                        
                                                 @endphp
                                                
                                                                     
                                                     @foreach($subsubcategory as $ssC)

                                                    <li><a href="{{URL::to('sub-sub-category-product/'.$ssC->id)}}"   style="background: #fff;">&#155; &nbsp; {{$ssC->subsub_category_name}}</a></li>

                                                    @endforeach

                                                </ul>
                                        </li>

                                        @endforeach
                                     
                                    </ul>
                                </li>
                             @endforeach
                             
                            </ul>

               
                        </nav> 
                                 

          <!-- /.side-menu --> 
          <!-- /.sidebar-module-container --> 
        </div>
        <!-- /.sidebar -->
        



            <div class="col-md-9">
                    <div class="shop_grid_product_area">
 
                             <div class="col-12 col-sm-12 col-lg-12">
                                  @foreach($allData as $data)

                                   {!! Form::open(['url'=>'add-to-cart','method'=>'POST']) !!}
                                <div class="single_product_area mb-30 col-lg-4" style="float: left">
                                    <div class="single_arrivals_slide">
                                        <div class="product_image">
                                            <!-- Product Image -->


                                         @if(isset($data->itemImage[0]) and $data->itemImage[0]->photo!=null)
                                            <img class="normal_img" src="{{asset('images/items/small/'.$data->itemImage[0]->photo)}}" alt="">
                                               @endif
                                            @if(isset($data->itemImage[1]) and $data->itemImage[1]->photo!=null)
                                                   <a href="{{url('/product-details/'.$data->id)}}"> <img class="hover_img" src="{{asset('images/items/small/'.$data->itemImage[1]->photo)}}" alt="">
                                                </a>
                         
                                        @endif

                                            <!-- Product Badge -->
                                            <div class="product_badge">
                                                <span class="badge-new">New</span>
                                            </div>
                                            <!-- Wishlist -->
                                            <div class="product_wishlist">
                                                <a href="#" title="Wishlist"><i class="ti-heart"></i></a>
                                            </div>
                                            <!-- Compare -->
                                          
                                            <!-- Add to cart -->
                                             <div class="product_add_to_cart">

                                               <input type="hidden" value="{{$data->id}}" name="id">

                                                    <input type="hidden" value="1" name="quantity">


                                                <button class="" type="submit" style="border-bottom: none; border-width: 0px;"> <i class="ti-shopping-cart" aria-hidden="true" ></i> Add to Cart </button>


                                            </div>
                                            <!-- Quick View -->
                                            <div class="product_quick_view">
                                                <a href="#" data-toggle="modal" data-target="#quickview{{ $data->id }}"><i class="ti-eye" aria-hidden="true"></i> Quick View</a>
                                            </div>
                                        </div>
                                        <!-- Product Description -->
                                        <div class="product_description">
                                           
                                            <h5><a href="{{url('/product-details/'.$data->id)}}">{{$data->title}}</a></h5>

                   
                     <h6>BDT:{{$data->discount}} <span>BDT:{{$data->price}}</span></h6>
                </div>



                       
                                    </div>

                                </div>

             {!! Form::close() !!}


        <div class="modal fade" id="quickview{{ $data->id }}" tabindex="-1" role="dialog" aria-labelledby="quickview" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close btn" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <div class="quickview_body">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-lg-5">
                                    <div class="quickview_pro_img">

                                @if(isset($data->itemImage[0]) and $data->itemImage[0]->photo!=null)

                                        <img class="first_img" src="{{asset('images/items/small/'.$data->itemImage[0]->photo)}}" alt="">

                                         @endif

                                             @if(isset($data->itemImage[1]) and $data->itemImage[1]->photo!=null)

                                        <img class="hover_img" src="{{asset('images/items/small/'.$data->itemImage[1]->photo)}}" alt="">

                                            @endif
                                        <!-- Product Badge -->
                                        <div class="product_badge">
                                            <span class="badge-new">New</span>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 col-lg-7">
                                    <div class="quickview_pro_des">
                                        <h4 class="title">{{$data->title}}</h4>
                                        <div class="top_seller_product_rating mb-15">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <h5 class="price">&#2547;{{$data->discount}}&nbsp;<span> &#2547;{{$data->price}}</span></h5>
                                        <p>{{$data->meta_description}}</p>
                                        <a href="{{url('/product-details/'.$data->id)}}">View Full Product Details</a>
                                    </div>
                                    <!-- Add to Cart Form -->
                                  {!! Form::open(['url'=>'add-to-cart','method'=>'POST','class'=>'cart']) !!}



                                        
                                    

                                            <input type="hidden" value="{{$data->id}}" name="id">

                                            <input type="hidden" value="1" name="quantity">


                                                <button class="cart-submit" type="submit"> <i class="ti-shopping-cart" aria-hidden="true" ></i> Add to Cart </button>


                                   
                                        <!-- Wishlist -->
                                        <div class="modal_pro_wishlist">
                                            <a href="wishlist.html" target="_blank"><i class="ti-heart"></i></a>
                                        </div>
                                        <!-- Compare -->
                                        
                                   {!! Form::close() !!}

                                    <div class="share_wf mt-30">
                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


     @endforeach
</div>

                    

                        

                        

                    

                    </div>

              

                </div>
        <!-- /.col --> 
      </div>
      <!-- /.row --> 
      <!-- ============================================== BRANDS CAROUSEL ============================================== -->
      
      <!-- /.logo-slider --> 
      <!-- ============================================== BRANDS CAROUSEL : END ============================================== --> </div>
    <!-- /.container --> 
  </div>
@endsection