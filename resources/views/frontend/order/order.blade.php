@extends('frontend._partials.app')
@section('title')
   Order form

        @endsection

@section('content')
<div class="container">
<div class="row">
    <div class="shortcodes_content">
        <div class="col-md-12">
            <div class="row">
            <div id="content" class="col-md-12">
                
            <div class="col-md-12">
                
                <h4> Order Details </h4> </br>
                <h5> <b> Order ID:  </b> {{$order->id}} </h5></br>
                <h5> <b> Order Date:  </b> {{date("d-m-Y", strtotime($order->created_at))}}  </h5>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="5%">SL</th>
                            <th width="5%">Photo</th>
                            <th>Item</th>
                            <th>Attribute</th>
                            <th width="10%" class="text-right">Price</th>
                            <th class="text-center">Qty</th>
                            <th width="10%" class="text-right">Sub Total</th>
            
                        </tr>
                    </thead>
                    <tbody>
                              <? $i=0; ?>
                             @foreach($order->hasproduct as $data)
                              <? $i++; ?> 
                         <tr>
                                <td class="text-center">{{$i++}}</td>
                                @php
                                    $select_image = App\ItemPhoto::where('fk_item_id','=',$data->fk_item_id)->first();
                                        
                                @endphp
                                <td><img src="{{asset('images/items/small/'.$select_image->photo) }}"></td>

                                <td class="text-left"><a href=""> {{$data->hasItem->title}} </a><br>  </td>

                                <td>
                                    @foreach(json_decode($data->attributes,true) as $attr =>$val)
                                        <p><b>{{$attr}} : </b> {{$val}} </p>
                                    @endforeach
                                     </td>
                                <td class="text-right">{{$data->price}}</td>
                                <td class="text-center">{{$data->quantity}}</td>
                
                                <td class="text-right"> {{$data->amount}}</td>
                            </tr>
                        @endforeach
                    
                    </tbody>


                </table>
                       <div class="col-md-12">  
         <div class="row">           
        <div class="shipping-item col-md-6">
            
         <h5> <b> Billing Address:  </b> {{$order->billing_address}} </h5>
        <h5> <b> Shipping Address:  </b> {{$order->shipping_address}} </h5>
         <h5> <b> Shipment :  </b>  {{$order->shipping_amount}}</h5>
        <h5> <b> Payment Method :  </b> {{$order->hasMethode->name}} </h5>
        </div>
   
    <div class="shipping_amount col-md-6">
        <table class="table table-bordered">
        <tbody>
        <tr>
            <td class="text-right"><strong>Sub-Total</strong></td>
            <td class="text-right">{{$order->total}}</td>
        </tr>
        <tr>
            <td class="text-right"><strong>Shipping Amount</strong></td>
            <td class="text-right">{{$order->shipping_amount}}</td>
        </tr>

        <tr>
            <td class="text-right"><strong>Total</strong></td>
            <td class="text-right">BDT.&nbsp;{{$order->total_amount}}</td>
        </tr>
        </tbody>
    </table>
    </div>  
</div>
</div>
            </div>
            </div>
          </div>


        </div>

      </div>



</div>
</div>
</div>



             <!--Middle Part End-->
        
         @endsection