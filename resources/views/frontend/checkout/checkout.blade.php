@extends('frontend._partials.app')
@section('title')
    Cheackout

        @endsection

@section('content')


<div id="accordion">

  {!! Form::open(['url'=>'order-form','method'=>'POST'])  !!}

  <div class="card">
    <div class="card-header" id="headingOne">
      <h5 class="mb-0">
        <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fa fa-address-book-o"></i>
         <b>Address</b>
        </button>
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
           <div class="row">
         <div class="col-md-6 mb-3">
             <label for="first_name">Billing Address</label>
                    <div>
                    <span><b>Name:</b> &nbsp<span style="font-size: 18px;">{{$data->name}}</span></span> 
                  </br>
                    <span><b>E-mail:</b>&nbsp{{$data->email}} </span> </br>
                    <span><b>Mobile No:</b>&nbsp{{$data->phone_number}} </span> </br>
                    <span><b>Address:</b> &nbsp{{$data->address}}</span> 
                  <input type="hidden" name="fk_user_id" value="{{Auth::user()->id}}">
                    </div>
                </div>


               <div class="col-md-6 mb-3">
                <label for="last_name">Shipping Address</label>
                  <input type="text"  class="form-control"  name="shipping_address" placeholder="Address" value="" required>
               </div>
        </div>



      </div>
    </div>
  </div>


  <div class="card">
    <div class="card-header" id="headingTwo">
      <h5 class="mb-0">
        <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="fa fa-address-book-o"></i>
         <b>Shipping Method</b>
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
     <div class="row">
         <div class="col-md-6 mb-3">
             <label for="first_name"><b>Shipment Option</b></label>
                    <div>

                      <?
                        $shipments =DB::table('shipment')->orderBy('id','ASC')->get();

                      ?>
                        @foreach( $shipments as $shipment)
                      <label> <input class="shipment" type="radio" required  value="{{$shipment->tk}}" id="delivery_charge" onclick="deliveryCharge(this.value)" name="shipping_amount"> {{$shipment->details}}</label>

                   @endforeach

                       <div class="col-md-12">
                                <table class="table-responsive table  pull-right">
                                 

                                      <tr>
                                        <th> Total :</th>
                                        
                                        <td>{{Cart::total()}}
                                          <input name="total" value="{{Cart::total()}}" type="hidden" id="cartTotalAmount">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th class="text-right"> Shipping:</th>

                                        <td class="text-right">

                                          <span id="shipping-amount">
                                          

                                        

                                           </span>

                                            <input type="hidden" name="shipping_amount" value="" id="shipping-amount2">
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> <b> Total : </b></th>
                                        <td>
                                            <span id="total-amount"></span>

                                            <input type="hidden" id="total-amount2" name="total_amount" value="" >
                                        </td>
                                    </tr>
                              

                              </table>
                            </div>

                  
                    </div>
                </div>


                   <div class="col-md-6 mb-3">
                <label for="last_name"><b>Payment Methode</b> </label>
                      <div>

                     {{Form::select('payment_method_id',$paymentmethode,'',['class'=>'form-control','required','placeholder'=>'Select methode','onchange'=>'loadpayment(this.value)'])}}
                  </div>
                  <div id="loadpayment">
                    <!-- Load here -->
                  </div>

            <button class="btn btn-success" style="margin-top: 20px;"> Confirm </button>

          </div>
                   
        </div>
      </div>
    </div>
  </div>
  <div class="card">
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class="fa fa-shopping-cart"></i>
         <b> Show Cart</b>
        </button>
      </h5>
    </div>
    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
          
    <!-- ***** Breadcumb Area End ***** -->

    <!-- ***** Cart Area Start ***** -->
    <div class="cart_area clearfix">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="cart-table clearfix">
                         @php

                                  $total = floatval(str_replace(',','', Cart::total()));
                                    $cart = [
                                        'totalAmount'=>$total,
                                        'total'=>Cart::total(),
                                        'tax'=>Cart::tax(),
                                        'subtotal'=>Cart::subtotal(),
                                        'count'=>Cart::count(),
                                        'content'=>Cart::content(),
                                    ];

                                    @endphp





                        @if(count($cart)>0)
                        
                    <form action="#">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th><i class="fa fa-trash-o text-danger" aria-hidden="true"></i></th>
                                    <th>Image</th>
                                    <th>Product</th>
                                    <th>Price</th>
                                    <th width="5%">Qty</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($cart['content'] as $rowKey => $product)
                                            <?
                                            $item=$product->options;

                               
                             $link= $item->link;
                             $photo= $item->photo;
                                            
                                ?>
                                <tr>
                                    <td class="action"><a class="btn btn-danger btn-xs" href='{{url("cart-remove-item/$product->rowId")}}'>
                                        <i class="fa fa-times" aria-hidden="true"></i>

                                    </a></td>

                                    

                                    <td class="cart_product_img">

                                        <a href="#">
                                            @if($photo!=null)
                                            <img src='{{asset('images/items/small/'.$photo)}}' alt="Product" style="height: 40px; width: 40px">
                                        @else
                                         <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image cart_img" alt="{{ $product->title }}" src='{{asset("public/img/item/default.png")}}' />

                                      @endif

                                        </a>
                                    </td>

                                     


                                    <td class="cart_product_desc">

                                        <h5>{{$product->name}}</h5>
                                        @foreach($item->attr_details as $attr => $val )
                                            <p> <b> {{$attr}} : </b> {{$val}} </p>
                                        @endforeach

                                    </td>
                                    <td class="price"><span>&#2547;&nbsp{{$product->price}}</span></td>
                                    <td class="qty">
                                        <div class="quantity">

                                           <a class="decrease" href='{{url("decrement/$product->rowId")}}'><i class="fa fa-minus-square-o"></i></a>

                                       
                                           <span style="width: 50px">{{$product->qty}}</span>

                                            <a class="increment" href='{{url("increment/$product->rowId")}}'><i class="fa fa-plus-square-o"></i></a>
                                         
                                        </div>
                                    </td>
                                    <td class="total_price"><span>{{$product->subtotal}}</span></td>
                                </tr>
                          @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>Total (tax incl.)</td>
                                    <td>&#2547;&nbsp{{Cart::tax()}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Total</strong></td>
                                    <td><strong>BDT.&nbsp;{{Cart::total()}}</strong></td>
                                </tr>
                            </tfoot>
                        </table>


                        </form>
                    
                                
                    </div>

                    <div class="cart- d-flex mt-30">
                        <div class="back-to-shop w-50">
                          
                           <a href="{{URL::to('/')}}"> <button type="button" class="btn btn-outline-secondary">Back to Shopping</button></a>
                            
                        </div>
                        <div class="update-checkout w-50 text-right">

                           <a href="{{url('clear-cart')}}" style="background-color: #fff;"><button  type="button" class="btn btn-outline-danger">Clear Cart</button></a>

                         
                           
                          
                        </div>
                    </div>

                            @else
                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                                                <h2>No Items in Cart!</h2>
                                            </div>
                                        </div>
                                    @endif


                </div>
            </div>
        </div>
    </div>
                   

      </div>
    </div>
  </div>

  {!! Form::close() !!}
</div>



 @endsection


 @section('script')

    <script type="text/javascript">

        function loadpayment(id){
            $('#loadpayment').load('{{URL::To("load-payment-methode")}}/'+id);
        }



   function deliveryCharge(tk){
            $('#shipping-amount').html(tk);
            $('#shipping-amount2').val(tk);
            var total = '<? echo floatval(str_replace(',','', Cart::total())); ?>';
            var final = parseFloat(tk)+parseFloat(total);
            $('#total-amount').html(final);
            $('#total-amount2').val(final);

        }



       
      
    </script>  

 @endsection