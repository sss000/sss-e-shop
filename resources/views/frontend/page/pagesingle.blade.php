@extends('frontend._partials.app')
@section('title')
   

   {{$page->title}}

        @endsection

@section('content')

   <div class="breadcumb_area bg-img background-overlay-white" style="background-image: url({{asset('public/frontend/img/bg-img/breadcumb.jpg')}});">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active">{{$page->name}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>



    <section class="about_us_area section_padding_100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12">
                    <div class="about_us_content">
                        <h5>{{$page->title}}</h5>
                        <p><? echo $page->description ?></p>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>







    @endsection