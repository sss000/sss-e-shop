@extends('frontend._partials.app')
	@section('title')
	
			{{$info->company_name}}
      
       
@endsection

@section('content')


    <!-- ***** Welcome Slides Area Start ***** -->
    <section class="welcome_area">
        <div class="welcome_slides">
            <!-- Single Slide Start --> 
           
            <!-- Single Slide Start -->
              @foreach($sliders as $slider)

            <div class="single_slide height-700 bg-img" style="background-image: url({{asset($slider->image)}});">
                <div class="container h-100">
      
                    <div class="row h-100 align-items-center">
                         <div class="col-12 col-md-8">
                            <div class="welcome_slide_text">
                                <p data-animation="fadeInUp" data-delay="0">{{$slider->top_caption}}</p>
                                <h2 data-animation="fadeInUp" data-delay="300ms">{{$slider->middle_caption}}</h2>
                                 <h4 data-animation="fadeInLeftBig" data-delay="600ms">{{$slider->buttom_caption}}</h4>

                                 

                           



                                    <button class="btn buy-now" type="submit" data-animation="fadeInUp" data-delay="600ms"> Show Now </button>

                                  

                            </div>
                        </div>
                   
                    </div>
 
                </div>
            </div>

              @endforeach

            <!-- Single Slide Start -->


              
      
        </div>
    </section>
    <!-- ***** Welcome Slides Area End ***** -->

    

      
    <!-- ***** New Arrivals Area Start ***** -->
    <section class="new_arrivals_area "  style="margin-top: 50px;margin-bottom: 50px">

        @foreach($categories as $cat)
        <div class="container">
            <div class="row section_heading">
                <div class="col-8">
                    <div class="section_heading ">
                        <h5>{{$cat->category_name}}</h5>
                    </div>
                </div> 
                <div class="col-4">
                    <ul class="subcat">
                          @foreach($cat->subcategory as $subcat)

                        <li><a href="#"> {{$subcat->sub_category_name}}</a></li>

                        @endforeach
                    </ul>
                </div> 
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="new_arrivals_slides">


                        @foreach($cat->items as $item)
                        {!! Form::open(['url'=>'add-to-cart','method'=>'POST']) !!}
                        <!-- Singel Arrivals Slide Start -->
                        <div class="single_arrivals_slide">
                            <div class="product_image">
                                <!-- Product Image -->
                                 @if(isset($item->itemImage[0]) and $item->itemImage[0]->photo!=null)
                                <img class="normal_img" src="{{asset('images/items/small/'.$item->itemImage[0]->photo)}}" alt="">
                                   @endif
                                @if(isset($item->itemImage[1]) and $item->itemImage[1]->photo!=null)
                               <a href="{{url('/product-details/'.$item->id)}}"> <img class="hover_img" src="{{asset('images/items/small/'.$item->itemImage[1]->photo)}}" alt="">
                            </a>
                         
                                        @endif
                                <!-- Product Badge -->
                                <div class="product_badge">
                                    <span class="badge-new">New</span>
                                </div>
                                <!-- Wishlist -->
                                <div class="product_wishlist">
                                    <a href="wishlist.html" target="_blank"><i class="ti-heart"></i></a>
                                </div>
                                <!-- Compare -->
                                <div class="product_compare">
                                    <a href="compare.html" target="_blank"><i class="ti-stats-up"></i></a>
                                </div>
                                <!-- Add to cart -->
                            <div class="product_add_to_cart">

                                <input type="hidden" value="{{$item->id}}" name="id">

                                <input type="hidden" value="1" name="quantity">


                                    <button class="" type="submit" style="border-bottom: none; border-width: 0px;"> <i class="ti-shopping-cart" aria-hidden="true" ></i> Add to Cart </button>


                                </div>
                                <!-- Quick View -->
                                <div class="product_quick_view">
                                    <a href="#" data-toggle="modal" data-target="#quickview{{ $item->id }}"><i class="ti-eye" aria-hidden="true"></i> Quick View</a>
                                </div>
                            </div>
                            <!-- Product Description -->
                            <div class="product_description">
                               
                                <h5 class="brand_name">
                                    <a href="{{url('/product-details/'.$item->id)}}">{{$item->title}}</a></h5>

                                <h6>&#2547;&nbsp;{{$item->discount}}&nbsp;&nbsp;<span>&#2547;{{$item->price}}</span></h6>
                                
                                <div class="row">
                                     <div class="col-md-12">
                                        <? $attribute = App\ItemAttribute::groupBy('attribute_id')->get(); ?>

                                        @foreach($attribute as $attr)
                                           
                                            <div class="col-md-12">
                                            @foreach($item->itemAttribute as $option)
                                          
                                            @if($attr->attribute_id==$option->attribute_id)
                                            <div class="radio_label">

                                            <input required="" type="radio" value="{{$option->id}}" name="attribute[{{$item->id}}][[{{$option->attribute_id}}]]">

                                            <label style="background:{{($attr->attribute->type==1)?$option->optionAttribute->description:''}} ;width: 20px;height: 20px;" for="radio-164" title="  {{$option->optionAttribute->description}}">  @if($attr->attribute->type!=1) {{$option->optionAttribute->option_name}} @endif</label>
                                           
                                          </div>
                                          
                                          @endif
                                          @endforeach
                                          </div>

                                      @endforeach
                                       
                                    </div>

                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}


                        

              @endforeach

         

       

                    </div>
                </div>
            </div>
        </div>
        @foreach($cat->items as $value)
        <div class="modal fade" id="quickview{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="quickview" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <button type="button" class="close btn" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="modal-body">
                        <div class="quickview_body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-12 col-lg-5">
                                        <div class="quickview_pro_img">
                                        
                                         @if(isset($value->itemImage[0]) and $value->itemImage[0]->photo!=null)

                                            <img class="first_img" src="{{asset('images/items/small/'.$item->itemImage[0]->photo)}}" alt="">
                                            
                                            @endif

                                             @if(isset($value->itemImage[1]) and $value->itemImage[1]->photo!=null)

                                            <img class="hover_img" src="{{asset('images/items/small/'.$item->itemImage[1]->photo)}}" alt="">
                                             @endif

                                            <!-- Product Badge -->
                                            <div class="product_badge">
                                                <span class="badge-new">New</span>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-7">
                                        <div class="quickview_pro_des">
                                            <h4 class="title">{{ $value->title }}</h4>
                                            <div class="top_seller_product_rating mb-15">
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                                <i class="fa fa-star" aria-hidden="true"></i>
                                            </div>
                                            <h5 class="price">&#2547;{{$value->price}} <span>{{$value->discount}}</span></h5>
                                            <p>{{$value->meta_description}}</p>
                                            <a href="{{url('/product-details/'.$value->id)}}">View Full Product Details</a>
                                        </div>
                                        <!-- Add to Cart Form -->
                                              {!! Form::open(['url'=>'add-to-cart','method'=>'POST','class'=>'cart']) !!}



                                <div class="col-md-6">
                                        <? $attribute = App\ItemAttribute::groupBy('attribute_id')->get(); ?>

                                        @foreach($attribute as $attr)
                                           
                                            <div class="col-md-12">
                                            @foreach($value->itemAttribute as $option)
                                          
                                            @if($attr->attribute_id==$option->attribute_id)
                                            <div class="radio_label">

                                            <input required="" type="radio" value="{{$option->id}}" name="attribute[{{$value->id}}][[{{$option->attribute_id}}]]">

                                            <label style="background:{{($attr->attribute->type==1)?$option->optionAttribute->description:''}} ;width: 20px;height: 20px;" for="radio-164" title="  {{$option->optionAttribute->description}}">  @if($attr->attribute->type!=1) {{$option->optionAttribute->option_name}} @endif</label>
                                           
                                          </div>
                                          
                                          @endif
                                          @endforeach
                                          </div>

                                      @endforeach
                                       
                                    </div>



                                        <div>
                                    

                                            <input type="hidden" value="{{$value->id}}" name="id">

                                            <input type="hidden" value="1" name="quantity">


                                                <button class="cart-submit" type="submit"> <i class="ti-shopping-cart" aria-hidden="true" ></i> Add to Cart </button>

                    </div>
                                   
                                        <!-- Wishlist -->
                                        <div class="modal_pro_wishlist">
                                            <a href="wishlist.html" target="_blank"><i class="ti-heart"></i></a>
                                        </div>
                                        <!-- Compare -->
                                        
                                   {!! Form::close() !!}

                                    <div class="share_wf mt-30">
                                     
                                    </div>

                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach

        @endforeach

    </section>
    
    
   <!-- ***** Top Catagory Area Start ***** -->


    <section class="top_catagory_area d-md-flex clearfix">
        <div class="container">

        @foreach($adsmanager as $ads)
        <a href="{{$ads->url}}" class="">    <!-- Single Catagory -->
        <div class="single_catagory_area bg-img d-flex align-items-center" style="background-image: url({{asset('images/ads/'.$ads->image)}});">
            <div class="catagory-content">
                <h2></h2>
                <p></p>
               
            </div>
        </div> </a>
        @endforeach

        <!-- Single Catagory -->
   
        </div>
    </section>


    <!-- ***** Top Catagory Area End ***** -->
      
       <!-- ***** Best Rated/Onsale/Top Sale Area Start ***** -->

    <!-- ***** Best Rated/Onsale/Top Sale Area End ***** -->

   
   
    <!-- ***** Quick View Modal Area Start ***** -->
   
    <div class="modal fade" id="quickview" tabindex="-1" role="dialog" aria-labelledby="quickview" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close btn" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <div class="quickview_body">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-lg-5">
                                    <div class="quickview_pro_img">
                                        <img class="first_img" src="{{asset('public/frontend/img/product-img/new-1-back.png')}}" alt="">
                                        <img class="hover_img" src="{{asset('public/frontend/img/product-img/new-1.png')}}" alt="">
                                        <!-- Product Badge -->
                                        <div class="product_badge">
                                            <span class="badge-new">New</span>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 col-lg-7">
                                    <div class="quickview_pro_des">
                                        <h4 class="title">{{ $value->title }}</h4>
                                        <div class="top_seller_product_rating mb-15">
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                            <i class="fa fa-star" aria-hidden="true"></i>
                                        </div>
                                        <h5 class="price">$120.99 <span>$130</span></h5>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia expedita quibusdam aspernatur, sapiente consectetur accusantium perspiciatis praesentium eligendi, in fugiat?</p>
                                        <a href="#">View Full Product Details</a>
                                    </div>
                                    <!-- Add to Cart Form -->
                                    <form class="cart" method="post">
                                        <div class="quantity">
                                            <span class="qty-minus" onclick="var effect = document.getElementById('qty'); var qty = effect.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) effect.value--;return false;"><i class="fa fa-minus" aria-hidden="true"></i></span>
                                            <input type="number" class="qty-text" id="qty" step="1" min="1" max="12" name="quantity" value="1">
                                            <span class="qty-plus" onclick="var effect = document.getElementById('qty'); var qty = effect.value; if( !isNaN( qty )) effect.value++;return false;"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                        </div>
                                        <button type="submit" name="addtocart" value="5" class="cart-submit">Add to cart</button>
                                        <!-- Wishlist -->
                                        <div class="modal_pro_wishlist">
                                            <a href="wishlist.html" target="_blank"><i class="ti-heart"></i></a>
                                        </div>
                                        <!-- Compare -->
                                        <div class="modal_pro_compare">
                                            <a href="compare.html" target="_blank"><i class="ti-stats-up"></i></a>
                                        </div>
                                    </form>

                                    <div class="share_wf mt-30">
                                        <p>Share With Friend</p>
                                        <div class="_icon">
                                            <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                                            <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ***** Quick View Modal Area End ***** -->

    <!-- ***** Featured Products Area Start ***** -->
    <section class="featured_product_area">
        <div class="container" style="margin-top: 50px;margin-bottom: 50px">
            <div class="row">
                <!-- Featured Offer Area Start -->
                <div class="col-12 col-lg-7 item">
                    <div class="featured_offer_area d-flex align-items-center" style="background-image: url(img/bg-img/fea_offer.jpg);">
                        <div class="featured_offer_text">
                            <p>Summer 2017</p>
                            <h2>15% OFF</h2>
                            <h4>All kid’s items</h4>
                            <a href="#" class="btn bigshop-btn bigshop-btn-sm">Shop Now</a>
                        </div>
                    </div>
                </div>
                <!-- Featured Product Area Start -->
                <div class="col-12 col-lg-5 item">
                    <div class="section_heading featured">
                        <h5>Featured Products</h5>
                    </div>

                    <div class="featured_product_slides">

                    @foreach($specials as $special)

                        <!-- Single Featured Products Area Start -->
                        <div class="single_fea_product_slide">
                            <div class="fea_product_image">

                                @if(isset($special->itemImage[0]) and $special->itemImage[0]->photo!=null)

                                <img class="normal_img" src="{{asset('images/items/small/'.$special->itemImage[0]->photo)}}" alt="Featured">
                                 @endif
                             @if(isset($special->itemImage[1]) and $special->itemImage[1]->photo!=null)

                               <a href="{{url('/product-details/'.$special->id)}}"> <img class="hover_img" src="{{asset('images/items/small/'.$special->itemImage[1]->photo)}}" alt="Featured"></a>
                        @endif

                                <!-- Wishlist -->
                                <div class="product_wishlist">
                                    <a href="wishlist.html" target="_blank"><i class="ti-heart"></i></a>
                                </div>
                                <!-- Compare -->
                              <div class="product_compare">
                                    <a href="#"><i class="ti-shopping-cart"></i></a>
                                </div>
                                <!-- Add to cart -->
                                
                                <!-- Quick View -->
                                <div class="product_quick_view">
                                    <a href="#" data-toggle="modal" data-target="#quickview"><i class="ti-eye" aria-hidden="true"></i> Quick View</a>
                                </div>
                            </div>
                            <div class="featured_pro_desc">
                                <h6><a href="{{url('/product-details/'.$special->id)}}">{{$special->title}}</a></h6>
                                <p>&#2547; &nbsp;{{$special->discount}}</p>
                                <div class="product_rating">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                </div>
                            </div>
                        </div>

                        @endforeach


                        <!-- Single Featured Products Area Start -->
                        

                        <!-- Single Featured Products Area Start -->
                        
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- ***** Featured Products Area End ***** -->


    <!-- ***** Special Featured Area Start ***** -->
    <section class="special_feature_area d-md-flex align-items-center">
        <!-- Single Feature Area -->
        <div class="single_feature_area d-flex align-items-center justify-content-center">
            <div class="feature_icon">
                <i class="ti-truck"></i>
                <span><i class="ti-check" aria-hidden="true"></i></span>
            </div>
            <div class="feature_content">
                <h6>FREE SHIPPING</h6>
                <p>For orders above $100</p>
            </div>
        </div>
        <!-- Single Feature Area -->
        <div class="single_feature_area d-flex align-items-center justify-content-center">
            <div class="feature_icon">
                <i class="ti-headphone-alt"></i>
                <span><i class="ti-check" aria-hidden="true"></i></span>
            </div>
            <div class="feature_content">
                <h6>Customer Care</h6>
                <p>24/7 Friendly Support</p>
            </div>
        </div>
        <!-- Single Feature Area -->
        <div class="single_feature_area d-flex align-items-center justify-content-center">
            <div class="feature_icon">
                <i class="ti-back-left"></i>
                <span><i class="ti-check" aria-hidden="true"></i></span>
            </div>
            <div class="feature_content">
                <h6>Happy Returns</h6>
                <p>7 Days free Returns</p>
            </div>
        </div>
        <!-- Single Feature Area -->
        <div class="single_feature_area d-flex align-items-center justify-content-center">
            <div class="feature_icon">
                <i class="ti-credit-card"></i>
                <span><i class="ti-check" aria-hidden="true"></i></span>
            </div>
            <div class="feature_content">
                <h6>100% Money Back</h6>
                <p>If product is damaged</p>
            </div>
        </div>
    </section>






@endsection


<div class="col-12 col-md-5">
                    <div class="single_product_thumb">
                        <div id="product_details_slider" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">

                                @if(isset($products->itemImage[0]) and $products->itemImage[0]->photo!=null)

                                <li class="active" data-target="#product_details_slider" data-slide-to="0" style="background-image: url({{asset('images/items/big/'.$products->itemImage[0]->photo)}});">

                                </li>
                                      @else
                       
                   @endif


                            @if(isset($products->itemImage[1]) and $products->itemImage[1]->photo!=null)

                                <li data-target="#product_details_slider" data-slide-to="1" style="background-image: url({{asset('images/items/small/'.$products->itemImage[1]->photo)}});">
                                </li>

                                @else
                        
                   @endif


                                    @if(isset($products->itemImage[2]) and $products->itemImage[2]->photo!=null)

                                <li data-target="#product_details_slider" data-slide-to="2" style="background-image: url({{asset('images/items/small/'.$products->itemImage[2]->photo)}});">
                                </li>

                                @else
                
                   @endif

                                    @if(isset($products->itemImage[3]) and $products->itemImage[3]->photo!=null)

                                <li data-target="#product_details_slider" data-slide-to="1" style="background-image: url({{asset('images/items/small/'.$products->itemImage[3]->photo)}});">
                                </li>

                                @else
                     
                   @endif


                            </ol>
                            <div class="carousel-inner">
                                <div class="carousel-item active">

                                     @if(isset($products->itemImage[0]) and $products->itemImage[0]->photo!=null)

                                    <a class="gallery_img" href="{{asset('images/items/big/'.$products->itemImage[0]->photo)}}">
                                        <img class="d-block w-100" src="{{asset('images/items/small/'.$products->itemImage[0]->photo)}}" alt="First slide">
                                    </a>
                                    <!-- Product Badge -->
                                    <div class="product_badge">
                                        <span class="badge-new">New</span>
                                    </div>

                                                        @else
                       
                   @endif

                                </div>

                                <div class="carousel-item">
                                     @if(isset($products->itemImage[1]) and $products->itemImage[1]->photo!=null)
                                    <a class="gallery_img" href="{{asset('images/items/big/'.$products->itemImage[1]->photo)}}">
                                        <img class="d-block w-100" src="{{asset('images/items/small/'.$products->itemImage[1]->photo)}}" alt="Second slide">
                                    </a>
                                    <!-- Product Badge -->
                                    <div class="product_badge">
                                        <span class="badge-new">New</span>
                                    </div>

                               @else

                               <img id="image_load" src="{{asset('images/default/photo.png')}}">
                       
                   @endif
                                </div>

                                <div class="carousel-item">

                                        @if(isset($products->itemImage[2]) and $products->itemImage[2]->photo!=null)
                                    <a class="gallery_img" href="{{asset('images/items/big/'.$products->itemImage[2]->photo)}}">
                                        <img class="d-block w-100" src="{{asset('images/items/small/'.$products->itemImage[2]->photo)}}" alt="Third slide">
                                    </a>
                                    <!-- Product Badge -->
                                    <div class="product_badge">
                                        <span class="badge-new">New</span>
                                    </div>
                         @else

                         <img id="image_load" src="{{asset('images/default/photo.png')}}">
                       
                   @endif
                                </div>

                                <div class="carousel-item">

                                    @if(isset($products->itemImage[3]) and $products->itemImage[3]->photo!=null)

                                    <a class="gallery_img" href="{{asset('images/items/big/'.$products->itemImage[3]->photo)}}">
                                        <img class="d-block w-100" src="{{asset('images/items/small/'.$products->itemImage[3]->photo)}}" alt="Fourth slide">
                                    </a>
                                    <!-- Product Badge -->
                                    <div class="product_badge">
                                        <span class="badge-new">New</span>
                                    </div>

                                          @else

                                          <img id="image_load" src="{{asset('images/default/photo.png')}}">
                       
                   @endif
                                </div>

                            </div>
                        </div>
                    </div>
                </div>