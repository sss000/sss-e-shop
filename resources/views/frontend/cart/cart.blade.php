@extends('frontend._partials.app')
@section('title')
   Cart 

        @endsection

@section('content') 


        <!-- Bottom Header Area Start -->
    
    <!-- ***** Header Area End ***** -->

    <!-- ***** Breadcumb Area Start ***** -->
    <div class="breadcumb_area bg-img background-overlay-white" style="background-image: url({{asset('public/frontend/img/bg-img/breadcumb.jpg')}});">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{URL::to('/')}}">Home</a></li>
                        <li class="breadcrumb-item active">Cart</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Breadcumb Area End ***** -->

    <!-- ***** Cart Area Start ***** -->
    <div class="cart_area section_padding_100 clearfix">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="cart-table clearfix">

                        @if(count($cart)>0)
                        
                    <form action="#">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th><i class="fa fa-trash-o text-danger" aria-hidden="true"></i></th>
                                    <th>Image</th>
                                    <th>Product</th>
                                    <th>Price</th>
                                    <th width="5%">Qty</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($cart['content'] as $rowKey => $product)
                                            <?
                                            $item=$product->options;

                               
                             $link= $item->link;
                             $photo= $item->photo;
                                            
                                ?>
                                <tr>
                                    <td class="action"><a class="btn btn-danger btn-xs" href='{{url("cart-remove-item/$product->rowId")}}'>
                                        <i class="fa fa-times" aria-hidden="true"></i>

                                    </a></td>

                                    

                                    <td class="cart_product_img">

                                        <a href="#">
                                            @if($photo!=null)
                                            <img src='{{asset('images/items/small/'.$photo)}}' alt="Product" style="height: 50px; width: 50px">
                                        @else
                                         <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image cart_img" alt="{{ $product->title }}" src='{{asset("public/img/item/default.png")}}' />

                                      @endif

                                        </a>
                                    </td>

                                     


                                    <td class="cart_product_desc">

                                        <h5>{{$product->name}}</h5>
                                        @foreach($item->attr_details as $attr => $val )
                                            <p> <b> {{$attr}} : </b> {{$val}} </p>
                                        @endforeach

                                    </td>
                                    <td class="price"><span> BDT.&nbsp;{{$product->price}}</span></td>
                                    <td class="qty">
                                        <div class="quantity">

                                           <a class="decrease" href='{{url("decrement/$product->rowId")}}'><i class="fa fa-minus-square-o"></i></a>

                                       
                                           <span style="width: 50px">{{$product->qty}}</span>

                                            <a class="increment" href='{{url("increment/$product->rowId")}}'><i class="fa fa-plus-square-o"></i></a>
                                         
                                        </div>
                                    </td>
                                    <td class="total_price"><span>BDT.&nbsp;{{$product->subtotal}}</span></td>
                                </tr>
                          @endforeach

                            </tbody>
                            <tfoot>
                                <tr>
                                    <td>Total (tax incl.)</td>
                                    <td>BDT.&nbsp;{{Cart::tax()}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Total</strong></td>
                                    <td><strong>BDT.&nbsp;{{Cart::total()}}</strong></td>
                                </tr>
                            </tfoot>
                        </table>


                        </form>
                    
                                
                    </div>

                    <div class="cart- d-flex mt-30">
                        <div class="back-to-shop w-50">
                          
                            <a href="{{URL::to('/')}}"><button type="button" class="btn btn-outline-secondary">Back to Shopping</button></a>
                            
                        </div>
                        <div class="update-checkout w-50 text-right">

                           <a href="{{url('clear-cart')}}" style="background-color: #fff;"><button  type="button" class="btn btn-outline-danger">Clear Cart</button></a>

                           <a href="{{url('checkout')}}" style="background-color: #fff;"><button type="button" class="btn btn-outline-secondary">Checkout</button></a>
                           
                          
                        </div>
                    </div>

                            @else
                                        <div class="row">
                                            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                                                <h2>No Items in Cart!</h2>
                                            </div>
                                        </div>
                                    @endif


                </div>
            </div>
        </div>
    </div>
    <!-- ***** Cart Area End ***** -->

    <!-- ***** Special Featured Area Start ***** -->
    <section class="special_feature_area d-md-flex align-items-center">
        <!-- Single Feature Area -->
        <div class="single_feature_area d-flex align-items-center justify-content-center">
            <div class="feature_icon">
                <i class="ti-truck"></i>
                <span><i class="ti-check" aria-hidden="true"></i></span>
            </div>
            <div class="feature_content">
                <h6>FREE SHIPPING</h6>
                <p>For orders above $100</p>
            </div>
        </div>
        <!-- Single Feature Area -->
        <div class="single_feature_area d-flex align-items-center justify-content-center">
            <div class="feature_icon">
                <i class="ti-headphone-alt"></i>
                <span><i class="ti-check" aria-hidden="true"></i></span>
            </div>
            <div class="feature_content">
                <h6>Customer Care</h6>
                <p>24/7 Friendly Support</p>
            </div>
        </div>
        <!-- Single Feature Area -->
        <div class="single_feature_area d-flex align-items-center justify-content-center">
            <div class="feature_icon">
                <i class="ti-back-left"></i>
                <span><i class="ti-check" aria-hidden="true"></i></span>
            </div>
            <div class="feature_content">
                <h6>Happy Returns</h6>
                <p>7 Days free Returns</p>
            </div>
        </div>
        <!-- Single Feature Area -->
        <div class="single_feature_area d-flex align-items-center justify-content-center">
            <div class="feature_icon">
                <i class="ti-credit-card"></i>
                <span><i class="ti-check" aria-hidden="true"></i></span>
            </div>
            <div class="feature_content">
                <h6>100% Money Back</h6>
                <p>If product is damaged</p>
            </div>
        </div>
    </section>
    <!-- ***** Special Featured Area End ***** -->

      @endsection
