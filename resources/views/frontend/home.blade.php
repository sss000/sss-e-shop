@extends('frontend._partials.app')
    @section('title')
    
            {{$info->company_name}}
      
       
@endsection

@section('content')


    <!-- ***** Welcome Slides Area Start ***** -->
    <section class="welcome_area">
        <div class="welcome_slides">
            <!-- Single Slide Start --> 
           
            <!-- Single Slide Start -->
              @foreach($sliders as $slider)

            <div class="single_slide height-700 bg-img" style="background-image: url({{asset($slider->image)}});">
                <div class="container h-100">
      
                    <div class="row h-100 align-items-center">
                         <div class="col-12 col-md-8">
                            <div class="welcome_slide_text">
                                <p data-animation="fadeInUp" data-delay="0">{{$slider->top_caption}}</p>
                                <h2 data-animation="fadeInUp" data-delay="300ms">{{$slider->middle_caption}}</h2>
                                 <h4 data-animation="fadeInLeftBig" data-delay="600ms">{{$slider->buttom_caption}}</h4>

                                 

                           



                                    <button class="btn buy-now" type="submit" data-animation="fadeInUp" data-delay="600ms"> Shop Now </button>

                                  

                            </div>
                        </div>
                   
                    </div>
 
                </div>
            </div>

              @endforeach

            <!-- Single Slide Start -->


              
      
        </div>
    </section>
    <!-- ***** Welcome Slides Area End ***** -->

    

      
    <!-- ***** New Arrivals Area Start ***** -->
     <section class="best_rated_onsale_top_sellares home-2 section_padding_70">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs_area">
                     
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade show active" id="top-sellers">
                                <div class="top_sellers_area">
                                    <div class="row">
                                     

                                       

                                       
                                        @foreach($product as $item)
                                        <div class="col-12 col-sm-6 col-lg-3">
                                            <div class="single_top_sellers">
                                                <div class="top_seller_image">

                                                    
                                                      @if(isset($item->itemImage[0]) and $item->itemImage[0]->photo!=null)
                                                    <a href="{{url('/product-details/'.$item->id)}}"><img src="{{asset('images/items/small/'.$item->itemImage[0]->photo)}}" alt=""></a>
                                                   @endif
                                                    <!-- Wishlist -->
                                                 

                                                                      <? 
                                              if(Auth::check()){

                                                $userId=Auth::user()->id;
                                                $wish=App\Model\Wishlist::where(['fk_item_id'=>$item->id,'fk_user_id'=>$userId])->count();

                                              }else{
                                                $userId='';
                                                $wish=0;
                                              } 
                                               ?>


                                                <!-- Wishlist -->
                                                <div class="product_wishlist">
                                                    @if($wish>0)
                                                    <a><i class="ti-heart"></i></a>
                                                    @else
                                                      <a href='{{URL::to("wishlist-store?fk_item_id=$item->id&fk_user_id=$userId")}}'><i class="ti-heart"></i></a>
                                                       @endif
                                                </div>

                                                    <!-- Compare -->
                                                   <div class="product_compare">
                                                        <a href="{{url('/product-details/'.$item->id)}}"><i class="ti-shopping-cart"></i></a>
                                                    </div>
                                                    <!-- Add to cart -->
                                                   
                                                </div>
                                                <div class="top_seller_desc">
                                                    <h5><a href="{{url('/product-details/'.$item->id)}}"">{{$item->title}}</a></h5>
                                                    <h6>BDT.&nbsp;{{$item->discount}} &nbsp;&nbsp;<span>BDT.{{$item->price}}</span></h6>
                                                </div>
                                            </div>
                                        </div>


                                  @endforeach


                                    </div>
                                </div>
                            </div>

                            

                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Best Rated/Onsale/Top Sale Area End ***** -->
     <!-- ***** New Arrivals Area Start ***** -->
    
    
    
   <!-- ***** Top Catagory Area Start ***** -->


    <section class="top_catagory_area d-md-flex clearfix">
        <div class="container">

        @foreach($adsmanager as $ads)
        <a href="{{$ads->url}}" class="">    <!-- Single Catagory -->
        <div class="single_catagory_area bg-img d-flex align-items-center" style="background-image: url({{asset('images/ads/'.$ads->image)}});">
            <div class="catagory-content">
                <h2></h2>
                <p></p>
               
            </div>
        </div> </a>
        @endforeach

        <!-- Single Catagory -->
   
        </div>
    </section>


    <!-- ***** Top Catagory Area End ***** -->
      
       <!-- ***** Best Rated/Onsale/Top Sale Area Start ***** -->

    <!-- ***** Best Rated/Onsale/Top Sale Area End ***** -->

   
   
    <!-- ***** Quick View Modal Area Start ***** -->
    <!-- ***** Quick View Modal Area Start ***** -->

    <!-- ***** Quick View Modal Area End ***** -->

    <!-- ***** Featured Products Area Start ***** -->
    <section class="featured_product_area">
        <div class="container" style="margin-top: 50px;margin-bottom: 50px">
            <div class="row">
                <!-- Featured Offer Area Start -->
                <div class="col-12 col-lg-7 item">
                    <div class="featured_offer_area d-flex align-items-center" style="background-image: url({{asset($Discount->image)}});">
                        <div class="featured_offer_text">
                            <p>{{$Discount->top_caption}}</p>
                            <h2>{{$Discount->middle_caption}}</h2>
                            <h4>{{$Discount->buttom_caption}}</h4>
                            <a href="{{$Discount->url}}" class="btn bigshop-btn bigshop-btn-sm">Shop Now</a>
                        </div>
                    </div>
                </div>
                <!-- Featured Product Area Start -->
                <div class="col-12 col-lg-5 item">
                    <div class="section_heading featured">
                        <a href="{{URL::to('Special-product')}}"><h5>Featured Products</h5></a>
                    </div>

                    <div class="featured_product_slides">

                    @foreach($specials as $special)

                        <!-- Single Featured Products Area Start -->
                        <div class="single_fea_product_slide">
                            <div class="fea_product_image">

                                @if(isset($special->itemImage[0]) and $special->itemImage[0]->photo!=null)

                                <img class="normal_img" src="{{asset('images/items/small/'.$special->itemImage[0]->photo)}}" alt="Featured">
                                 @endif
                             @if(isset($special->itemImage[1]) and $special->itemImage[1]->photo!=null)

                               <a href="{{url('/product-details/'.$special->id)}}"> <img class="hover_img" src="{{asset('images/items/small/'.$special->itemImage[1]->photo)}}" alt="Featured"></a>
                        @endif

                                <!-- Wishlist -->
                               

                                  <? 
                              if(Auth::check()){

                                $userId=Auth::user()->id;
                                $wish=App\Model\Wishlist::where(['fk_item_id'=>$special->id,'fk_user_id'=>$userId])->count();

                              }else{
                                $userId='';
                                $wish=0;
                              } 
                               ?>


                                <!-- Wishlist -->
                                <div class="product_wishlist">
                                    @if($wish>0)
                                    <a><i class="ti-heart"></i></a>
                                    @else
                                      <a href='{{URL::to("wishlist-store?fk_item_id=$special->id&fk_user_id=$userId")}}'><i class="ti-heart"></i></a>
                                       @endif
                                </div>


                                <!-- Compare -->
                              <div class="product_compare">
                                    <a href="{{url('/product-details/'.$special->id)}}"><i class="ti-shopping-cart"></i></a>
                                </div>
                                <!-- Add to cart -->
                                
                                <!-- Quick View -->
                                <div class="product_quick_view">
                                    <a href="{{url('/product-details/'.$special->id)}}" data-toggle="modal" data-target="#quickview{{ $special->id }}"><i class="ti-eye" aria-hidden="true"></i> Quick View</a>
                                </div>


                            </div>
                            <div class="featured_pro_desc">
                                <h6><a href="{{url('/product-details/'.$special->id)}}">{{$special->title}}</a></h6>
                                
                                <div class="product_description">
                                 <h5>&#2547; &nbsp;{{$special->discount}}&nbsp;&nbsp;&nbsp;&nbsp;
                                    <span> &#2547; {{$special->price}}</span></h5>
                                </div>
                            </div>
                        </div>






                        @endforeach


                        <!-- Single Featured Products Area Start -->
                        

                        <!-- Single Featured Products Area Start -->
                        
                    </div>

                </div>
            </div>
        </div>
    </section>
    @foreach($specials as $special)

        <div class="modal fade" id="quickview{{ $special->id }}" tabindex="-1" role="dialog" aria-labelledby="quickview" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close btn" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <div class="quickview_body">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-lg-5">
                                    <div class="quickview_pro_img">
                                         @if(isset($special->itemImage[0]) and $special->itemImage[0]->photo!=null)

                                        <img class="first_img" src="{{asset('images/items/small/'.$item->itemImage[0]->photo)}}" alt="">
                                        @endif
                                         @if(isset($special->itemImage[1]) and $special->itemImage[1]->photo!=null)
                                        <img class="hover_img" src="{{asset('images/items/small/'.$special->itemImage[1]->photo)}}" alt="">
                                          @endif
                                        <!-- Product Badge -->
                                        <div class="product_badge">
                                            <span class="badge-new">New</span>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 col-lg-7">
                                    <div class="quickview_pro_des">
                                        <h4 class="title">{{$special->title}}</h4>
                                      
                                        <h5 class="price">BDT.{{$special->discount}} <span>BDT.{{$special->price}}</span></h5>
                                        <p>{{$special->meta_description}}</p>
                                        <a href="{{url('/product-details/'.$special->id)}}">View Full Product Details</a>
                                    </div>
                                    <!-- Add to Cart Form -->
                                    <div class="cart">
                                       

                                        <a href="{{url('/product-details/'.$special->id)}}">
                                            <button type="submit" name="addtocart" value="5" class="cart-submit">Add to cart</button>
                                        </a>
                                        

                                             <? 
                                              if(Auth::check()){

                                                $userId=Auth::user()->id;
                                                $wish=App\Model\Wishlist::where(['fk_item_id'=>$item->id,'fk_user_id'=>$userId])->count();

                                              }else{
                                                $userId='';
                                                $wish=0;
                                              } 
                                            ?>
                                        <!-- Wishlist -->


                                        <div class="modal_pro_wishlist">
                                            @if($wish>0)
                                            <a><i class="ti-heart"></i></a>
                                             @else
                                             <a href='{{URL::to("wishlist-store?fk_item_id=$item->id&fk_user_id=$userId")}}'><i class="ti-heart"></i></a>
                                       @endif
                                        </div>
                                     
                                    </div>

                                    <div class="share_wf mt-30">
                                        <p>Share With Friend</p>
                                        <div class="_icon">
                                             <div id="share"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     @endforeach


<!-- brand start -->


<div class="owl-carousel owl-theme all-brand" style="height: 60px;">

    @foreach($brand as $bra)

    <div class="item">
        <!-- item one -->
        <div class="brand-name">
            <div class="brand-thumb">
                <img src="{{ asset('images/Brand/small/'.$bra->image) }}" style="height: 60px">
            </div>
        </div>

    </div>

    @endforeach


 
</div>


    <!-- ***** Featured Products Area End ***** -->


    <!-- ***** Special Featured Area Start ***** -->
    <section class="special_feature_area d-md-flex align-items-center">
        <!-- Single Feature Area -->
        <div class="single_feature_area d-flex align-items-center justify-content-center">
            <div class="feature_icon">
                <i class="ti-truck"></i>
                <span><i class="ti-check" aria-hidden="true"></i></span>
            </div>
            <div class="feature_content">
                <h6>FREE SHIPPING</h6>
                <p>For orders above $100</p>
            </div>
        </div>
        <!-- Single Feature Area -->
        <div class="single_feature_area d-flex align-items-center justify-content-center">
            <div class="feature_icon">
                <i class="ti-headphone-alt"></i>
                <span><i class="ti-check" aria-hidden="true"></i></span>
            </div>
            <div class="feature_content">
                <h6>Customer Care</h6>
                <p>24/7 Friendly Support</p>
            </div>
        </div>
        <!-- Single Feature Area -->
        <div class="single_feature_area d-flex align-items-center justify-content-center">
            <div class="feature_icon">
                <i class="ti-back-left"></i>
                <span><i class="ti-check" aria-hidden="true"></i></span>
            </div>
            <div class="feature_content">
                <h6>Happy Returns</h6>
                <p>7 Days free Returns</p>
            </div>
        </div>
        <!-- Single Feature Area -->
        <div class="single_feature_area d-flex align-items-center justify-content-center">
            <div class="feature_icon">
                <i class="ti-credit-card"></i>
                <span><i class="ti-check" aria-hidden="true"></i></span>
            </div>
            <div class="feature_content">
                <h6>100% Money Back</h6>
                <p>If product is damaged</p>
            </div>
        </div>
    </section>








@endsection
 @section('script')
 


    


    <script type="text/javascript">

                 
    $("#share").jsSocials({
       showLabel: false,
          showCount: "inside",
          shareIn: "popup",
            shares: [ "twitter", "facebook", "googleplus", "pinterest", "whatsapp","linkedin"]
        });



       


</script>




       
      
   

 @endsection