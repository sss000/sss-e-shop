@extends('frontend._partials.app')
@section('title')
    {{$products->title}}|| DailyshoppersBD

        @endsection

@section('content')
        <!-- Bottom Header Area Start -->
        <style type="text/css">
          ._icon{}
          ._icon ul{}
          ._icon ul li{display: inline;}
        </style>
    <!-- ***** Header Area End ***** -->

    <!-- <<<<<<<<<<<<<<<<<<<< Single Product Details Area Start >>>>>>>>>>>>>>>>>>>>>>>>> -->
    <section class="single_product_details_area ">

       

        <div class="container">

               <div class="col-12">
                        <div class="row">
                        <ol class="breadcrumb" style="background:  #fff;">
                            <li class="breadcrumb-item"><a href="{{URL::to('/')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{URL::to('category-product/'.$products->categories->id)}}">{{$products->categories->category_name}}</a></li>
                            <li class="breadcrumb-item active">{{$products->link}}</li>
                        </ol>
                    </div>
                    </div>
            <div class="row">


                 

    <div class="app-figure col-12 col-md-5" id="zoom-fig" >

             @if(isset($products->itemImage[0]) and $products->itemImage[0]->photo!=null)

                <a id="Zoom-1" class="MagicZoom" title="{{$products->title}}"
                    href="{{asset('images/items/big/'.$products->itemImage[0]->photo)}}?scale.height=100"
                >
                    <img src="{{asset('images/items/big/'.$products->itemImage[0]->photo)}}?scale.height=100" alt=""/>
                </a>


                  @else
                       
             @endif


        <div class="selectors">

            @if(isset($products->itemImage[0]) and $products->itemImage[0]->photo!=null)


           <a
                data-zoom-id="Zoom-1"
                href="{{asset('images/items/big/'.$products->itemImage[0]->photo)}}"
                data-image="{{asset('images/items/big/'.$products->itemImage[0]->photo)}}?scale.height=300"
            >
                <img srcset="{{asset('images/items/small/'.$products->itemImage[0]->photo)}}?scale.width=112 2x" src="{{asset('images/items/small/'.$products->itemImage[0]->photo)}}" style="height: 70px; width: 107px; margin-top: 5px;" />
            </a>


                  @else
                       
             @endif

              @if(isset($products->itemImage[1]) and $products->itemImage[1]->photo!=null)

           <a
                data-zoom-id="Zoom-1"
                href="{{asset('images/items/big/'.$products->itemImage[1]->photo)}}"
                data-image="{{asset('images/items/big/'.$products->itemImage[1]->photo)}}?scale.height=300"
            >
                <img srcset="({{asset('images/items/small/'.$products->itemImage[1]->photo)}}?scale.width=112 2x" src="{{asset('images/items/small/'.$products->itemImage[1]->photo)}}" style="height: 70px; width: 107px; margin-top: 5px;"/>
            </a>


               @else
                        
            @endif

        @if(isset($products->itemImage[2]) and $products->itemImage[2]->photo!=null)

           <a
                data-zoom-id="Zoom-1"
                href="{{asset('images/items/big/'.$products->itemImage[2]->photo)}}"
                data-image="{{asset('images/items/big/'.$products->itemImage[2]->photo)}}?scale.height=300"
            >
                <img srcset="{{asset('images/items/small/'.$products->itemImage[2]->photo)}}?scale.width=112 2x" src="{{asset('images/items/small/'.$products->itemImage[2]->photo)}}?scale.width=56" style="height: 70px; width: 107px; margin-top: 5px;" />
            </a>

               @else
                 
         @endif

 @if(isset($products->itemImage[3]) and $products->itemImage[3]->photo!=null)
           <a
                data-zoom-id="Zoom-1"
                href="{{asset('images/items/big/'.$products->itemImage[3]->photo)}}"
                data-image="{{asset('images/items/big/'.$products->itemImage[3]->photo)}}?scale.height=300"
            >
                <img srcset="{{asset('images/items/small/'.$products->itemImage[3]->photo)}}?scale.width=112 2x" src="{{asset('images/items/small/'.$products->itemImage[3]->photo)}}?scale.width=56" style="height: 70px; width: 107px; margin-top: 5px;" />
            </a>

                 @else
                
         @endif
        

       


         

        </div> 

    </div>

                
                
                <div class="col-12 col-md-7">
                    <div class="single_product_desc">
                        <h2 ><a href="#">{{$products->title}}</a></h2>

                         @if($products->fk_brand_id!=null)

                        <span><b>Brands:</b>&nbsp &nbsp{{$products->itemBrands->name}}</span> </br>

                        @endif
                        <span><b>Code:</b>&nbsp &nbsp{{$products->product_code}}</span> </br>

                  

                   <h6><b style="color: #01a161; padding-top: 12px;">BDT:{{$products->discount}}</b>&nbsp;&nbsp;<span style="color: #888;font-size:13px;text-decoration: line-through;">BDT:{{$products->price}}</span></h6>



                     {!! Form::open(['url'=>'add-to-cart','method'=>'POST']) !!}
                       
                     <div class="col-md-12">
                        <div class="row">

                        <? $attribute = App\ItemAttribute::groupBy('attribute_id')->get(); ?>

                        @foreach($attribute as $attr)
                           
                            <div class="col-md-12">
                            @foreach($products->itemAttribute as $option)
                          
                            @if($attr->attribute_id==$option->attribute_id)
                            <div class="radio_label">

                            <input required="" type="radio" value="{{$option->id}}" name="attribute[{{$products->id}}][[{{$option->attribute_id}}]]" id="radio{{$option->id}}">

                            <label style="width: 50px;height: 25px; margin-right: 12px;     margin-bottom: 5px;" for="radio{{$option->id}}" title="  {{$option->optionAttribute->description}}">  {{$option->optionAttribute->option_name}}</label>
                           
                          </div>
                          
                          @endif
                          @endforeach
                          </div>

                      @endforeach
                  </div>
                       
                    </div>
                       


                                           
                      
                                               

                        
                            <input type="hidden" value="{{$products->id}}" name="id">

                                <input type="hidden" value="1" name="quantity">


                            <button type="submit" class="btn bigshop-btn bigshop-btn-sm ml-2" style="    margin: 12px;"><i class="ti-shopping-cart" aria-hidden="true"></i>Add to cart</button>
 
                            <a href="{{URL::to('cart')}}"><button type="submit" class="btn bigshop-btn bigshop-btn-sm ml-2">Buy Now</button></a>

                           
                    
                        
                        <div class="others_info_area mb-15 d-flex">



                                           <? 
                                          if(Auth::check()){

                                            $userId=Auth::user()->id;
                                            $wish=App\Model\Wishlist::where(['fk_item_id'=>$products->id,'fk_user_id'=>$userId])->count();

                                          }else{
                                            $userId='';
                                            $wish=0;
                                          } 
                                           ?>


                                            <!-- Wishlist -->
                                          
                                @if($wish>0)  

                                  <a><i class="ti-heart"></i>ADD TO WISHLIST</a>
                                 @else         
                            <a class="add_to_wishlist" href='{{URL::to("wishlist-store?fk_item_id=$products->id&fk_user_id=$userId")}}'><i class="fa fa-heart" aria-hidden="true"></i> ADD TO WISHLIST</a>
                             @endif


                           
                            

                      </div>
                      

                         

                <!-- Place this tag where you want the share button to render. -->
               <!-- Place this tag where you want the share button to render. -->


                       
                                           

                        
     {!! Form::close() !!}

               <div class="share_wf mt-30">
                  <p>Share With Friend</p>
                <div class="_icon">


                   <div id="share"></div>
                   
                  


                    <!-- Place this tag in your head or just before your close body tag. -->
                

                  <!-- Place this tag where you want the share button to render. -->
                 


                     
                     
                </div>
                
                </div>

                
                    
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="product_details_tab clearfix">
                        <!-- Tabs -->
                        <ul class="nav nav-tabs" role="tablist" id="product-details-tab">
                            <li class="nav-item">
                                <a href="#description" class="nav-link active" data-toggle="tab" role="tab">Description</a>
                            </li>
                         
                      <!--       <li class="nav-item">
                          <a href="#addi-info" class="nav-link" data-toggle="tab" role="tab">Additional Information</a>
                      </li>
                      <li class="nav-item">
                          <a href="#refund" class="nav-link" data-toggle="tab" role="tab">Meta Description</a>
                      </li> -->
                        </ul>
                        <!-- Tab Content -->
                        <div class="tab-content">

                            <div role="tabpanel" class="tab-pane fade show active" id="description">
                                <div class="description_area">
                                    <h6>Description</h6>
                                    <p><?php echo $products->description ?></p>
                               
                           
                                </div>
                            </div>

                      <!--       
                      
                      <div role="tabpanel" class="tab-pane fade" id="addi-info">
                          <div class="additional_info_area">
                              <h6>Additional Info</h6>
                              <p><? echo $products->additional_info ?></p> 
                          </div>
                      </div>
                      
                      <div role="tabpanel" class="tab-pane fade" id="refund">
                          <div class="refund_area">
                              <h6>Meta Description</h6>
                              <p><? echo $products->meta_description ?></p>
                                
                           
                      
                             
                          </div>
                      </div>
                       -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="you_may_like_area section_padding_30_30 clearfix">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="section_heading">
                        <h5>Related Product</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="you_make_like_slider">

                        @foreach($related_product as $item)

                        <!-- Singel Arrivals Slide Start -->
                        <div class="single_you_make_like_slide">
                            <div class="product_image">

                                @if(isset($item->itemImage[0]) and $item->itemImage[0]->photo!=null)
                                <!-- Product Image -->
                                <img class="normal_img" src="{{asset('images/items/small/'.$item->itemImage[0]->photo)}}" alt="">
                                @endif

                                @if(isset($item->itemImage[1]) and $item->itemImage[1]->photo!=null)

                               <a href="{{url('/product-details/'.$item->id)}}"> <img class="hover_img" src="{{asset('images/items/small/'.$item->itemImage[1]->photo)}}" alt=""></a>
                                @endif

                                <!-- Product Badge -->
                                <div class="product_badge">
                                    <span class="badge-new">New</span>
                                </div>
                                <!-- Wishlist -->
                                <? 
                              if(Auth::check()){

                                $userId=Auth::user()->id;
                                $wish=App\Model\Wishlist::where(['fk_item_id'=>$item->id,'fk_user_id'=>$userId])->count();

                              }else{
                                $userId='';
                                $wish=0;
                              } 
                               ?>


                                <!-- Wishlist -->
                                <div class="product_wishlist">
                                    @if($wish>0)
                                    <a><i class="ti-heart"></i></a>
                                    @else
                                      <a href='{{URL::to("wishlist-store?fk_item_id=$item->id&fk_user_id=$userId")}}'><i class="ti-heart"></i></a>
                                       @endif
                                </div>
                                <!-- Compare -->
                               
                                <!-- Add to cart -->
                                <div class="product_add_to_cart">
                                    <a href="{{url('/product-details/'.$item->id)}}"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Add to Cart</a>
                                </div>
                                <!-- Quick View -->
                                <div class="product_quick_view">
                                    <a href="#" data-toggle="modal" data-target="#quickview{{ $item->id }}"><i class="fa fa-eye" aria-hidden="true"></i> Quick View</a>
                                </div>
                            </div>
                            <!-- Product Description -->
                            <div class="product_description">

                               @if($item->fk_brand_id!=null)
                                <p class="brand_name">{{$item->itemBrands->name}}</p>
                                @endif
                                <h5><a href="{{url('/product-details/'.$item->id)}}">{{$item->title}}</a></h5>
                                <h6>BDT:{{$item->discount}} <span>BDT:{{$item->price}}</span></h6>
                            </div>
                        </div>



 @endforeach





                       

                        
                       

                    </div>

                </div>
            </div>
        </div>
    </section>

         @foreach($related_product as $item)
          <div class="modal fade" id="quickview{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="quickview" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <button type="button" class="close btn" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div class="modal-body">
                    <div class="quickview_body">
                        <div class="container">
                            <div class="row">
                                <div class="col-12 col-lg-5">
                                    <div class="quickview_pro_img">
                                        



                                           @if(isset($item->itemImage[0]) and $item->itemImage[0]->photo!=null)
                                <!-- Product Image -->
                                <img class="normal_img" src="{{asset('images/items/small/'.$item->itemImage[0]->photo)}}" alt="">
                                @endif

                                @if(isset($item->itemImage[1]) and $item->itemImage[1]->photo!=null)

                               <a href="{{url('/product-details/'.$item->id)}}"> <img class="hover_img" src="{{asset('images/items/small/'.$item->itemImage[1]->photo)}}" alt=""></a>
                                @endif



                                       

                                        <!-- Product Badge -->

                                        <div class="product_badge">
                                            <span class="badge-new">New</span>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-12 col-lg-7">
                                    <div class="quickview_pro_des">
                                        <h4 class="title">{{$item->title}}</h4>
                                        <div class="top_seller_product_rating mb-15">
                                           
                                        </div>
                                        <h5 class="price">BDT:{{$item->discount}} &nbsp;<span>BDT:{{$item->price}}</span></h5>
                                        <p>{{$item->meta_description}}</p>
                                        <a href="{{url('/product-details/'.$item->id)}}">View Full Product Details</a>
                                    </div>



                                    <!-- Add to Cart Form -->
                                   {!! Form::open(['url'=>'add-to-cart','method'=>'POST','class'=>'cart']) !!}



                                <div class="col-md-7">
                                    <div class="row">


                                  <? $attribute = App\ItemAttribute::groupBy('attribute_id')->get(); ?>

                                    @foreach($attribute as $attr)
                                       
                                        <div class="col-md-8">
                                          <div class="row">
                                        @foreach($item->itemAttribute as $option)
                                      
                                        @if($attr->attribute_id==$option->attribute_id)
                                         <div class="radio_label">

                                        <input required="" type="radio" value="{{$option->id}}" name="attribute[{{$item->id}}][[{{$option->attribute_id}}]]" id="radio{{$option->id}}">

                                        <label style="width: 50px;height: 25px; margin-right: 12px;     margin-bottom: 5px;" for="radio{{$option->id}}" title="  {{$option->optionAttribute->description}}">  {{$option->optionAttribute->option_name}}</label>
                                       
                                      </div>
                                      
                                      @endif
                                      @endforeach
                                    </div>
                                      </div>

                                  @endforeach

                        </div>
                                       
                                    </div>



                                        <div>
                                    

                                            <input type="hidden" value="{{$item->id}}" name="id">

                                            <input type="hidden" value="1" name="quantity">


                                                <button class="cart-submit" type="submit"> <i class="ti-shopping-cart" aria-hidden="true" ></i> Add to Cart </button>

                    </div>




                                   
                                        <!-- Wishlist -->
                                       



                               <? 
                              if(Auth::check()){

                                $userId=Auth::user()->id;
                                $wish=App\Model\Wishlist::where(['fk_item_id'=>$item->id,'fk_user_id'=>$userId])->count();

                              }else{
                                $userId='';
                                $wish=0;
                              } 
                               ?>


                                <!-- Wishlist -->
                                <div class="modal_pro_wishlist">
                                    @if($wish>0)
                                    <a><i class="ti-heart"></i></a>
                                    @else
                                      <a href='{{URL::to("wishlist-store?fk_item_id=$item->id&fk_user_id=$userId")}}'><i class="ti-heart"></i></a>
                                       @endif
                                </div>

                                        <!-- Compare -->
                                        
                                   {!! Form::close() !!}


                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     @endforeach



    <!-- <<<<<<<<<<<<<<<<<<<< Single Product Details Area End >>>>>>>>>>>>>>>>>>>>>>>>> -->


    <!-- <<<<<<<<<<<<<<<<<<<< Quick View Modal Area End >>>>>>>>>>>>>>>>>>>>>>>>> -->

    @endsection


 @section('script')
 


    


    <script type="text/javascript">

                 
    $("#share").jsSocials({
       showLabel: false,
          showCount: "inside",
          shareIn: "popup",
            shares: [ "twitter", "facebook", "googleplus", "pinterest", "whatsapp","linkedin"]
        });



       


</script>




       
      
   

 @endsection