<?php 

$menus=DB::table('menu')->where('status',1)->orderBy('serial_num','ASC')->get();
$categories=DB::table('categories')->where('status',1)->get();

    $url= Request::path();
 ?> 


<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from demo.designing-world.com/bigshop-ab/bigshop/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 11 Jul 2018 13:14:32 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
     <title>@yield('title')</title>

    <!-- Favicon  -->
    <link rel="shortcut icon" href="{{asset('public/frontend/img/core-img/favicon.ico')}}">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/css/core-style.css')}}">
    <link rel="stylesheet" href="{{asset('public/frontend/css/style.css')}}">

    <!-- Responsive CSS -->
    <link href="{{asset('public/frontend/css/responsive.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">

</head>

<body>
    <!-- ***** Header Area Start ***** -->
    <!-- ***** Header Area Start ***** -->
    <header class="header_area home-2">
        <!-- Top Header Area Start -->
        <div class="top_header_area">
            <div class="container h-100">
                <div class="row h-100">
                    <div class="col-12 col-md-5 h-100">
                        <div class="top_single_area h-100 d-flex align-items-center">
                            <div class="top_menu">
                                <ul>
                                    <li><a href="#">Hotline: 880-{{$info->mobile_no}}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-7 h-100">
                        <div class="top_single_area h-100 d-flex align-items-center justify-content-end">
                            <!-- Login Area Start -->
                            <div class="login_area">
                                <p><a href="#" ><i class="fa fa-home" aria-hidden="true"></i> Home |</a>
                                    <a href="#" >How to buy |</a>
                                    <a href="#" target="_blank">Cash on delivery |</a>
                                    <a href="#" target="_blank">Easy Replacement |</a>
                                    <a href="#" target="_blank">Seller Corner |</a>
                                    <a href="#" target="_blank">eStore</a>
                                </p>
                            </div>
                            <!-- Get Support -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Top Header Area End -->


        <!-- Bottom Header Area Start -->
        <div class="bottom-header-area" id="stickyHeader">
            <div class="main_header_area">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <!-- Logo Area Start -->
                        <div class="col-6 col-md-3">
                            <div class="logo_area">
                                <a href="{{URL::to('/')}}"><img src="{{asset('images/logo/'.$info->logo)}}"></a>
                            </div>
                        </div>
                        <!-- Search Area Start -->
                        <div class="col-12 col-md-6">
                            <div class="hero_search_area">
                                <form action="#" method="get" style="border: 1px solid;">
                                    <input type="search" class="form-control" id="search" aria-describedby="search" placeholder="Search for products, brands and catagory">
                                    <button type="button" class="btn btn-warning" style="    background: #FF6600;color: white;margin: 2px 2px 2px 0;padding: 0 74px 0 16px;">Search</button>
                                </form>
                            </div>
                        </div>
                        <!-- Hero Meta Area Start -->
                        <div class="col-6 col-md-3">
                            <div class="hero_meta_area d-flex text-right align-items-center justify-content-end">

                                @php

                                  $total = floatval(str_replace(',','', Cart::total()));
                                    $cart = [
                                        'totalAmount'=>$total,
                                        'total'=>Cart::total(),
                                        'tax'=>Cart::tax(),
                                        'subtotal'=>Cart::subtotal(),
                                        'count'=>Cart::count(),
                                        'content'=>Cart::content(),
                                    ];

                                    @endphp

                                <!-- Cart Area -->
                                @if(count($cart)>0)

                                <form action="#">

                                <div class="cart">
                                    <a href="#" id="header-cart-btn" target="_blank"><i class="ti-bag"></i><span class="cart_quantity">{{Cart::count()}}</span></a>
                                    <!-- Cart List Area Start -->
                                    <ul class="cart-list">

                                  @foreach($cart['content'] as $rowKey => $product)


                                  <?
                                      $item=$product->options;
                                  $link= $item->link;
                                $photo= $item->photo;

                                  ?>
                                     
                                        <li>
                                            <a href="#" class="image">

                                                        @if($photo!=null)
                                                            <img src='{{asset('images/items/small/'.$photo)}}' alt="Product" style="height: 50px; width: 50px" class="cart-thumb">
                                                        @else
                                                         <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image cart_img" alt="{{ $product->title }}" src='{{asset("public/img/item/default.png")}}' />

                                                      @endif

                                            </a>
                                            <div class="cart-item-desc">
                                                <h6><a href="{{url('/product-details/'.$product->id)}}">{{$product->name}}</a></h6>
                                                <p><span class="price">&#2547;&nbsp{{$product->price}}</span></p>
                                            </div>
                                        </li>

                                        @endforeach

                                        <li class="total">
                                            <span>Total: &#2547;&nbsp{{Cart::total()}}</span>
                                            <a href="{{URL::to('cart')}}" class="btn btn-sm btn-cart">Cart</a>
                                            <a href="{{url('checkout')}}" class="btn btn-sm btn-checkout">Checkout</a>
                                        </li>
                                     
                                    </ul>
                                </div>

                                 </form>

                                 @endif


                                <!-- User Area -->
                               @auth

                                <div class="user_thumb">

                                    <a href="#" id="header-user-btn"><img class="img-fluid" src="{{asset('images/default/photo.png')}}" alt=""></a>

                                    <!-- User Meta Dropdown Area Start -->
                                    <ul class="user-meta-dropdown">
                                        <li class="user-title"> {{ Auth::user()->name }} </li>
                                        <li><a href="profile.html">My Profile</a></li>
                                        <li><a href="order-list.html">Orders List</a></li>
                                        <li><a href="wishlist.html">Wishlist</a></li>
                                        <li>
                                                
                                        <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                               <i class="fa fa-sign-out fa-fw"></i> Logout
                                            </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>

                                        </li>
                                    </ul>
                                </div>
                             @else
                            <button type="button" class="btn btn-outline-info"><a href="{{route('login')}}">Login</a></button>
                           @endauth




                                 

                                     
                              

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mainmenu_area">
                <div class="container">
                    <nav id="bigshop-nav" class="navigation">
                        <!-- Logo Area Start -->
                        <div class="nav-header">
                            <div class="nav-toggle"></div>
                        </div>
                        <!-- Nav Text Area Start -->
                        <!--<span class="nav-text align-to-right"><i class="ti-headphone-alt"></i> +008 111 22472 44</span>-->
                        <!-- Main Menus Wrapper -->
                        <div class="nav-menus-wrapper">
                            <ul class="nav-menu">
                                
                             @foreach($menus as $menu)

                                  <?
                                  $subMenus=DB::table('sub_menu')->where('status',1)->where('fk_menu_id',$menu->id)->orderBy('serial_num','ASC')->get();
                                     ?>


                              


                                <li class="<? echo($url==$menu->url)?'current' : '' ?> {{(count($subMenus)>0)}}">
                                    <a href='{{URL::to("$menu->url")}}'>{{ucfirst($menu->name)}}

                                          @if(count($subMenus)>0)
                                            
                                       
                                        @endif

                                    </a>

                                    @if(count($subMenus)>0)

                                    <ul class="nav-dropdown">

                                           @foreach($subMenus as $subMenu)

                                                 <? $subSubMenu=DB::table('sub_sub_menu')->where('fk_sub_menu_id',$subMenu->id)->orderBy('serial_num','ASC')->get(); ?>


                                        <li class="{{(count($subSubMenu)>0)}}">

                                            <a href="{{URL::to('/')}}/{{$subMenu->url}}">{{$subMenu->name}}</a>
                                            
                                            @if(count($subSubMenu)>0)

                                            <ul class="nav-dropdown">
                                                 @foreach($subSubMenu as $ssMenu)

                                                <li>
                                                    <a href="{{URL::to('/')}}/{{$ssMenu->url}}" title="{{$ssMenu->name}}" target="_blank">{{$ssMenu->name}}</a>
                                                </li>
                                             @endforeach

                                            </ul>

                                            @endif

                                        </li>
                                  
                                      
                                   
                                @endforeach
                                      
                                    </ul>
                                     @endif
                                </li>
                                
                       
                           @endforeach





                                <li><a href="#">Categories</a>
                                    <div class="megamenu-panel">

                                       
                                        <div class="megamenu-tabs">
                                             
                                            <ul class="megamenu-tabs-nav">
                                                @foreach($categories as $cat)
                                                <li class="active"><a href="#">{{$cat->category_name}}</a></li>
                                                   @endforeach
                                            </ul>
                                            @foreach($categories as $cate)
                                            <div class="megamenu-tabs-pane">
                                                <div class="megamenu-lists">

                                                @php
                                                    $subcategory = App\SubCategory::where('fk_category_id','=',$cate->id)->get();
                                                @endphp

                                                    @foreach($subcategory as $subcat)

                                                    <ul class="megamenu-list list-col-3">
                                                        <li class="megamenu-list-title"><a href="#">{{$subcat->sub_category_name}}</a></li>

                                                            <ul class="megamenu-list">

                                                            @php
                                                                    $subsubcategory = App\SubSubCategory::where('fk_subsubcategory_id','=',$subcat->id)->get();
                                                                        
                                                            @endphp

                                                            @foreach($subsubcategory as $ssC)
                                                                  <li><a href="{{URL::to('category-product/'.$ssC->id)}}">{{$ssC->subsub_category_name}}</a></li>
                                                                  @endforeach  
                                                            </ul>
                                                    </ul>
                                                    @endforeach
                                                 
                                                </div>

                                          
                                            </div>
                                            @endforeach
                                            
                                           
                                            
                                        </div>
                                       
                                    </div>
                                </li>
                                
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
        <!-- Bottom Header Area End -->
    </header>
    <!-- ***** Header Area End ***** -->

    @yield('content')

    @include('frontend._partials.footer')

    @yield('script')