<div class="footer_upper">
      
    <div class="container">
        <div class="row">
            <div class="col-md-12 section_padding_30_0">
                <div class="row">
            <div class="col-md-4">
                <div class="block-title">
                    <span style="font-size: 14px;font-family: 'robotolight';
                     color: #767479;padding-bottom: 10px;">SIGN UP FOR JOINLETTER:</span>
                    <div class="clear"></div>
                </div>
                {!! Form::open(['route'=>'join-us.store','method'=>'POST','role'=>'form','data-toggle'=>'validator']) !!}

                    <div class="block-content">
                        <!--
                        <div class="form-subscribe-header">
                            <label for="newsletter">Sign Up for Our Newsletter:</label>
                        </div>
                        -->
                        <div class="input-box">
                           <input type="text" name="email" id="newsletter" placeholder="Email Address" title="Sign up for our join us" required="" style="    border: none;float: left;">
                        </div>
                        <div class="actions">
                            <button type="submit" class="btn btn-info" style="background: #FF6600;">JOIN US</button>
                        </div>
                        <div class="subs_btm_text">
                            <span>Get the first look at collaborations, new products,  events, sales &amp; more.</span>
                        </div>
                    </div>

                {!! Form::close() !!}
            </div>

         <div class="col-md-5">
            
            <li class="block-title"><strong><a href="#" style="font-size: 14px;font-family: 'robotolight';color: #767479;padding-bottom: 10px;display: inline-block;">We Accept:</a></strong> <a style="display: inline-block;" href="#"> <img src="https://d287v37ii85t8x.cloudfront.net/media/wysiwyg/cards_credt.png" alt="Dailyshoppers-accept-credit-cards"> </a></li>
        </div>

         <div class="share_wf col-md-3">
                  <p>FOLLOW US AT:</p>
                <div class="_icon">


                     <div class="footer_social_area mt-15">
                            @foreach($icons as $icon)
                            <a href="{{$icon->url}}"><img src="{{asset($icon->icon_class)}}"></a>
                            @endforeach
                        </div>
                   
                  


                    <!-- Place this tag in your head or just before your close body tag. -->
                

                  <!-- Place this tag where you want the share button to render. -->
                 


                     
                     
                </div>
                </div>
            </div>
            </div>




  </div>


 </div>
</div>

  <!-- ***** Footer Area Start ***** -->
    <footer class="footer_area section_padding_100_0">
        <div class="container" style="padding-bottom: 35px;">
            <div class="row">
                <!-- Single Footer Area Start -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single_footer_area">
                        <div class="footer_heading mb-30">
                            <h6>About us</h6>
                        </div>
                        <div class="footer_content">
                            <p>{{$info->meta_description}}.</p>
                            <p>Phone: +880 {{$info->mobile_no}}</p>
                            <p>Email: {{$info->email}}</p>
                        </div>
                        <div class="footer_social_area mt-15">
                            @foreach($icons as $icon)
                            <a href="{{$icon->url}}"><i class="{{$icon->icon_class}}" aria-hidden="true"></i></a>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- Single Footer Area Start -->
                <div class="col-12 col-md-6 col-lg">
                    <div class="single_footer_area">
                        <div class="footer_heading mb-30">
                            <h6>Account Information</h6>
                        </div>
                        <ul class="footer_widget_menu">
                            <li><a href="{{URL::to('user-profile')}}"><i class="fa fa-angle-right" aria-hidden="true"></i>Your Account</a></li>
                            <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Free Shipping Policy</a></li>
                            <li><a href="{{URL::to('cart')}}"><i class="fa fa-angle-right" aria-hidden="true"></i>Your Cart</a></li>
                            <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Return Policy</a></li>
                           
                            <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Delivary Info</a></li>
                        </ul>
                    </div>
                </div>
                <!-- Single Footer Area Start -->
                <div class="col-12 col-md-6 col-lg">
                    <div class="single_footer_area">
                        <div class="footer_heading mb-30">
                            <h6>Support</h6>
                        </div>
                        <ul class="footer_widget_menu">
                            <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Help</a></li>
                            <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Product Support</a></li>
                            <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Terms &amp; Conditions</a></li>
                            <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Privacy Policy</a></li>
                            <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Payment Method</a></li>
                            <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Affiliate Proggram</a></li>
                        </ul>
                    </div>
                </div>
                <!-- Single Footer Area Start -->
                <div class="col-12 col-md-6 col-lg">
                    <div class="single_footer_area">
                        <div class="footer_heading mb-30">
                            <h6>Join our mailing list</h6>
                        </div>
                        <div class="subscribtion_form">
                            <form action="#" method="post">
                                <input type="email" name="mail" class="mail" placeholder="Your E-mail Addrees">
                                <button type="submit" class="submit"><i class="ti-check" aria-hidden="true"></i></button>
                            </form>
                        </div>

                          <div class="single_footer_area mt-30">
                        <div class="footer_heading mb-15">
                            <h6>Download our Mobile Apps</h6>
                        </div>
                        <div class="apps_download">
                            <a href="https://play.google.com/store?hl=en" target="_blank"><img src="{{asset('public/frontend/img/core-img/play-store.png')}}" alt="Play Store"></a>
                            <a href="https://www.apple.com/lae/ios/app-store/" target="_blank"><img src="{{asset('public/frontend/img/core-img/app-store.png')}}" alt="Apple Store"></a>
                        </div>
                    </div>
                    </div>
          
                </div>
            </div>
        </div>
        <!-- Footer Bottom Area Start -->


         <div class="footer-bottom-area">
            <div class="container">
                <div class="row clearfix" style="padding: 12px 0px;">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                       
                      
                            © <?php echo date("Y"); ?>All Rights Reserved by <a href="{{url('/')}}" style="color: #fff"><b>{{$info->company_name}}</b></a> 
                           

                    </div>

                    <div class="col-md-6 col-sm-12 col-xs-12">

                     <div class="textright">Developed by:<a href="http://www.smartsoftware.com.bd" target="_blank" title="best web design Development Company in Bangladesh, best Software Development Company in Bangladesh"><strong> Smart Software Inc</strong> </a></div>

                    </div>
                </div>
            </div>
        </div>

    </footer>
    <!-- ***** Footer Area End ***** -->

    <!-- jQuery (Necessary for All JavaScript Plugins) -->
    <script src="{{asset('public/frontend/js/jquery/jquery-2.2.4.min.js')}}"></script>

    <!-- Popper js -->
    <script src="{{asset('public/frontend/js/popper.min.js')}}"></script>
    <!-- Bootstrap js -->
     <script src="{{asset('public/frontend/js/bootstrap.min.js')}}"></script>
    <!-- Plugins js -->
    <script src="{{asset('public/frontend/js/plugins.js')}}"></script>
    <!-- Active js -->
    <script src="{{asset('public/frontend/js/active.js')}}"></script>
     <script src="{{asset('public/frontend/dist/js/sb-admin-2.js')}}"></script>
      <script src="{{asset('public/frontend/vendor/metisMenu/metisMenu.min.js')}}"></script>
      <script src="{{asset('public/frontend/vendor/metisMenu/owl.carousel.js')}}"></script>
      <script src="{{asset('public/frontend/js/jquery.js')}}"></script>

    <script src="{{asset('public/frontend/js/jssocials.min.js')}}"></script>
      <script type="text/javascript">
          $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    autoplay:2000,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})
      </script>



    




