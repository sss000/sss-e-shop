@extends('frontend._partials.app')
@section('title')
    Contact us | dailyshoppers

        @endsection 

@section('content')


		    <div class="breadcumb_area bg-img background-overlay-white" style="background-image:  url({{asset('public/frontend/img/bg-img/breadcumb.jpg')}});">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active">Contact</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Breadcumb Area End ***** -->

    <!-- >>>>>>>>>>>>>>>> Message Now Area Start <<<<<<<<<<<<<<<< -->
    <div class="message_now_area section_padding_100" id="contact">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-10">
                    <div class="contact_from">
                        
                       <form method="post" action="mail.php" class="contact-form mt-45" id="contact">
                            <!-- Message Input Area Start -->
                            <div class="contact_input_area">
                                <div id="success_fail_info"></div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="f_name" id="f-name" placeholder="First Name" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="l_name" id="l-name" placeholder="Last Name" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="email" class="form-control" name="email" id="email" placeholder="Your E-mail" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                             <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" required>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <textarea name="message" class="form-control" id="message" cols="30" rows="10" placeholder="Your Message *" required></textarea>
                                        </div>
                                    </div>


                                      <?php
                                  if (isset($_POST['submit'])) {
                                      
                                      $rep=$_POST['g-recaptcha-response'];
                                       echo $rep;
                                         }

                                            ?>

                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn bigshop-btn" value="Send Message">Send Message</button>
                                      
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- >>>>>>>>>>>>>>>> Message Now Area Start <<<<<<<<<<<<<<<< -->

    <!-- Map Area Start -->
    <div id="googleMap">
    	

    	<div id="map"><iframe src="{{$info->map_embed}}" frameborder="0" style="border:0; height: 450px; width: 100%" allowfullscreen ></iframe> </div>
    </div>





 @endsection