<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                     
                        <li><a href="{{URL::to('')}}" target="_blank"><i class="fa fa-file-o fa-fw"></i> Visit SIte</a></li>
                        <li>

                        <li>
                            <a href="{{URL::to('dashboard')}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>

                      <li>
                            <a href="{{URL::to('product-create')}}"><i class="fa fa-edit fa-fw"></i> Product Create</a>
                        </li>  

                            <li>
                            <a href="#"><i class="fa fa-edit fa-fw"></i> Order<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{URL::to('orders')}}">New Order</a>
                                </li>
                                  <li>
                                    <a href="{{URL::to('received')}}">Received Order</a>
                                </li> 

                                <li>
                                    <a href="{{URL::to('delivered')}}">Delivered Order</a>
                                </li>

                                 <li>
                                    <a href="{{URL::to('cancel-order')}}">Cancel Order</a>
                                </li>
                        
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                        <li>
                            <a href="{{URL::to('Slider-info')}}"><i class="fa fa-edit fa-fw"></i> Slider</a>
                        </li>

                         <li>
                            <a href="{{URL::to('category')}}"><i class="fa fa-edit fa-fw"></i> Category</a>
                        </li>

                         <li>
                            <a href="{{URL::to('attribute')}}"><i class="fa fa-edit fa-fw"></i> Attribute</a>
                        </li>

                           <li>
                            <a href="{{URL::to('all-brand')}}"><i class="fa fa-edit fa-fw"></i> Brand</a>
                        </li>

                          <li>
                            <a href="{{URL::to('add-manager')}}"><i class="fa fa-edit fa-fw"></i> Ad managers</a>
                        </li>

                        <li>
                            <a href="{{URL::to('admin-discount')}}"><i class="fa fa-edit fa-fw"></i> Discount Offer</a>
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Organigation info<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{URL::to('others-info/primary/edit')}}">Primary info</a>
                                </li>
                                <li>
                                    <a href="{{URL::to('other/about')}}">About Company</a>
                                </li>

                                  <li>
                                    <a href="{{URL::to('Social-links')}}">Social link</a>
                                </li>

                                    <li>
                                    <a href="{{URL::to('payment-methode')}}">Payment Methode</a>
                                </li>  

                                    <li>
                                    <a href="{{URL::to('join-us')}}">join us</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                      
                     

                         <li>
                            <a href="{{URL::to('menu')}}"><i class="fa fa-edit fa-fw"></i> Menu Configure</a>
                        </li>
                           <li>
                            <a href="{{URL::to('pages')}}"><i class="fa fa-edit fa-fw"></i> Page</a>
                        </li> 

                          <li>
                            <a href="{{URL::to('users')}}"><i class="fa fa-table fa-fw"></i> Users</a>
                        </li>
               
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
        </nav>