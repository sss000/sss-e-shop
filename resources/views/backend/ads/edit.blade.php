@extends('backend.app')
@section('content')
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="panel panel-info page-panel">
                        <div class="panel-heading">
                         update now
                            <div class="panel-btn pull-right">
                            	<a href="{{URL::to('add-manager')}}" class="btn btn-success btn-sm"> <i class="fa fa-asterisk"></i> All </a>
                            </div>
                        </div>
                           

                            {!! Form::open(['route'=>['add-manager.update',$data->id],'method'=>'PUT','role'=>'form','data-toggle'=>'validator','class'=>'form-horizontal','files'=>'true'])  !!}
                        <div class="panel-body min-padding">
                            <div class="col-md-9" style="">


                                     <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                                    {{Form::label('image', 'Image:', array('class' => 'col-md-12'))}}
                                    <div class="col-md-12">
                                        <label class="post_upload" for="file">

                                            @if($data->image!=null)
                                        <img id="image_load" src='{{asset('images/ads/'.$data->image)}}' class="img-responsive">
                                        @else
                                        <img id="image_load" src="{{asset('images/default/photo.png')}}">
                                        @endif
                                            <!--  -->
                                           
                                        </label>
                                        {{Form::file('image',array('id'=>'file','style'=>'display:none','onchange'=>"photoLoad(this,'image_load')"))}}
                                         @if ($errors->has('image'))
                                                <span class="help-block" style="display:block">
                                                    <strong>{{ $errors->first('image') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                </div>

                     
                                
                                <div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
                                    {{Form::label('name','Ads Name:',['class'=>'col-md-12'])}}
                                    <div class="col-md-7">
                                        {{Form::text('name',$data->name,['class'=>'form-control','placeholder'=>'Write Title','required'])}}
                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                      <div class="col-md-2">
                                    {{Form::select('status',['1'=>'Active','0'=>'Inactive'],$data->status,['class'=>'form-control','required'])}}
                                </div>

                             

                                </div>

                              
                         
                                
                           

                   
                                <div class="form-group">
                                    <div class="col-md-12">
                                       <button  type="submit" class="btn btn-success"><i class="fa fa-save"></i> &nbsp;Submit</button>
                                    </div>
                                   
                                </div>                  

 </div>
                        </div>
                        <!-- /.panel-body -->
                        {{Form::close()}}
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

@endsection
@section('script')

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="{{asset('public/backend/plugin/tagbox/js/tag-it.min.js')}}"></script>

@endsection