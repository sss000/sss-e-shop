@extends('backend.app')
@section('content')
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="panel panel-info page-panel">
                        <div class="panel-heading">
                           Add New Post
                            <div class="panel-btn pull-right">
                            	<a href="{{URL::to('add-manager')}}" class="btn btn-success btn-sm"> <i class="fa fa-asterisk"></i> All Ads</a>
                            </div>
                        </div>

                                   {!! Form::open(['route'=>'add-manager.store','method'=>'POST','role'=>'form','data-toggle'=>'validator','class'=>'form-horizontal','files'=>'true'])  !!}
        
        <div class="form-group ">
            <label for="slide_photo" class="col-md-3 control-label">Ads image</label>
            <div class="col-md-8">
                <label class="slide_upload" for="file">
                    <!--  -->
                        <img id="image_load" src="{{asset('images/default/photo.png')}}">

                </label>
                <input id="file" style="display:none" required="" name="image" type="file">
                             </div>
        </div>

       

        <div class="form-group">
            <label for="name" class="col-md-3 control-label">Title</label>
            <div class="col-md-8">
                <input class="form-control" placeholder="ads name" name="name" type="text" value="" id="name">
            </div>

        </div>
     


        <div class="form-group">
            <label for="status" class="col-md-3 control-label">Status</label>

            <div class="col-md-4">
                <select class="form-control" id="status" name="status"><option value="1" selected="selected">Active</option><option value="2">Inactive</option></select>
            </div>
        </div>
            
        <div class="form-group">
            <div class="col-md-9 col-md-offset-3">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
      {!! Form::close() !!}

                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

@endsection
@section('script')

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="{{asset('public/backend/plugin/tagbox/js/tag-it.min.js')}}"></script>
<script type="text/javascript">
    function loadSubCat(id){
        $('#subCategoryLoad').load('{{URL::To("load-sub-cat")}}/'+id);
    }
   
    $(document).ready(function() {
            $('#tagbox').tagit({
                allowSpaces: true,
                singleField: true,
                singleFieldNode: $('#tagboxField'),
               
            });
        });
</script>
@endsection