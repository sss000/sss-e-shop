@extends('backend.app')
@section('content')
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="panel panel-default page-panel">
                        <div class="panel-heading">
                            All Slider
                            <div class="panel-btn pull-right">
                            	<a href="{{URL::to('Slider-info/create')}}" class="btn btn-primary btn-sm" > <i class="fa fa-plus"></i> Add New</a>
                            </div>
                        </div>
                        <div class="panel-body min-padding">
                        	<table class="table table-striped table-bordered">
                        		<thead>
                        			<tr>
                        				<th width="5%">SL</th>
                        				
                                        <th >Slide Photo</th>
                                        <th >Middle Caption</th>
                                        <th >status</th>
                                        <th width="12%">Created date</th>
                        				
                        				<th width="7%">Action</th>
                        			</tr>
                        		</thead>
                        		<tbody>
                                   <? $i=0; ?>
                             @foreach($alldata as $data)
                              <? $i++; ?>
                        			<tr>
                        				<td>{{$i}}</td>
                                        
                                        <td><img src="{{asset($data->image)}}" style="height: 80px; width: 200px;"></td>
                                        <td>{{$data->middle_caption}}</td>
                                       <td><i class="{{($data->status==1)? 'fa fa-check success' : 'fa fa-times-circle'}}"></i></td>
                        				<td>{{$data->created_at}}</td>
                        				<td style="text-align: center">
                                           
                                         {{Form::open(array('route'=>['Slider-info.destroy',$data->id],'method'=>'DELETE','id'=>"deleteForm$data->id"))}}
                                          

                                             <a href='{{URL::to("Slider-info/$data->id/edit")}}' class="btn btn-info btn-xs"> <i class="fa fa-pencil-square"></i></a>
                                             
                                            <button type="button" class="btn btn-danger btn-xs" onclick='return deleteConfirm("deleteForm{{$data->id}}")'><i class="fa fa-trash"></i></button>
                                        {!! Form::close() !!}

                        				</td>
                        			</tr>
                                  @endforeach  
                        		</tbody>
                        		
                        	</table>
                        </div>
                      
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        


@endsection