@extends('backend.app')
@section('content')
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="panel panel-info page-panel">
                        <div class="panel-heading">
                           Add New Slider
                            <div class="panel-btn pull-right">
                            	<a href="{{URL::to('Slider-info')}}" class="btn btn-success btn-sm"> <i class="fa fa-asterisk"></i> All Slider</a>
                            </div>
                        </div>
                           
 
                            {!! Form::open(['route'=>['Slider-info.update',$data->id],'method'=>'PUT','role'=>'form','data-toggle'=>'validator','class'=>'form-horizontal','files'=>'true'])  !!}
                            
                        <div class="panel-body min-padding">
                            <div class="col-md-9">

                                <div class="form-group  {{ $errors->has('top_caption') ? 'has-error' : '' }}">
                                    {{Form::label('top_caption','top caption:',['class'=>'col-md-12'])}}
                                    <div class="col-md-12">
                                        {{Form::text('top_caption',$data->top_caption,['class'=>'form-control','placeholder'=>'Write top_caption'])}}
                                        @if ($errors->has('top_caption'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('top_caption') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                    <div class="form-group  {{ $errors->has('middle_caption') ? 'has-error' : '' }}">
                                    {{Form::label('middle_caption','middle caption:',['class'=>'col-md-12'])}}
                                    <div class="col-md-12">
                                        {{Form::text('middle_caption',$data->middle_caption,['class'=>'form-control','placeholder'=>'Write middle_caption'])}}
                                        @if ($errors->has('middle_caption'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('middle_caption') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group  {{ $errors->has('buttom_caption') ? 'has-error' : '' }}">
                                    {{Form::label('buttom_caption','buttom caption :',['class'=>'col-md-12'])}}
                                    <div class="col-md-12">
                                        {{Form::text('buttom_caption',$data->buttom_caption,['class'=>'form-control','placeholder'=>'Write buttom_caption'])}}
                                        @if ($errors->has('buttom_caption'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('buttom_caption') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                        <div class="form-group">
                                {{Form::label('fk_category_id', 'Category', array('class' => 'col-md-12'))}}

                                <div class="col-md-4">
                                    {{Form::select('fk_category_id',$categories,$data->category_name, ['class' => 'form-control','placeholder'=>'Select a category'])}}
                                </div>
                            </div>              
                       
                               
                           
                          <div class="col-md-2">
                                    {{Form::number('serial_num',$data->serial,['class'=>'form-control','required','min'=>0])}}
                                    @if ($errors->has('category_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('category_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                 <div class="col-md-2">
                                    {{Form::select('status',['1'=>'Active','0'=>'Inactive'],$data->status,['class'=>'form-control','required'])}}
                                </div>

                               

 </div>
                            <div class="col-md-3 well min-padding">
                                
                                
                                <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
                                    {{Form::label('image', 'Slider Image:', array('class' => 'col-md-12'))}}
                                    <div class="col-md-12">
                                        <label class="post_upload" for="file">

                                        	@if($data->image!=null)
                                        <img id="image_load" src='{{asset("$data->image")}}' class="img-responsive">
                                        @else
                                        <img id="image_load" src="{{asset('images/default/photo.png')}}">
                                        @endif
                                            <!--  -->
                                           
                                        </label>
                                        {{Form::file('image',array('id'=>'file','style'=>'display:none','onchange'=>"photoLoad(this,'image_load')"))}}
                                         @if ($errors->has('image'))
                                                <span class="help-block" style="display:block">
                                                    <strong>{{ $errors->first('image') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <div class="col-md-12">
                                       <button  type="submit" class="btn btn-success"><i class="fa fa-save"></i> &nbsp;Submit</button>
                                    </div>
                                   
                                </div>
                                
                            </div>
                            
                        </div>
                        <!-- /.panel-body -->
                        {{Form::close()}}
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>

@endsection
@section('script')

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="{{asset('public/backend/plugin/tagbox/js/tag-it.min.js')}}"></script>

@endsection