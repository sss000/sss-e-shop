@extends('backend.app')
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-info page-panel">

<aside id="column-right" class="col-sm-11">


        <!-- ngView: --><div class="ng-view ng-scope"><ul class="breadcrumb ng-scope">
        <li><a href="#!/"><i class="fa fa-home"></i></a></li>
        <li>Add Product</li>
    </ul>
    <div class="ng-scope">

 
    <h3 class="subtitle">Product Add</h3>



     {!! Form::open(['route'=>'product-create.store','method'=>'POST','role'=>'form','data-toggle'=>'validator','class'=>'form-horizontal','files'=>true])  !!}

 <div class="panel-body panel panel-info page-panel">

      
        <div class="col-md-12 form-group no-padding">
            
            <div class="col-sm-6">
                <label class="col-sm-6 label-title">Category<span class="required">*</span></label>
                <div class="col-sm-12">

               {{Form::select('fk_category_id',$categories,'',['class'=>'form-control','required','placeholder'=>'Select Category','onchange'=>'loadSubCat(this.value)','id'=>'category'])}}

            <div class="help-block with-errors"></div>
            <span class="text-danger ng-binding ng-hide" ng-show="error.fk_category_id"></span>

        </div>
       
            </div>


             <div class="col-md-6 form-group no-padding">
              
               
                    {{Form::label('fk_sub_category_id','Sub Category*',['class'=>'col-md-12  label-title'])}}

                          <div class="col-md-12" id="subCategoryLoad">
                                <span class="form-control">First select category</span>

                            </div>


                             </div>

        </div>


           <div class="col-md-12 form-group no-padding">
            
            <div class="col-sm-6">
                <label class="col-sm-6 label-title">Brand<span class="required">*</span></label>
                <div class="col-sm-12">

              {{Form::select('fk_brand_id',$Brands,'',['class'=>'form-control','placeholder'=>'Select Brand','id'=>'Brands'])}}

            <div class="help-block with-errors"></div>
            <span class="text-danger ng-binding ng-hide" ng-show="error.fk_category_id"></span>

        </div>
      
            </div>


             <div class="col-md-6 form-group no-padding">
              
               
                    
                          {{Form::label('fk_subsubcategory_id','Sub Sub Category*',['class'=>'col-md-12  label-title'])}}
                        <div class="col-md-12" id="subsubCategoryLoad">
                                <span class="form-control">First select Sub category</span>

                            </div>
                             </div>

        </div>


            <div class="col-md-12 form-group no-padding">
            
            <div class="col-sm-6">
                <label class="col-sm-6 label-title">Product Code<span class="required">*</span></label>
                <div class="col-sm-12">

              {{Form::text('product_code','',['class'=>'form-control','required','placeholder'=>'Ex: m123'])}}

           

        </div>
      
            </div>


             <div class="col-md-6 form-group no-padding">
              
               
                     <label class="col-sm-12 label-title">Product Link*</label>

                        <div class="col-md-12">
                                {{Form::text('link','',['class'=>'form-control','required','placeholder'=>'Ex: m123'])}}

                            </div>
                             </div>

        </div>

           
    <div class="col-md-12 no-padding form-group add-title ">
        <label class="col-sm-12 label-title">Title <span class="required">*</span></label>
        <div class="col-sm-12">
            <input type="text" class="form-control" placeholder="Product title" name="title" required="">

            <div class="help-block with-errors"></div>
           

        </div>
    </div>




    
 
    <div class="col-md-12 no-padding form-group add-image">
        <div class="col-sm-12">
            <p><span>The first photo is required.</span></p>
            <div class="upload-section">

                
            <label class="upload-image">
           
               
                   
                     <div class="col-md-12">
                <label class="upload_photo upload " for="file">
                    <!--  -->
                    <img src="{{asset('images/default/photo.png')}}" id="image_load">
                    <i class="upload_hover ion ion-ios-camera-outline"></i>
                </label>
                {{Form::file('image',array('id'=>'file','style'=>'display:none','onchange'=>'loadPhoto(this,"image_load")'))}}
                 @if ($errors->has('image'))
                        <span class="help-block" style="display:block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                    @endif
            </div>
         
   </label>

               <label class="upload-image">
           
                
                   
                    
                      
                      
                     <div class="col-md-12">
                <label class="upload_photo upload " for="file_two"
                    <!--  -->
                    <img src="{{asset('images/default/photo.png')}}" id="image_two">
                    <i class="upload_hover ion ion-ios-camera-outline"></i>
                </label>
                {{Form::file('image-one',array('id'=>'file_two','style'=>'display:none','onchange'=>'loadPhoto(this,"image_two")'))}}
                 @if ($errors->has('image-one'))
                        <span class="help-block" style="display:block">
                            <strong>{{ $errors->first('image-one') }}</strong>
                        </span>
                    @endif
            </div>
         
   </label>

                 <label class="upload-image">
           
                  
                     <div class="col-md-12">
                <label class="upload_photo upload client_logo_upload" for="file_three">
                    <!--  -->
                    <img src="{{asset('images/default/photo.png')}}" id="image_three">
                    <i class="upload_hover ion ion-ios-camera-outline"></i>
                </label>
                {{Form::file('image_three',array('id'=>'file_three','style'=>'display:none','onchange'=>'loadPhoto(this,"image_three")'))}}
                 @if ($errors->has('image_three'))
                        <span class="help-block" style="display:block">
                            <strong>{{ $errors->first('image_three') }}</strong>
                        </span>
                    @endif
            </div>
         
   </label>

               <label class="upload-image">
           
                
                    
                     <div class="col-md-12">
                <label class="upload_photo upload client_logo_upload post_upload" for="file-four">
                    <!--  -->
                    <img src="{{asset('images/default/photo.png')}}" id="file-one">
                    <i class="upload_hover ion ion-ios-camera-outline"></i>
                </label>
                {{Form::file('image_four',array('id'=>'file-four','style'=>'display:none','onchange'=>'loadPhoto(this,"file-one")'))}}
                 @if ($errors->has('image_four'))
                        <span class="help-block" style="display:block">
                            <strong>{{ $errors->first('image_four') }}</strong>
                        </span>
                    @endif
            </div>
   </label>
            </div>
            <div class="help-block with-errors"></div>
            <span class="text-danger ng-binding ng-hide" ng-show="error.photo_one"></span>


        </div>
    </div>
    <div class="col-md-12 no-padding form-group select-wholesale_price">
        <div class="col-sm-4">
            <span class="help-block">Price</span>

            <input type="number" class="form-control" placeholder="TK" name="price" min="0" step="any">

            <div class="help-block with-errors"></div>
           


        </div>
        <div class="col-sm-4">
            <span class="help-block">Discount price</span>
            <input type="number" class="form-control" placeholder="discount price" name="discount" min="0" step="any">
           

        </div>

        <div class="col-sm-4">
            <span class="help-block">Product Quantity</span>
            <input type="number" class="form-control" placeholder="quantity" name="quantity" min="0" step="any">
           

        </div>
    </div>


    <div class="col-md-12 no-padding form-group item-description">

        <label class="col-sm-12 label-title">Short Description <span class="required">*</span></label>
        <div class="col-sm-12">

           {{Form::textArea('meta_description','',['class'=>'form-control','placeholder'=>'Write short description about this Product. ','rows'=>3])}}

            @if ($errors->has('meta_description'))
                     <span class="help-block">
          <strong>{{ $errors->first('meta_description') }}</strong>
                        </span>
             @endif

           
        </div>
    </div>

      <div class="col-md-12 no-padding form-group item-description">

        <label class="col-sm-12 label-title">Description <span class="required">*</span></label>
        <div class="col-sm-12">

           {{Form::textArea('description','',['class'=>'form-control tinymce','placeholder'=>'Write description about this Product. ','rows'=>3])}}

            @if ($errors->has('description'))
                     <span class="help-block">
          <strong>{{ $errors->first('description') }}</strong>
                        </span>
             @endif

           
        </div>
    </div>
   
    <div  class="col-md-12 no-padding form-group select-wholesale_price">
       <div class="col-sm-4">
         <span class="help-block">Is Special? </span>
        
           <select class="form-control"  name="is_featured" required="">

            <option label="Normal" value="0" >Normal</option>
            <option label="Special" value="1">Special</option>
            

        </select>
            <div class="help-block with-errors"></div>


        </div>



    </div>

    



  <!-- ngRepeat: attr in allData.attribute --><!-- end ngRepeat: attr in allData.attribute -->
  @foreach($attrubute as $attr)
    <div class="col-md-12 no-padding form-group" >
        <label class="col-sm-12 label-title">{{$attr->name}} : </label>
        <div class="col-sm-12">
        @foreach($attr->options as $option)
           <label class="btn btn-xs bordered "> <input type="checkbox" name="option_id[{{$attr->id}}][]" value="{{$option->id}}" > {{$option->option_name}} </label>
       @endforeach
       
    </div>

</div>
@endforeach

        <div class="col-md-12 no-padding form-group">
            <br>
            <div class="col-sm-12">


                 <button type="submit" class="btn btn-primary">Submit</button>


               
            </div>

            </div>
        </div>

    </div>

      {!! Form::close() !!}

    </div>
    

</div>



</aside>


     </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>



@endsection


@section('script')

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="{{asset('public/backend/plugin/tagbox/js/tag-it.min.js')}}"></script>
    <script type="text/javascript">

        function loadSubCat(id){
            $('#subCategoryLoad').load('{{URL::To("load-sub-cat")}}/'+id);
        }




          function loadSubSubCat(id){

            $('#subsubCategoryLoad').load('{{URL::To("load-subsub-cat")}}/'+id);
        }
      
    </script>  

@endsection