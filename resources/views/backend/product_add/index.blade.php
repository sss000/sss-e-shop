@extends('backend.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 no-padding">
            <div class="panel panel-default page-panel">
                <div class="panel-heading">
                    All Posts
                    <div class="panel-btn pull-right">
                        <a href="{{URL::to('product-create/create')}}" class="btn btn-primary btn-sm" > <i class="fa fa-plus"></i> Add New</a>
                    </div>
                </div>
                <div class="panel-body min-padding">
                    <table id="all_data" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th width="5%">SL</th>
                            <th width="15%">Title</th>
                             <th width="12%">Category Name</th>
                            <th width="12%">product_code</th>
                           
                           
                            <th width="12%">Show Home Page</th>
                             <th width="12%">creates_at</th>
                            <th width="7%">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                          
                          <? $i=0; ?>
                             @foreach($alldata as $data)
                               <? $i++; ?>    
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$data->title}}</td>
                                        <td>{{$data->category_name}}</td>
                                        <td>{{$data->product_code}}</td>
                                     
                                        <td><i class="{{($data->status==1)? 'fa fa-check-circle text-success' : 'fa fa-times text-danger'}}"></i></td>

                                        <td>{{date('d-m-Y',strtotime($data->created_at))}}</td>

                                       
                                        <td style="text-align: center">

                                            {{Form::open(array('route'=>['product-create.destroy',$data->id],'method'=>'DELETE','id'=>'deleteForm'))}}
                                            <a href='{{URL::to("product-create/$data->id/edit")}}' class="btn btn-info btn-xs"> <i class="fa fa-pencil-square"></i></a>
                                       

                                     <button type="submit" class="btn btn-danger btn-xs" onclick='return deleteConfirm("deleteForm{{$data->id}}")'><i class="fa fa-trash"></i></button>
                                     
                                             <!-- <button type="submit" class="btn btn-danger btn-xs" onclick="return deleteConfirm()" title="Item Delete">
                                                    <i class="fa fa-trash-o" aria-hidden="true"></i></button> -->

                                        {!! Form::close() !!}


                                        

                                        </td>
                                    </tr>
                                    @endforeach    



                        </tbody>



                    </table>

                     <div class="pull-right">{{$alldata->render()}}</div>
                </div>
            <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

@endsection


<script type="text/javascript">

function deleteConfirm(){
  var con= confirm("Do you want to delete?");
  if(con){
    return true;
  }else 
  return false;
}
</script>
