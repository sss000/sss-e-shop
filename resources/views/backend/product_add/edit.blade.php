@extends('backend.app')
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-info page-panel">

<aside id="column-right" class="col-sm-11">


     

    <div class="ng-scope">


    <h3 class="subtitle">Product Update</h3>



     {!! Form::open(array('route' => ['product-create.update', $data->id],'method'=>'PUT','class'=>'form-horizontal','files'=>true)) !!}




 <div class="panel-body panel panel-info page-panel">

      
        <div class="col-md-12 form-group no-padding">
            
            <div class="col-sm-6">
                <label class="col-sm-6 label-title">Category<span class="required">*</span></label>
                <div class="col-sm-12">

               {{Form::select('fk_category_id',$categories,$data->fk_category_id,['class'=>'form-control','required','placeholder'=>'Select Category','onchange'=>'loadSubCat(this.value)','id'=>'category'])}}

            <div class="help-block with-errors"></div>
            <span class="text-danger ng-binding ng-hide" ng-show="error.fk_category_id"></span>

        </div>
      
            </div>


             <div class="col-md-6 form-group no-padding">
              
               
                    {{Form::label('fk_sub_category_id','Sub Category*',['class'=>'col-md-12  label-title'])}}

                          <div class="col-md-12" id="subCategoryLoad">
                              {{Form::select('fk_sub_category_id',$subCategory,$data->fk_subsubcategory_id,['class'=>'form-control','required','placeholder'=>'Select Sub Category','id'=>'subCategory','onchange'=>"loadSubSubCat(this.value)"])}}

                            </div>


                             </div>

        </div>


           <div class="col-md-12 form-group no-padding">
            
            <div class="col-sm-6">
                <label class="col-sm-6 label-title">Brand<span class="required">*</span></label>
                <div class="col-sm-12">

              {{Form::select('fk_brand_id',$Brands,$data->fk_brand_id,['class'=>'form-control','required','placeholder'=>'Select Brand','id'=>'Brands'])}}

            <div class="help-block with-errors"></div>
            <span class="text-danger ng-binding ng-hide" ng-show="error.fk_category_id"></span>

        </div>
      
            </div>


             <div class="col-md-6 form-group no-padding">
              
               
                     <label class="col-sm-12 label-title">Sub Sub Category*</label>

                        <div class="col-md-12" id="subsubCategoryLoad">
                               {{Form::select('fk_subsubcategory_id',$subsubCategory,$data->fk_subsubcategory_id,['class'=>'form-control','placeholder'=>'Select Sub Sub Category','id'=>'subsubCategory'])}}

                            </div>
                             </div>

        </div>


            <div class="col-md-12 form-group no-padding">
            
            <div class="col-sm-6">
                <label class="col-sm-6 label-title">Product Code<span class="required">*</span></label>
                <div class="col-sm-12">

              {{Form::text('product_code',$data->product_code,['class'=>'form-control','required','placeholder'=>'Ex: m123'])}}

           

        </div>
      
            </div>


             <div class="col-md-6 form-group no-padding">
              
               
                     <label class="col-sm-12 label-title">Product Link*</label>

                        <div class="col-md-12">
                                {{Form::text('link',$data->link,['class'=>'form-control','required','placeholder'=>'Ex: m123'])}}

                            </div>
                             </div>

        </div>

           
    <div class="col-md-12 no-padding form-group add-title ">
        <label class="col-sm-12 label-title">Title <span class="required">*</span></label>
        <div class="col-sm-12">
            <input type="text" class="form-control" placeholder="Product title" name="title" ng-model="form.title" required="" value="{{$data->title}}">

            <div class="help-block with-errors"></div>
           

        </div>
    </div>




    
 
    <div class="col-md-12 no-padding form-group add-image">
        <div class="col-sm-12">
            <p><span>The first photo is required.</span></p>
            <div class="upload-section">

                
            <label class="upload-image">
           
               
                   
                     <div class="col-md-12">
                <label class="upload_photo upload " for="file">
                    <!--  -->
                   @if(isset($data->itemImage[0]) and $data->itemImage[0]->photo!=null)
                        <img id="image_load" src='{{asset('images/items/small/'.$data->itemImage[0]->photo)}}' class="img-responsive">
                         @else
                        <img id="image_load" src="{{asset('images/default/photo.png')}}">
                   @endif
                </label>
                {{Form::file('image',array('id'=>'file','style'=>'display:none','onchange'=>'loadPhoto(this,"image_load")'))}}
                 @if ($errors->has('image'))
                        <span class="help-block" style="display:block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                    @endif
            </div>
         
   </label>

               <label class="upload-image">
           
                
                   
                    
                      
                      
                     <div class="col-md-12">
                <label class="upload_photo upload " for="file_two"
                    <!--  -->
                      @if(isset($data->itemImage[1]) and $data->itemImage[1]->photo!=null)
                        <img id="image_two" src='{{asset('images/items/small/'.$data->itemImage[1]->photo)}}' class="img-responsive">
                         @else
                        <img id="image_two" src="{{asset('images/default/photo.png')}}">
                   @endif
                </label>
                {{Form::file('image-one',array('id'=>'file_two','style'=>'display:none','onchange'=>'loadPhoto(this,"image_two")'))}}
                 @if ($errors->has('image-one'))
                        <span class="help-block" style="display:block">
                            <strong>{{ $errors->first('image-one') }}</strong>
                        </span>
                    @endif
            </div>
         
   </label>

                 <label class="upload-image">
           
                  
                     <div class="col-md-12">
                <label class="upload_photo upload client_logo_upload" for="file_three">
                    <!--  -->
                      @if(isset($data->itemImage[2]) and $data->itemImage[2]->photo!=null)
                        <img id="image_three" src='{{asset('images/items/small/'.$data->itemImage[2]->photo)}}' class="img-responsive">
                         @else
                        <img id="image_three" src="{{asset('images/default/photo.png')}}">
                   @endif
                </label>
                {{Form::file('image_three',array('id'=>'file_three','style'=>'display:none','onchange'=>'loadPhoto(this,"image_three")'))}}
                 @if ($errors->has('image_three'))
                        <span class="help-block" style="display:block">
                            <strong>{{ $errors->first('image_three') }}</strong>
                        </span>
                    @endif
            </div>
         
   </label>

               <label class="upload-image">
           
                
                    
                     <div class="col-md-12">
                <label class="upload_photo upload client_logo_upload post_upload" for="file-four">
                    <!--  -->
                  @if(isset($data->itemImage[3]) and $data->itemImage[3]->photo!=null)
                        <img id="image_four" src='{{asset('images/items/small/'.$data->itemImage[3]->photo)}}' class="img-responsive">
                         @else
                        <img id="image_four" src="{{asset('images/default/photo.png')}}">
                   @endif
                </label>
                {{Form::file('image_four',array('id'=>'file-four','style'=>'display:none','onchange'=>'loadPhoto(this,"image_four")'))}}
                 @if ($errors->has('image_four'))
                        <span class="help-block" style="display:block">
                            <strong>{{ $errors->first('image_four') }}</strong>
                        </span>
                    @endif
            </div>
   </label>
            </div>
            <div class="help-block with-errors"></div>
            <span class="text-danger ng-binding ng-hide" ng-show="error.photo_one"></span>


        </div>
    </div>
    <div class="col-md-12 no-padding form-group select-wholesale_price">
        <div class="col-sm-4">
            <span class="help-block">Price</span>

            <input type="number" class="form-control" placeholder="TK" name="price" value="{{$data->price}}">

            <div class="help-block with-errors"></div>
           


        </div>

           <div class="col-sm-4">
            <span class="help-block">Discount price</span>
            <input type="number" class="form-control" placeholder="discount price" name="discount" min="0" step="any" value="{{$data->discount}}">
           

        </div>
        <div class="col-sm-4">
            <span class="help-block">Product Quantity</span>
            <input type="number" class="form-control" placeholder="quantity" name="quantity" value="{{$data->quantity}}">
           

        </div>
    </div>


    <div class="col-md-12 no-padding form-group item-description">

        <label class="col-sm-12 label-title">Short Description <span class="required">*</span></label>
        <div class="col-sm-12">

           {{Form::textArea('meta_description',$data->meta_description,['class'=>'form-control','placeholder'=>'Write short description about this Product. ','rows'=>3])}}

            @if ($errors->has('meta_description'))
                     <span class="help-block">
          <strong>{{ $errors->first('meta_description') }}</strong>
                        </span>
             @endif

           
        </div>
    </div>

      <div class="col-md-12 no-padding form-group item-description">

        <label class="col-sm-12 label-title">Description <span class="required">*</span></label>
        <div class="col-sm-12">

           {{Form::textArea('description',$data->description,['class'=>'form-control tinymce','placeholder'=>'Write description about this Product. ','rows'=>3])}}

            @if ($errors->has('description'))
                     <span class="help-block">
          <strong>{{ $errors->first('description') }}</strong>
                        </span>
             @endif

           
        </div>
    </div>
   
    <div  class="col-md-12 no-padding form-group select-wholesale_price">
       <div class="col-sm-4">
         <span class="help-block">Is Special? </span>
        
           <select class="form-control"  name="is_featured" value="{{$data->is_featured}}">

             <option label="Normal" value="0" >Normal</option>

            <option label="Special" value="1">Special</option>
           

        </select>
           

        </div>


       



    </div>



  <!-- ngRepeat: attr in allData.attribute --><!-- end ngRepeat: attr in allData.attribute -->
  @foreach($attrubute as $attr)
    <div class="col-md-12 no-padding form-group" >
        <label class="col-sm-12 label-title">{{$attr->name}} : </label>
        <div class="col-sm-12">
        @foreach($attr->options as $option)
        <?php 
            if(isset($attributeOption[$option->id])){
                $checked = 'checked';
            }else{
                $checked ='';
            }
         ?>
           <label class="btn btn-xs bordered " > <input type="checkbox" name="option_id[{{$option->id}}]" value="{{$option->id}}" <?php echo $checked ?>> {{$option->option_name}} </label>
       @endforeach
       
    </div>

</div>
@endforeach

        <div class="col-md-12 no-padding form-group">
            <br>
            <div class="col-sm-12">


                 <button type="submit" class="btn btn-primary">Submit</button>


               
            </div>

            </div>
        </div>
         {!! Form::close() !!}

    </div>

     

    </div>
    

</div>



</aside>


     </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>



@endsection


@section('script')

    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="{{asset('public/backend/plugin/tagbox/js/tag-it.min.js')}}"></script>
    <script type="text/javascript">

        function loadSubCat(id){
            $('#subCategoryLoad').load('{{URL::To("load-sub-cat")}}/'+id);
        }




          function loadSubSubCat(id){

            $('#subsubCategoryLoad').load('{{URL::To("load-subsub-cat")}}/'+id);
        }
      
    </script>  

@endsection