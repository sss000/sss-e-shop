<?
 $info = DB::table('about_company')->first();
?>
<style>
    *{margin: 0;} 
</style>
<br>
<div class="row" style="width: 960px;margin: 0 auto;border:1px solid #ddd;">
<div style="width: 100%;height: 80px;background:#ddd;text-align: center">
    <h1 style="margin-bottom: 0;padding: 0"> {{$info->company_name}} </h1>
    <p> {{$info->address}}, {{$info->mobile_no}}, <a href="{{URL::to('')}}"> {{URL::to('')}} </a> </p>
</div>
    <br>
    <div class="col-lg-12" style="padding:0 20px;padding-bottom: 20px; overflow: hidden">
        <div class="panel panel-default page-panel">
            <div class="panel-heading" style="text-align: center">
               <u>Order Details</u>
            </div>
            <div class="panel-body" id="print_body" style="width:100%;overflow: hidden">

                <div class="col-md-5" style="width: 50%;float: left">
                    <div class="">
                        <h4> Bill to </h4>
                        <span> {{$order->user->name}} </span><br>
                        <span> {{$order->user->phone_number}} </span><br>
                        <span>{{$order->billing_address}}</span><br>
                    </div>

                </div>
                <div class="col-md-7" style="width: 50%;float: left">
                    <div class="" style="width: 100%;">
                        <h4> Invoice </h4>
                        <span> Invoice : {{$order->invoice_id}} </span><br>
                        <span> Invoice Date:  {{date('h:i A, jS M, Y ',strtotime($order->created_at))}} </span><br>
                        <span> Shipping Address : {{$order->shipping_address}} </span><br>

                    </div>

                </div>
                <div class="col-md-12 no-padding text-right" style="text-align: right">
                    @if($order->status==1)
                        <label class="labels slabel-info">Processing</label>
                    @elseif($order->status==2)
                        <label class="labels slabel-primary">Accepted</label>
                    @elseif($order->status==3)
                        <label class="labels slabel-warning">Pending</label>
                    @elseif($order->status==0)
                        <label class="labels slabel-danger">Cancel</label>
                    @elseif($order->status==4)
                        <label class="labels slabel-success">Delivered</label>
                    @endif
                </div>
                <table width="100%" class="table table-bordered" cellpadding="5" cellspacing="0" border="1">
                    <thead>
                    <tr>
                        <th width="5%">SL</th>

                        <th>Item</th>
                        <th>Attribute</th>
                        <th width="10%" class="text-right">Price</th>
                        <th class="text-center">Qty</th>
                        <th width="15%" class="text-right">Sub Total</th>

                    </tr>
                    </thead>
                    <tbody>

                    @foreach($order->hasproduct as $key => $item)
                        <tr>
                            <td class="text-center">{{$key+1}}</td>
                            <td class="text-left"><a href="{{URL::to('item-view')}}/{{$item->hasItem->link}}"> {{$item->hasItem->title}} </a><br>  </td>
                            <td>
                                @foreach(json_decode($item->attributes,true) as $attr => $opt )
                                    <span><b>{{$attr}} : </b> {{$opt}}</span><br>
                                @endforeach
                            </td>
                            <td class="text-right"> {{$item->price}} tk</td>
                            <td class="text-center">{{$item->qty}}</td>

                            <td class="text-right"> {{$item->amount}} tk</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
                <div style="width:100%">
                    <div class="col-md-4 no-padding" style="width: 40%;float: right" >
                        <br>
                        <table width="100%" class="table table-bordered" cellpadding="5" cellspacing="0" border="1">
                            <tbody>
                            <tr>
                                <th class="text-right">Sub-Total : </th>
                                <td class="text-right">{{$order->total_amount-$order->shipping_amount}}</td>
                            </tr>
                            <tr>
                                <th class="text-right">Shipping Amount : </th>
                                <td class="text-right">{{$order->shipping_amount}}</td>
                            </tr>

                            <tr>
                                <th class="text-right">Total : </th>
                                <td class="text-right">{{$order->total_amount}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


<br>
            <!--Middle Part End-->
        </div>
    </div>
    <div style="width: 100%;background:#ddd">
        <p style=" text-align:center;padding: 15px;"> All Rights Reserved by {{$info->company_name}} </p>
    </div>
</div>