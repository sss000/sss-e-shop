@extends('backend.app')
@section('content')

<div id="wrapper">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12">
<div class="panel-heading table-bordered">
                    Order Details
                    <div class="panel-btn pull-right">
                        <a href="{{URL::to('orders')}}" class="btn btn-primary btn-sm"> <i class="fa fa-list"></i> View All</a>
                    </div>
                </div>
<div id="print_body" class="all">
    <style type="text/css">
        .cart_image{width: 100px;height:80px;float: left;}
        .all{
            padding: 25px 25px;
                border: 2px solid #ddd;
        }
        .printable{display: none;}
        @media print {
            #print_body{
                overflow: hidden; 
                
                }
            .printable{display:inline-block;}
            .no-print{display: none;}
            .col-md-6{width: 50%;float: left;}
            .row{width: 100%;overflow: hidden;}
            a[href]:after {
                content: none !important;
              }
              .customerInfo p{margin: 0;line-height: 16px;}
              input{border: 0;}

        @page {
            size: auto;
            margin: 1cm;
            margin-top: 0 !important;
        }
          
        }        
    </style>
    <div class="print_top printable" style="width: 100%; overflow: hidden;border-bottom:1px solid #ddd;padding-bottom: 5px;margin-bottom: 5px;">
        <div class="view_logo" style="margin: 0 auto;width: 100%;text-align: center;">
            
            <h3 style="margin-top: 0;margin-left: 20px;"><img class="print-logo" src='{{asset("public/img/logo.jpg")}}' style="width: 100px; height: auto;margin-right:30px;"><strong>smartsoft</strong></h3>
        </div>
        <div class="view_company_info" style="width: 100%; float: left; margin-left: 10px;text-align: center;">panthopoth Dhaka<br />
            Phone: 018457941824, Email: @email.com
        </div>
    </div>
<div class="row">
<div class="col-md-6 customerInfo">
    <p><b>Invoice: </b> {{$data->invoice_id}} </p>
    <p><b> Name: </b> {{$data->name}} </p>
    <p><b>Email: </b> {{$data->email}} </p>
    <p><b>Phone no: </b> {{$data->phone_number}} </p>
    <p><b>Order Date: </b> {{date('h:i A, jS M, Y',strtotime($data->created_at))}}</p>
    
</div>
<div class="col-md-4 customerInfo">
   
    <p><b>Billing Address : </b> {{$data->billing_address}}</p>
    <p><b>Shipping Address : </b> {{$data->shipping_address}} </p>
    <p><b>Payment Method: </b> {{$data->hasMethode->name}}</p>
    @if($data->payment_method_id!=null)
    <p><b>Trx ID: </b> {{$data->transaction_no}}</p>
    @endif
    <p><b>Delivery By: </b> {{$data->delivered_by}}</p>
    <p><b>Delivery Date: </b> {{date('h:i A, jS M, Y',strtotime($data->date_time))}}</p>

    
</div>

</div>
<br>
    <div class="cart-tables">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Photo</th>
                    <th class="p-name">Product Name</th>
                    <th class="p-amount">Option</th>
                    <th class="p-amount">Price</th>
                    
                    <th class="p-quantity">Quantity</th>
                    <th class="p-total">Total</th>
                </tr>
            </thead>
            <tbody>

                  <? $i=0; ?>
                    @foreach($data->hasproduct as $item)
                     <? $i++; ?> 
           
                <tr>
               
                   
                    <td class="p-name">
                        <a href=''>{{$i++}}</a>
                    </td>

                      @php
                                    $select_image = App\ItemPhoto::where('fk_item_id','=',$item->fk_item_id)->first();
                                        
                                @endphp

                    <td><img src="{{asset('images/items/small/'.$select_image->photo) }}" style="width: 80px; height: 60px"> </td>
                    <td class="p-amount"><span class="amount"> {{$item->hasItem->title}}</span>
                       
                   
                        
                    </td>
                    <td class="p-amount"><span class="amount">

                        @foreach(json_decode($item->attributes,true) as $attr =>$val)
                        <p><b>{{$attr}} : </b> {{$val}} </p>
                        @endforeach
                        
                    </span></td>
                    <td class="p-amount"><span class="amount">{{$item->price}}</span></td>

                    <td>
                    <b>{{$item->quantity}}</b>
                    </td>
                    <td class="p-total">{{$item->amount}}</td>
                </tr>

                 @endforeach
         
            </tbody>
        </table>
    </div>
    <!-- total -->
<div class="shipping-content">
    
        <div class="row">
            <div class="col-md-12"> 
              
        <div class="col-md-8 no-print" style="width: 60%;float: left;">
                    @if($data->status ==2)
                    <div class="dalivery">
                        {!! Form::open(['route'=>['orders.update',$data->id],'method'=>'PUT']) !!}
                        <div class="form-group">
                            <label class="col-md-12">Delivered By</label>
                            <div class="col-md-12">
                                <input type="text" name="delivered_by" placeholder="Delivered By" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Date and Time</label>
                            <div class="col-md-12">
                                <input type="text" name="date_time" value="{{date('d-m-Y h:i:s A')}}" placeholder="Date Time" class="form-control datetimepicker">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>&nbsp;</label>
                            <div class="col-md-12">
                                <button class="btn btn-success">Delivered</button>
                                @if($data->status!=0)
                                    <a href='{{URl::to("orders/$data->id/edit?action=0")}}' class="btn btn-danger">Cancel</a>
                                @endif
                            </div>
                        </div>
                        {!!Form::close()!!}
                    </div>
                    @else
                        <div class="order_action">
                            @if($data->status!=2 and $data->status!=4)
                                <a href='{{URl::to("orders/$data->id/edit?action=2")}}' class="btn btn-success">Accept</a>

                            @endif

                            @if($data->status!=3 and $data->status!=1)
                                <a href='{{URl::to("orders/$data->id/edit?action=3")}}' class="btn btn-warning">Pending</a>
                            @endif

                            @if($data->status!=0)
                                <a href='{{URl::to("orders/$data->id/edit?action=0")}}' class="btn btn-danger">Cancel</a>
                            @endif

                            <button class="btn btn-primary"  onclick="printPage('print_body')">Print</button>
                        </div>
                    @endif

                </div>



   <div class="shipping_amount col-md-4 pull-right">
        <table class="table table-bordered">
        <tbody>
        <tr>
            <td class="text-right"><strong>Sub-Total</strong></td>
            <td class="text-right">{{$data->total}}</td>
        </tr>
        <tr>
            <td class="text-right"><strong>Shipping Amount</strong></td>
            <td class="text-right">{{$data->shipping_amount}}</td>
        </tr>

        <tr>
            <td class="text-right"><strong>Total</strong></td>
            <td class="text-right">{{$data->total_amount}}</td>
        </tr>
        </tbody>
    </table>
    </div>  
    </div>
        </div>
    
       
</div>



</div>

        <div class="col-md-6 col-md-offset-6">

        </div>
    </div>
  


</div>
</div>     

@endsection
@section('script')

<script type="text/javascript">
    function printPage(id){
        $('#'+id).printThis({
            importStyle: false,
        });
    }
</script>
@endsection






