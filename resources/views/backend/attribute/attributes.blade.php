@extends('backend.app')
@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-info page-panel">
           
            <div class="panel-body">

            	<div class="box-body col-md-12">
					{!! Form::open(array('route' => 'attribute.store','class'=>'form-horizontal')) !!}
					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
						{{Form::label('name', 'Name', array('class' => 'col-md-2'))}}
						<div class="col-md-8">
							{{Form::text('name','',array('class'=>'form-control','placeholder'=>'Sub Category Name','required'))}}
							@if ($errors->has('name'))
			                    <span class="help-block">
			                        <strong>{{ $errors->first('name') }}</strong>
			                    </span>
				            @endif
						</div>
						<div class="col-md-2">
							{{Form::select('status', array('1' => 'Active', '2' => 'Inactive'), '1', ['class' => 'form-control'])}}
						</div>
					</div>
				<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
					<div class="col-md-10">
							{{Form::label('description', 'Description', array('class' => 'col-md-2'))}}
							<div class="col-md-10">
								{{Form::textArea('description','', ['class' => 'form-control','rows'=>'3','placeholder'=>'Write something about sub Attribute. This is helpful for seo.'])}}
							</div>
						</div>

						<div class="col-md-2">
							<select class="form-control" name="type">
								<option value="0" selected="selected">Size</option>
								<option value="1">Color</option>
								
							</select>
						</div>
</div>

						</div>
					<div class="form-group">
						<div class="col-md-10 col-md-offset-2">
							{{Form::submit('Submit',array('class'=>'btn btn-info'))}}
						</div>
					</div>
						
					{!! Form::close() !!}
				</div>



           	</div>
        </div>


        	<table class="table table-striped table-hover table-bordered center_table" id="my_table">
						<thead>
							<tr>
								<th>SL</th>
								<th>Attribute name</th>
								<th>Type</th>
								<th>option</th>
								<th>Status</th>
								<th>Created At</th>
								<th colspan="2">Action</th>
							</tr>
						</thead>
						<tbody>
						<? $i=1; ?>
						@foreach($allData as $data)
							<tr>
								<td>{{$i++}}</td>
								<td>{{$data->name}}</td>
								<td>{{$data->type}}</td>

								<td><a href='{{URL::to("sub-attribute/$data->id")}}' class="btn btn-warning btn-xs">Sub Option ({{DB::table('sub_attribute')->where('fk_attribute_id',$data->id)->count()}})</a></td>
							
								<td><i class="{{($data->status==1)? 'fa fa-check-circle text-success' : 'fa fa-times text-danger'}}"></i></td>
								<td><? echo date('d-M-Y',strtotime($data->created_at)); ?></td>
								<td><a href="#editModal{{$data->id}}" data-toggle="modal" class="btn btn-info btn-xs"><i class="fa fa-pencil-square"></i></a>
								<!-- Modal -->
					<div class="modal fade" id="editModal{{$data->id}}" tabindex="-1" role="dialog">
					  <div class="modal-dialog modal-lg">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					        <h4 class="modal-title">Edit : {{$data->name}}</h4>
					      </div>
					        {!! Form::open(array('route' => ['attribute.update',$data->id],'class'=>'form-horizontal','method'=>'PUT')) !!}
					      <div class="modal-body">
							<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
								{{Form::label('name', 'Name', array('class' => 'col-md-2'))}}
								<div class="col-md-8">
									{{Form::text('name',$data->name,array('class'=>'form-control','placeholder'=>'Category Name','required'))}}
									@if ($errors->has('name'))
					                    <span class="help-block">
					                        <strong>{{ $errors->first('name') }}</strong>
					                    </span>
						            @endif
								</div>
								<div class="col-md-2">
									{{Form::select('status', array('1' => 'Active', '2' => 'Inactive'),$data->status, ['class' => 'form-control'])}}
								</div>
							</div>
							<div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
		                        {{Form::label('description','Descripton',['class'=>'col-md-2'])}}

		                        <div class="col-md-8">
		                            {{Form::textarea('description',$data->description,['class'=>'form-control','rows'=>'3','placeholder'=>'Ex: description','required'])}}
		                            @if ($errors->has('description'))
					                    <span class="help-block">
					                        <strong>{{ $errors->first('description') }}</strong>
					                    </span>
						            @endif
		                        </div>
		                   
		                    </div>
						
						
							
								
					      </div>
					   

					      <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									{{Form::submit('Save changes',array('class'=>'btn btn-info'))}}
					      </div>
							{!! Form::close() !!}
					    </div><!-- /.modal-content -->
					  </div><!-- /.modal-dialog -->
					</div><!-- /.modal -->

								</td>
								<td>
									{{Form::open(array('route'=>['attribute.destroy',$data->id],'method'=>'DELETE','id'=>"deleteForm$data->id"))}}
									
			            				<button type="button" class="btn btn-danger btn-xs" onclick='return deleteConfirm("deleteForm{{$data->id}}")'><i class="fa fa-trash"></i></button>

			            			

			        				{!! Form::close() !!}
								</td>
							</tr>
						@endforeach 
						</tbody>
					</table>

					
					
				
    </div>
</div>


	

@endsection




<script type="text/javascript">

function deleteConfirm(){
  var con= confirm("Do you want to delete?");
  if(con){
    return true;
  }else 
  return false;
}
</script>