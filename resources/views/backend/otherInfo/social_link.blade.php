@extends('backend.app')
@section('content')

<div class="tab_content">


  <div class="menu_form left">
      {!! Form::open(array('route' => 'Social-links.store','class'=>'form-horizontal','files'=>true)) !!}


        <div class="form-group {{ $errors->has('icon_class') ? 'has-error' : '' }}">
            {{Form::label('icon_class', 'Icon image', array('class' => 'col-md-12'))}}
            <div class="col-md-8">
                <label class="upload_photo upload client_logo_upload" for="file">
                    <!--  -->
                     <img src="{{asset('images/default/photo.png')}}" id="image_load">
                    <i class="upload_hover ion ion-ios-camera-outline"></i>
                </label>
                {{Form::file('icon_class',array('id'=>'file','style'=>'display:none'))}}
                 @if ($errors->has('icon_class'))
                        <span class="help-block" style="display:block">
                            <strong>{{ $errors->first('icon_class') }}</strong>
                        </span>
                    @endif
            </div>
        </div> 
 
        <div class="form-group  col-md-8 {{ $errors->has('name') ? 'has-error' : '' }}">
            {{Form::label('name', ' Name', array('class' => 'col-md-12'))}}
            <div class="col-md-12">
                {{Form::text('name','',array('class'=>'form-control','placeholder'=>'Name','required'))}}
            </div>
        </div>

              <div class="form-group col-md-4">
            {{Form::label('status', 'Status', array('class' => 'col-md-12'))}}
            <div class="col-md-12">
                {{Form::select('status', array('1' => 'Active', '2' => 'Inactive'),'1', ['class' => 'form-control'])}}
            </div>
        </div>

        
      
      
        <div class="col-md-8 form-group">
            
            {{Form::label('url', 'URL', array('class' => 'col-md-12'))}}
            <div class="col-md-12">
               
                  
                    {{Form::text('url','',array('class'=>'form-control','placeholder'=>'URL(with http://)','required'))}}
           
                @if ($errors->has('url'))
                    <span class="help-block">
                        <strong>{{ $errors->first('url') }}</strong>
                    </span>
                @endif
            </div>
        </div>



 

        <div class="form-group">
            <div class="col-md-12">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
    {!! Form::close() !!}
      </div>
          <div class="or">
    
        <table class="table table-striped table-hover table-bordered center_table" id="my_table">
            <thead>
                <tr>
                    <th>SL</th>
                    <th>Name</th>
                    <th>URL</th>
                    
                  
                    <th>Status</th>
                    <th width="10%">Action</th>
                </tr>
            </thead>
            <tbody>
            <? $i=1; ?>
            @foreach($allData as $data)
                <tr>
                    <td>{{$i++}}</td>
                    <td><b>{{$data->name}}</b></td>
                    <td><a href="{{$data->url}}" target="_blank">{{$data->url}}</a></td>
                   
                
                    <td><i class="{{($data->status==1)? 'fa fa-check-square' : 'fa fa-times-circle'}}"></i></td>

                    <td>
                    <a href="#editModal{{$data->id}}" data-toggle="modal" class="btn btn-info btn-xs"><i class="fa fa-pencil-square"></i></a>
                    <!-- Modal -->
<div class="modal fade" id="editModal{{$data->id}}" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit Social icon : <b> {{$data->name}} </b></h4>
      </div>
        {!! Form::open(array('route' => ['Social-links.update', $data->id],'method'=>'PUT','class'=>'form-horizontal','files'=>true)) !!}
        <br>
        <div class="form-group   {{ $errors->has('name') ? 'has-error' : '' }}">
            {{Form::label('name', ' Name', array('class' => 'col-md-3 control-label'))}}
            <div class="col-md-8">
                {{Form::text('name',$data->name,array('class'=>'form-control','placeholder'=>'Name','required'))}}
            </div>
        </div>
         <div class="form-group  {{ $errors->has('url') ? 'has-error' : '' }}">
            
            {{Form::label('url', 'URL', array('class' => 'col-md-3 control-label'))}}
            <div class="col-md-8">
           
                    
                    {{Form::text('url',$data->url,array('class'=>'form-control','placeholder'=>'URL','required'))}}
                             @if ($errors->has('url'))
                    <span class="help-block">
                        <strong>{{ $errors->first('url') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    

        <div class="form-group">
            {{Form::label('status', 'Status', array('class' => 'col-md-3 control-label'))}}

            <div class="col-md-8">
                {{Form::select('status', array('1' => 'Active', '2' => 'Inactive'),$data->status, ['class' => 'form-control'])}}
            </div>
        </div>
            {{Form::hidden('id',$data->id)}}

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <input class="btn btn-info" type="submit" value="Save changes">
      </div>
    {!! Form::close() !!}
    </div><!-- /.modal-content --> 
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

        
            {{Form::open(array('route'=>['Social-links.destroy',$data->id],'method'=>'DELETE','id'=>"deleteForm$data->id"))}}
          

          <button type="button" class="btn btn-danger btn-xs" onclick='return deleteConfirm("deleteForm{{$data->id}}")'><i class="fa fa-trash"></i></button>

        {!! Form::close() !!}
                    </td>

                </tr>
            @endforeach
            </tbody>
        </table>
      
  </div>


@endsection
