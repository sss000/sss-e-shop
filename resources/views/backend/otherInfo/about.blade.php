@extends('backend.app')
@section('content')


<h3 class="box_title">About Organization</h3>
    {!! Form::open(array('route' =>['others-info.update', $data->id],'method'=>'PUT','class'=>'form-horizontal','files'=>true)) !!}
        

     <div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
            {{Form::label('image', 'About photo', array('class' => 'col-md-3 control-label'))}}
            <div class="col-md-8">
                <label class="upload_photo upload client_logo_upload" for="file">
                    <!--  -->
                    <img src="{{asset('images/about/'.$data->image)}}" id="image_load">
                    <i class="upload_hover ion ion-ios-camera-outline"></i>
                </label>
                {{Form::file('image',array('id'=>'file','style'=>'display:none'))}}
                 @if ($errors->has('image'))
                        <span class="help-block" style="display:block">
                            <strong>{{ $errors->first('image') }}</strong>
                        </span>
                    @endif
            </div>
        </div>


        



        <div class="form-group  {{ $errors->has('keyword') ? 'has-error' : '' }}">
            {{Form::label('keyword', 'Keyword', array('class' => 'col-md-3 control-label'))}}
            <div class="col-md-8">
                {{Form::textArea('keyword',$data->keyword,array('class'=>'form-control','placeholder'=>'keyword','rows'=>'5'))}}
                @if ($errors->has('keyword'))
                        <span class="help-block">
                            <strong>{{ $errors->first('keyword') }}</strong>
                        </span>
                    @endif
            </div>
        </div>

         <div class="form-group  {{ $errors->has('meta_description') ? 'has-error' : '' }}">
            {{Form::label('meta_description', 'Meta Description', array('class' => 'col-md-3 control-label'))}}
            <div class="col-md-8">
                {{Form::textArea('meta_description',$data->meta_description,array('class'=>'form-control','placeholder'=>'meta Description','rows'=>'5'))}}
                @if ($errors->has('meta_description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('meta_description') }}</strong>
                        </span>
                    @endif
            </div>
        </div>
        <div class="form-group  {{ $errors->has('description') ? 'has-error' : '' }}">
            {{Form::label('description', 'Long Description', array('class' => 'col-md-3 control-label'))}}
            <div class="col-md-8">
                {{Form::textArea('description',$data->description,array('class'=>'form-control tinymce','placeholder'=>'Long Description','rows'=>'10'))}}
                @if ($errors->has('description'))
                        <span class="help-block">
                            <strong>{{ $errors->first('description') }}</strong>
                        </span>
                    @endif
            </div>
        </div>


            {{Form::hidden('id',$data->id)}}
        <div class="form-group">
            <div class="col-md-9 col-md-offset-3">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>
      
	{!! Form::close() !!}

@endsection
@section('script')
<script type="text/javascript">
    $("textarea[maxlength]").on("propertychange input", function() {
        if (this.value.length > this.maxlength) {
            this.value = this.value.substring(0, this.maxlength);
        }  
    });
</script>
@endsection

