@extends('backend.app')
@section('content')
            <div class="row">
                <div class="col-lg-12 no-padding">
                    <div class="panel panel-default page-panel">
                        <div class="panel-heading">
                            All
                           
                        </div>
                        <div class="panel-body min-padding">
                        	<table class="table table-striped table-bordered">
                        		<thead>
                        			<tr>
                        				<th width="5%">SL</th>
                        				
                                       
                                        <th >E-mail</th>
                                     
                                        <th >Created date</th>
                        				
                        				<th >Action</th>
                        			</tr>
                        		</thead>
                        		<tbody>
                                        
                                         <? $i=0; ?>
                             @foreach($alldata as $data)
                              <? $i++; ?>
                                    <tr>
                                        <td>{{$i}}</td>
                                        
                                        
                                        <td>{{$data->email}}</td>
                                       
                                        <td>{{$data->created_at}}</td>
                                        
                                        <td style="text-align: center">
                                           {{Form::open(array('route'=>['join-us.destroy',$data->id],'method'=>'DELETE','id'=>"deleteForm$data->id"))}}
                                           
                                            <button type="button" class="btn btn-danger btn-xs" onclick='return deleteConfirm("deleteForm{{$data->id}}")'><i class="fa fa-trash"></i></button>
                                        {!! Form::close() !!}

                                        </td>
                                    </tr>
                                  @endforeach  
                        		</tbody>

                                <div class="pull-right">
                                    {{$alldata->render()}}
                                </div>
                        		
                        	</table>
                        </div>
                      
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
        


@endsection