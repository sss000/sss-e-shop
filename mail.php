<?php
/* Set e-mail recipient */
$myemail  = "mskamrul1@gmail.com";

/* Check all form inputs using check_input function */

$email    = check_input($_POST['email']);
$email    = check_input($_POST['f_name']);
$yourname = check_input($_POST['l_name']);
$message  = check_input($_POST['message']);
$subject  = check_input($_POST['subject']);

/* Let's prepare the message for the e-mail */
$message = "
Name: $yourname
E-mail: $email
Message:
$message
Subject:
$subject
";

/* Send the message using mail() function */
mail($myemail, $yourname, $message, $subject);

/* Redirect visitor to the thank you page */
print"<script>alert('Thank you for contacting us. We will be in touch with you very soon.');</script>"; 
print"<script>location.href='/E-shop/contact-us'</script>";
exit();

/* Functions we used */
function check_input($data, $problem='')
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    if ($problem && strlen($data) == 0)
    {
        show_error($problem);
    }
    return $data;
}
?>